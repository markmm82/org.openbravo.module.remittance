/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.remittance.ad_actionbutton;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.advpaymentmngt.utility.Value;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.system.Language;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.ad.ui.ProcessTrl;
import org.openbravo.module.remittance.Instruction;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.utility.REM_RemittanceCreateFile;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.xmlEngine.XmlDocument;

public class CreateFile extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strKey = vars.getStringParameter("inpremRemittanceId");
      String strTab = vars.getStringParameter("inpTabId");
      String strMessage = "";
      printPage(response, vars, strKey, strWindow, strTab, strProcessId, strMessage, true);
    } else if (vars.commandIn("GENERATE")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strKey = vars.getStringParameter("inpremRemittanceId");
      getFilePrintPage(response, vars, strKey, strWindow, strProcessId);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String tabId, String strProcessId, String strMessage, boolean isDefault)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button create file msg:" + strMessage);

    String strHelp = "", strDescription = "";
    OBContext.setAdminMode();
    try {
      if (vars.getLanguage().equals("en_US")) {
        Process process = OBDal.getInstance().get(Process.class, strProcessId);
        strDescription = process.getDescription();
        strHelp = process.getHelpComment();
      } else {
        ProcessTrl process = FIN_Utility.getOneInstance(
            ProcessTrl.class,
            new Value(ProcessTrl.PROPERTY_PROCESS, OBDal.getInstance().get(Process.class,
                strProcessId)),
            new Value(ProcessTrl.PROPERTY_LANGUAGE, FIN_Utility.getOneInstance(Language.class,
                new Value(Language.PROPERTY_LANGUAGE, vars.getLanguage()))));
        strDescription = process.getDescription();
        strHelp = process.getHelpComment();
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    String[] discard = { "" };
    if (strHelp == null || "".equals(strHelp))
      discard[0] = new String("helpDiscard");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/remittance/ad_actionbutton/CreateFile", discard).createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("description", strDescription);
    xmlDocument.setParameter("help", strHelp);
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("tabId", tabId);

    if (isDefault) {
      xmlDocument.setParameter("messageType", "");
      xmlDocument.setParameter("messageTitle", "");
      xmlDocument.setParameter("messageMessage", "");
    } else {
      OBError myMessage = new OBError();
      myMessage.setTitle("");
      if (strMessage == null || strMessage.equals(""))
        myMessage.setType("Success");
      else
        myMessage.setType("Error");
      if (strMessage != null && !strMessage.equals("")) {
        myMessage.setMessage(strMessage);
      } else
        Utility.translateError(this, vars, vars.getLanguage(), "Success");
      vars.setMessage("CreateFile", myMessage);
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void getFilePrintPage(HttpServletResponse response, VariablesSecureApp vars,
      String strKey, String windowId, String strProcessId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("generate " + strKey);
    Remittance remittance = OBDal.getInstance().get(Remittance.class, strKey);
    // TODO: Review contract
    // String strContract = CreateFileData.selectParam(this, strKey, "CONTRACT");

    List<Instruction> instructions = new ArrayList<Instruction>();

    try {
      OBContext.setAdminMode(true);
      final OBCriteria<Instruction> obc = OBDal.getInstance().createCriteria(Instruction.class);
      obc.add(Restrictions.eq("remittance", remittance));
      obc.add(Restrictions.isNull("businessPartner"));
      obc.addOrderBy("lineNo", false);
      obc.setMaxResults(1);
      instructions = obc.list();
    } catch (Exception e) {
      log4j.debug(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }

    if (instructions.size() > 0) {
      String message = String.format(
          Utility.messageBD(new DalConnectionProvider(), "REM_MissingBPartner", OBContext
              .getOBContext().getLanguage().getLanguage()),
          String.valueOf(instructions.get(0).getLineNo()));

      printPageError(response, vars, getOBError(this, vars, message, "Error", ""), "");
      return;
    }

    REM_RemittanceCreateFile createFile = null;
    String javaClassName;
    try {
      OBContext.setAdminMode(true);
      javaClassName = remittance.getRemittanceType().getJavaClassName();
    } finally {
      OBContext.restorePreviousMode();
    }
    if (javaClassName == null || "".equals(javaClassName)) {
      printPageError(response, vars,
          getOBError(this, vars, "@REM_WrongRemittanceFileJavaClassName@", "Error", ""), "");
      return;
    }
    try {
      createFile = (REM_RemittanceCreateFile) Class.forName(javaClassName).getDeclaredConstructor().newInstance();
    } catch (Exception e) {
      log4j.error("Error while creating new instance for REM_RemittanceCreateFile - " + e, e);
      printPageError(response, vars,
          getOBError(this, vars, "@REM_WrongRemittanceFileJavaClassName@", "Error", ""), "");
      return;
    }
    StringBuffer strFile = new StringBuffer();
    if (createFile != null) {
      createFile.init(remittance);
      strFile = createFile.generate(remittance);
      String strFileName = "BANK.DAT";
      if (createFile.getFilename() != null && !"".equals(createFile.getFilename())) {
        strFileName = createFile.getFilename();
      }
      OBError error = createFile.getMyError();
      if (error == null) {
        printPageFile(response, vars, strFile, strFileName);
      } else {
        String lineDetail = createFile.getLineErrorDetail();
        printPageError(response, vars, error, lineDetail);
      }
    }
    return;
  }

  private void printPageFile(HttpServletResponse response, VariablesSecureApp vars,
      StringBuffer strFile, String strFileName) throws IOException, ServletException {
    response.setContentType("application/rtf");
    response.setHeader("Content-Disposition", "attachment; filename=" + strFileName);
    PrintWriter out = response.getWriter();
    out.println(strFile.toString());
    out.close();
  }

  private void printPageError(HttpServletResponse response, VariablesSecureApp vars,
      OBError myError, String lineDetail) throws ServletException, IOException {
    String strTab = vars.getStringParameter("inpTabId");
    String strWindowPath = Utility.getTabURL(strTab, "R", true);
    if (strWindowPath.equals(""))
      strWindowPath = strDefaultServlet;
    if (!myError.getMessage().equals(
        Utility.messageBD(this, myError.getMessage(), vars.getLanguage()))) {
      myError.setMessage(Utility.messageBD(this, myError.getMessage(), vars.getLanguage()));
    }
    if (!"".equals(lineDetail)) {
      myError.setMessage(lineDetail + "\n" + myError.getMessage());
    }
    vars.setMessage(strTab, myError);
    printPageClosePopUp(response, vars, strWindowPath);
  }

  OBError getOBError(ConnectionProvider conn, VariablesSecureApp vars, String strMessage,
      String strMsgType, String strTittle) {
    OBError message = new OBError();
    message.setType(strMsgType);
    message.setTitle(Utility.messageBD(conn, strTittle, vars.getLanguage()));
    message.setMessage(Utility.parseTranslation(conn, vars, vars.getLanguage(), strMessage));
    return message;
  }

  public String getServletInfo() {
    return "Servlet for the generation of files for banks";
  } // end of getServletInfo() method
}
