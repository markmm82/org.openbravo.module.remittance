/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2015 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.ad_actionbutton;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.filter.RequestFilter;
import org.openbravo.base.filter.ValueListFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.process.REM_AddRemittance;
import org.openbravo.xmlEngine.XmlDocument;

public class SelectOrdersOrInvoices extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  private AdvPaymentMngtDao dao;
  private static final RequestFilter filterYesNo = new ValueListFilter("Y", "N", "");

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strWindowId = vars
          .getGlobalVariable("inpwindowId", "SelectOrdersOrInvoices|Window_ID");
      String strTabId = vars.getGlobalVariable("inpTabId", "SelectOrdersOrInvoices|Tab_ID");
      String strRemittanceId = vars.getGlobalVariable("inpremRemittanceId", strWindowId + "|"
          + "REM_Remittance_ID");

      printPage(response, vars, strRemittanceId, strWindowId, strTabId);

    } else if (vars.commandIn("GRIDLIST")) {
      String strRemittanceId = vars.getRequiredStringParameter("inpremRemittanceId");
      String strBPartnerId = vars.getStringParameter("inpcBpartnerId");
      String strDocumentType = vars.getStringParameter("inpDocumentType");
      String strTransactionType = vars.getStringParameter("inpTransactionType");
      String strSelectedPaymentDetails = vars.getInStringParameter("inpScheduledPaymentDetailId",
          "", null);
      Boolean showAlternativePM = "Y".equals(vars.getStringParameter("inpAlternativePaymentMethod",
          filterYesNo));
      Boolean showAlternativeCurrency = "Y".equals(vars.getStringParameter(
          "inpAlternativeCurrency", filterYesNo));

      printGrid(response, vars, strRemittanceId, strSelectedPaymentDetails, showAlternativePM,
          showAlternativeCurrency, strBPartnerId, strDocumentType, strTransactionType);

    } else if (vars.commandIn("SAVE")) {
      String strRemittanceId = vars.getRequiredStringParameter("inpremRemittanceId");
      String strSelectedScheduledPaymentDetailIds = vars.getInStringParameter(
          "inpScheduledPaymentDetailId", "", null);
      String strTabId = vars.getRequiredStringParameter("inpTabId");
      String strDifferenceAction = vars.getStringParameter("inpDifferenceAction", "");
      String strDifferenceAmount = vars.getRequiredNumericParameter("inpDifference");

      saveAndClose(response, vars, strRemittanceId, strSelectedScheduledPaymentDetailIds, strTabId,
          strDifferenceAction, strDifferenceAmount);

    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strRemittanceId, String strWindowId, String strTabId) throws IOException,
      ServletException {

    dao = new AdvPaymentMngtDao();
    String[] discard = { "discard", "discard" };
    Remittance remittance = dao.getObject(Remittance.class, strRemittanceId);
    String strIsReceipt = isReceiptRemittance(remittance);
    if ("N".equals(strIsReceipt)) {
      discard[0] = "TransactionIN";
    }
    if ("Y".equals(strIsReceipt)) {
      discard[1] = "TransactionOUT";
    }
    if ("".equals(strIsReceipt)) {
      discard[0] = "TransactionIN";
      discard[1] = "TransactionOUT";
    }
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/remittance/ad_actionbutton/SelectOrdersOrInvoices", discard)
        .createXmlDocument();

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    // classInfo.name not loaded for Processes
    // xmlDocument.setParameter("title", classInfo.name);
    OBContext.setAdminMode();
    try {
      xmlDocument.setParameter("title", dao.getObject(Process.class, classInfo.id).getIdentifier());
      xmlDocument.setParameter("precision", remittance.getFinancialAccount().getCurrency()
          .getStandardPrecision().toString());
      xmlDocument.setParameter("currency", remittance.getFinancialAccount().getCurrency().getId());
    } finally {
      OBContext.restorePreviousMode();
    }

    xmlDocument.setParameter("windowId", strWindowId);
    xmlDocument.setParameter("tabId", strTabId);
    xmlDocument.setParameter("orgId", remittance.getOrganization().getId());
    xmlDocument.setParameter("remittanceId", strRemittanceId);
    xmlDocument.setParameter("remittanceNo", remittance.getIdentifier());
    xmlDocument.setParameter("paymentMethod", remittance.getRemittanceType().getPaymentMethod()
        .getName());
    xmlDocument.setParameter("isreceipt", strIsReceipt);
    xmlDocument.setParameter("isMulticurrencyPayIn",
        isMulticurrencyPaymentAllowed(remittance, true));
    xmlDocument.setParameter("isMulticurrencyPayOut",
        isMulticurrencyPaymentAllowed(remittance, false));
    // TODO:Review this
    if (remittance.getDueDate() != null) {
      xmlDocument.setParameter("dueDate",
          Utility.formatDate(remittance.getDueDate(), vars.getJavaDateFormat()));
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printGrid(HttpServletResponse response, VariablesSecureApp vars,
      String strRemittanceId, String _strSelectedPaymentDetails, boolean showAlternativePM,
      boolean showAlternativeCurrency, String strBPartnerId, String strDocumentType,
      String strTransactionType) throws IOException, ServletException {
    log4j.debug("Output: Grid with pending payments");

    dao = new AdvPaymentMngtDao();
    String strSelectedPaymentDetails = _strSelectedPaymentDetails;
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/remittance/ad_actionbutton/SelectOrdersOrInvoicesGrid")
        .createXmlDocument();

    Remittance remittance = dao.getObject(Remittance.class, strRemittanceId);
    List<FIN_PaymentScheduleDetail> selectedScheduledPaymentDetails = new ArrayList<FIN_PaymentScheduleDetail>();
    boolean firstLoad = false;
    if (strSelectedPaymentDetails == null || "".equals(strSelectedPaymentDetails))
      firstLoad = true;
    if (firstLoad) {
      selectedScheduledPaymentDetails = REM_AddRemittance
          .getSelectedPendingPaymentsFromRemittance(remittance);
      strSelectedPaymentDetails = Utility.getInStrList(selectedScheduledPaymentDetails);
    } else {
      selectedScheduledPaymentDetails = FIN_AddPayment.getSelectedPaymentDetails(null,
          strSelectedPaymentDetails);
    }

    Date expectedDate = remittance.getDueDate();
    if (expectedDate != null) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(DateUtils.truncate(remittance.getDueDate(), Calendar.DATE));
      cal.add(Calendar.DATE, 1);
      expectedDate = cal.getTime();
    }
    // IsReceipt:
    // Y: it just allows IN
    // N: It just allows OUT
    String isReceipt = strTransactionType;
    // filtered scheduled payments list
    final List<FIN_PaymentScheduleDetail> filteredScheduledPaymentDetails = REM_AddRemittance
        .getFilteredScheduledPaymentDetails(remittance.getOrganization(), remittance,
            showAlternativeCurrency, null, expectedDate, strDocumentType, showAlternativePM ? null
                : remittance.getRemittanceType().getPaymentMethod(), isReceipt, strBPartnerId,
            selectedScheduledPaymentDetails);

    FieldProvider[] data = REM_AddRemittance.getShownScheduledPaymentDetails(vars,
        strSelectedPaymentDetails, selectedScheduledPaymentDetails,
        filteredScheduledPaymentDetails, firstLoad, remittance, strDocumentType);
    if (!"B".equals(strDocumentType)) {
      data = groupPerDocumentType(data, strDocumentType);
    }
    xmlDocument.setData("structure", (data == null) ? set() : data);

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private FieldProvider[] groupPerDocumentType(FieldProvider[] data, String strDocumenType) {
    ArrayList<FieldProvider> list = new ArrayList<FieldProvider>();
    HashMap<String, Integer> map = new HashMap<String, Integer>();
    String groupingField = "finScheduledPaymentDetailId";
    if ("I".equals(strDocumenType)) {
      groupingField = "invoicePaymentScheduleId";
    } else if ("O".equals(strDocumenType)) {
      groupingField = "orderPaymentScheduleId";
    }
    for (int i = 0; i < data.length; i++) {
      if (!map.containsKey(data[i].getField(groupingField))
          || "".equals(data[i].getField(groupingField))) {
        map.put(data[i].getField(groupingField), list.size());
        FieldProviderFactory.setField(data[i], "rownum", String.valueOf(list.size()));
        list.add(data[i]);
      } else {
        if (map.get(data[i].getField(groupingField)) != null
            && new BigDecimal(list.get(map.get(data[i].getField(groupingField))).getField(
                "outstandingAmount")).add(new BigDecimal(data[i].getField("outstandingAmount")))
                .signum() == 0) {
          map.remove(data[i].getField(groupingField));
          FieldProvider elementToRemove = null;
          for (FieldProvider element : list) {
            if (element.getField(groupingField).equals(data[i].getField(groupingField))) {
              elementToRemove = element;
              break;
            }
          }
          list.remove(elementToRemove);
          continue;
        }
        FieldProviderFactory.setField(
            list.get(map.get(data[i].getField(groupingField))),
            "finScheduledPaymentDetailId",
            list.get(map.get(data[i].getField(groupingField))).getField(
                "finScheduledPaymentDetailId")
                + "," + data[i].getField("finScheduledPaymentDetailId"));
        FieldProviderFactory.setField(
            list.get(map.get(data[i].getField(groupingField))),
            "finSelectedPaymentDetailId",
            list.get(map.get(data[i].getField(groupingField))).getField(
                "finSelectedPaymentDetailId")
                + "," + data[i].getField("finScheduledPaymentDetailId"));
        FieldProviderFactory.setField(
            list.get(map.get(data[i].getField(groupingField))),
            "outstandingAmount",
            new BigDecimal(list.get(map.get(data[i].getField(groupingField))).getField(
                "outstandingAmount")).add(new BigDecimal(data[i].getField("outstandingAmount")))
                .toString());
        BigDecimal payAmount = BigDecimal.ZERO;
        if (!""
            .equals(list.get(map.get(data[i].getField(groupingField))).getField("paymentAmount"))) {
          payAmount = new BigDecimal(list.get(map.get(data[i].getField(groupingField))).getField(
              "paymentAmount"));
        }
        FieldProviderFactory.setField(
            list.get(map.get(data[i].getField(groupingField))),
            "paymentAmount",
            !"".equals(data[i].getField("paymentAmount")) ? payAmount.add(
                new BigDecimal(data[i].getField("paymentAmount"))).toString() : (payAmount
                .compareTo(BigDecimal.ZERO) == 0 ? "" : payAmount.toString()));
        if ("O".equals(strDocumenType)) {
          String strGroupedInvoicesNr = list.get(map.get(data[i].getField(groupingField)))
              .getField("invoiceNr");
          FieldProviderFactory.setField(list.get(map.get(data[i].getField(groupingField))),
              "invoiceNr", (strGroupedInvoicesNr.isEmpty() ? "" : strGroupedInvoicesNr + ", ")
                  + data[i].getField("invoiceNr"));
          String invoiceNumber = list.get(map.get(data[i].getField(groupingField))).getField(
              "invoiceNr");
          String invoiceNumberTrunc = (invoiceNumber.length() > 9) ? invoiceNumber.substring(0, 6)
              .concat("...").toString() : invoiceNumber;
          FieldProviderFactory.setField(list.get(map.get(data[i].getField(groupingField))),
              "invoiceNrTrunc", invoiceNumberTrunc);
        } else {
          String strGroupedOrdersNr = list.get(map.get(data[i].getField(groupingField))).getField(
              "orderNr");
          FieldProviderFactory.setField(
              list.get(map.get(data[i].getField(groupingField))),
              "orderNr",
              (strGroupedOrdersNr.isEmpty() ? "" : strGroupedOrdersNr + ", ")
                  + data[i].getField("orderNr"));
          String orderNumber = list.get(map.get(data[i].getField(groupingField))).getField(
              "orderNr");
          String orerNumberTrunc = (orderNumber.length() > 9) ? orderNumber.substring(0, 6)
              .concat("...").toString() : orderNumber;
          FieldProviderFactory.setField(list.get(map.get(data[i].getField(groupingField))),
              "orderNrTrunc", orerNumberTrunc);
        }
      }
    }
    FieldProvider[] result = new FieldProvider[list.size()];
    list.toArray(result);
    return result;
  }

  private void saveAndClose(HttpServletResponse response, VariablesSecureApp vars,
      String strRemittanceId, String strSelectedScheduledPaymentDetailIds, String strTabId,
      String strDifferenceAction, String strDifferenceAmount) throws IOException, ServletException {
    OBError message = new OBError();
    String strMessageType = "Success";
    String strMessage = "";
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {
      Remittance remittance = new AdvPaymentMngtDao().getObject(Remittance.class, strRemittanceId);
      // Initialize payment proposal deleting existing ones.
      List<String> linesToDelete = new ArrayList<String>();
      List<RemittanceLine> lines = remittance.getREMRemittanceLineList();
      for (RemittanceLine line : lines) {
        if (line.getPayment() == null) {
          linesToDelete.add(line.getId());
        }
      }
      for (String strLineId : linesToDelete) {
        lines.remove(dao.getObject(RemittanceLine.class, strLineId));
        OBDal.getInstance().remove(dao.getObject(RemittanceLine.class, strLineId));
      }
      remittance.setREMRemittanceLineList(lines);
      OBDal.getInstance().save(remittance);
      OBDal.getInstance().flush();
      OBDal.getInstance().getSession().refresh(remittance);

      if (!"".equals(strSelectedScheduledPaymentDetailIds)) {
        List<FIN_PaymentScheduleDetail> selectedPaymentScheduleDetails = FIN_Utility
            .getOBObjectList(FIN_PaymentScheduleDetail.class, strSelectedScheduledPaymentDetailIds);
        HashMap<String, BigDecimal> selectedPaymentScheduleDetailAmounts = REM_AddRemittance
            .getSelectedPaymentDetailsAndAmount(vars, strSelectedScheduledPaymentDetailIds);

        REM_AddRemittance.saveRemittance(remittance, selectedPaymentScheduleDetails,
            selectedPaymentScheduleDetailAmounts,
            strDifferenceAction.equals("writeoff") ? new BigDecimal(strDifferenceAmount) : null);

        boolean warningDifferentPaymentMethod = false;
        for (FIN_PaymentScheduleDetail payScheDe : selectedPaymentScheduleDetails) {
          if ((payScheDe.getInvoicePaymentSchedule() != null)
              && !remittance.getRemittanceType().getPaymentMethod().getId()
                  .equals(payScheDe.getInvoicePaymentSchedule().getFinPaymentmethod().getId())) {
            warningDifferentPaymentMethod = true;
            break;
          } else if ((payScheDe.getOrderPaymentSchedule() != null)
              && !remittance.getRemittanceType().getPaymentMethod().getId()
                  .equals(payScheDe.getOrderPaymentSchedule().getFinPaymentmethod().getId())) {
            warningDifferentPaymentMethod = true;
            break;
          }
        }

        strMessage = selectedPaymentScheduleDetails.size() + " " + "@RowsInserted@";
        if (warningDifferentPaymentMethod) {
          strMessage += ". @REM_Different_PaymentMethod_Selected@";
          strMessageType = "Warning";
        }
      }

    } finally {
      OBContext.restorePreviousMode();
    }

    message.setType(strMessageType);
    message.setTitle(Utility.messageBD(this, strMessageType, vars.getLanguage()));
    message.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), strMessage));
    vars.setMessage(strTabId, message);
    message = null;

    printPageClosePopUpAndRefreshParent(response, vars);
  }

  private FieldProvider[] set() throws ServletException {
    HashMap<String, String> empty = new HashMap<String, String>();
    empty.put("finScheduledPaymentId", "");
    empty.put("salesOrderNr", "");
    empty.put("salesInvoiceNr", "");
    empty.put("expectedDate", "");
    empty.put("dueDate", "");
    empty.put("invoicedAmount", "");
    empty.put("expectedAmount", "");
    empty.put("paymentAmount", "");
    ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
    result.add(empty);
    return FieldProviderFactory.getFieldProviderArray(result);
  }

  private String isReceiptRemittance(Remittance remittance) {
    OBCriteria<FinAccPaymentMethod> finAccPaymentMethods = OBDal.getInstance().createCriteria(
        FinAccPaymentMethod.class);
    finAccPaymentMethods.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT,
        remittance.getFinancialAccount()));
    finAccPaymentMethods.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD, remittance
        .getRemittanceType().getPaymentMethod()));
    List<FinAccPaymentMethod> pms = finAccPaymentMethods.list();
    if (pms.size() == 0)
      throw new OBException(FIN_Utility.messageBD("REM_PMNotInFinancialAccount"));
    FinAccPaymentMethod finAccPaymentMethod = pms.get(0);
    if (finAccPaymentMethod.isPayinAllow() && !finAccPaymentMethod.isPayoutAllow()) {
      return "Y";
    }
    if (!finAccPaymentMethod.isPayinAllow() && finAccPaymentMethod.isPayoutAllow()) {
      return "N";
    }
    if (finAccPaymentMethod.isPayinAllow() && finAccPaymentMethod.isPayoutAllow()) {
      return "B";
    } else {
      return "";
    }
  }

  private String isMulticurrencyPaymentAllowed(Remittance remittance, boolean paymentIn) {
    try {
      OBContext.setAdminMode(true);
      String isMulticurrencyPaymentAllowed = "N";
      OBCriteria<FinAccPaymentMethod> finAccPaymentMethods = OBDal.getInstance().createCriteria(
          FinAccPaymentMethod.class);
      finAccPaymentMethods.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT,
          remittance.getFinancialAccount()));
      finAccPaymentMethods.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD,
          remittance.getRemittanceType().getPaymentMethod()));
      finAccPaymentMethods.setMaxResults(1);
      List<FinAccPaymentMethod> pms = finAccPaymentMethods.list();
      if (!pms.isEmpty()) {
        FinAccPaymentMethod finAccPaymentMethod = pms.get(0);
        if (paymentIn && finAccPaymentMethod.isPayinAllow()
            && finAccPaymentMethod.isPayinIsMulticurrency()) {
          isMulticurrencyPaymentAllowed = "Y";
        }
        if (!paymentIn && finAccPaymentMethod.isPayoutAllow()
            && finAccPaymentMethod.isPayoutIsMulticurrency()) {
          isMulticurrencyPaymentAllowed = "Y";
        }
      }
      return isMulticurrencyPaymentAllowed;
    } finally {
      OBContext.restorePreviousMode();
    }

  }

  public String getServletInfo() {
    return "Servlet that presents the payment proposal";
    // end of getServletInfo() method
  }

}
