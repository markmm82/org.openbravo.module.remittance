/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.module.remittance.hooks;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.advpaymentmngt.ModifyPaymentPlanHook;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.module.remittance.RemittanceLine;

/**
 * Modified Payment Plan hook implementation for Remittance module
 * 
 * It validates that it is not possible to change the due date of a Payment Schedule Detail to a
 * date after the expected date when this Payment Schedule Detail is already included in a
 * Remittance (or has a Payment included in a Remittance)
 */
@ApplicationScoped
public class REMModifyPaymentPlanHook extends ModifyPaymentPlanHook {

  @Override
  public void validatePaymentSchedule(List<FIN_PaymentSchedule> modifiedPaymentSchedule) {
    for (FIN_PaymentSchedule psd : modifiedPaymentSchedule) {
      if (isIncludedInARemmitance(psd)) {
        throw new OBException("APRM_remline_finpayschdet");
      }
    }
  }

  private boolean isIncludedInARemmitance(FIN_PaymentSchedule psd) {
    StringBuilder hql = new StringBuilder();
    hql.append(" payment.id in (select finPayment.id ");
    hql.append("                  from FIN_Payment_Detail pd ");
    hql.append("                  where pd.id in (select psd.paymentDetails.id ");
    hql.append("                                  from FIN_Payment_ScheduleDetail psd ");
    hql.append("                                  where psd.invoicePaymentSchedule.id = :paymentScheduleId and canceled = false) ) ");
    hql.append(" or paymentScheduleDetail.id in (select psd.id ");
    hql.append("                                   from FIN_Payment_ScheduleDetail psd ");
    hql.append("                                   where psd.invoicePaymentSchedule.id = :paymentScheduleId and canceled = false) ");

    OBQuery<RemittanceLine> query = OBDal.getInstance().createQuery(RemittanceLine.class,
        hql.toString());
    query.setNamedParameter("paymentScheduleId", psd.getId());
    query.setMaxResult(1);
    return query.uniqueResult() != null;
  }

}
