/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.remittance.ad_forms;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocFINPayment;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.DocLine_FINPayment;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.CashVATUtil;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.currency.ConversionRate;
import org.openbravo.model.common.currency.ConversionRateDoc;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.AcctSchemaTableDocType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaTable;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLineCancel;
import org.openbravo.module.remittance.RemittanceType;
import org.openbravo.module.remittance.RemittanceTypeAccount;

public class DocREMRemittanceCancel extends AcctServer {

  @SuppressWarnings("unused")
  private static final long serialVersionUID = 1L;
  static Logger log4j = Logger.getLogger(DocREMRemittanceCancel.class);
  public static final String DOCTYPE_RemittanceCancel = "REM_REMCANCEL";
  String SeqNo = "0";
  String generatedAmount = "";
  String usedAmount = "";
  List<DocLine_FINPayment> docPaymentLines = new ArrayList<DocLine_FINPayment>();

  public DocREMRemittanceCancel() {
  }

  public DocREMRemittanceCancel(String AD_Client_ID, String AD_Org_ID,
      ConnectionProvider connectionProvider) {
    super(AD_Client_ID, AD_Org_ID, connectionProvider);
  }

  @Override
  public boolean loadDocumentDetails(FieldProvider[] data, ConnectionProvider conn) {
    DocumentType = DOCTYPE_RemittanceCancel;
    DateDoc = data[0].getField("PaymentDate");
    Amounts[AMTTYPE_Gross] = data[0].getField("Amount");
    generatedAmount = data[0].getField("GeneratedCredit");
    usedAmount = data[0].getField("UsedCredit");
    docPaymentLines = loadDocLines_FINPayment(data[0].getField("FIN_Payment_ID"));
    loadDocumentType();
    return true;
  }

  @Override
  public Fact createFact(AcctSchema as, ConnectionProvider conn, Connection con,
      VariablesSecureApp vars) throws ServletException {
    // Select specific definition
    String strClassname = "";
    final StringBuilder whereClause = new StringBuilder();
    Fact fact = new Fact(this, as, Fact.POST_Actual);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    OBContext.setAdminMode();
    try {
      whereClause.append(" as astdt ");
      whereClause.append(
          " where astdt.acctschemaTable.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      whereClause.append(" and astdt.acctschemaTable.table.id = '" + AD_Table_ID + "'");
      whereClause.append(" and astdt.documentCategory = '" + DocumentType + "'");

      final OBQuery<AcctSchemaTableDocType> obqParameters = OBDal.getInstance()
          .createQuery(AcctSchemaTableDocType.class, whereClause.toString());
      final List<AcctSchemaTableDocType> acctSchemaTableDocTypes = obqParameters.list();

      if (acctSchemaTableDocTypes != null && acctSchemaTableDocTypes.size() > 0) {
        strClassname = acctSchemaTableDocTypes.get(0).getCreatefactTemplate().getClassname();
      }

      if (strClassname.equals("")) {
        final StringBuilder whereClause2 = new StringBuilder();

        whereClause2.append(" as ast ");
        whereClause2.append(" where ast.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
        whereClause2.append(" and ast.table.id = '" + AD_Table_ID + "'");

        final OBQuery<AcctSchemaTable> obqParameters2 = OBDal.getInstance()
            .createQuery(AcctSchemaTable.class, whereClause2.toString());
        final List<AcctSchemaTable> acctSchemaTables = obqParameters2.list();
        if (acctSchemaTables != null && acctSchemaTables.size() > 0
            && acctSchemaTables.get(0).getCreatefactTemplate() != null) {
          strClassname = acctSchemaTables.get(0).getCreatefactTemplate().getClassname();
        }
      }
      if (!strClassname.equals("")) {
        try {
          DocREMRemittanceCancelTemplate newTemplate = (DocREMRemittanceCancelTemplate) Class
              .forName(strClassname)
              .getDeclaredConstructor()
              .newInstance();
          return newTemplate.createFact(this, as, conn, con, vars);
        } catch (Exception e) {
          log4j.error("Error while creating new instance for DocREMRemittanceCancelTemplate - ", e);
        }
      }

      RemittanceLineCancel remittanceLineCancel = OBDal.getInstance()
          .get(RemittanceLineCancel.class, Record_ID);
      FIN_Payment payment = remittanceLineCancel.getPayment();
      String currency = C_Currency_ID;
      String conversionDate = null;
      BigDecimal convertedConsolidatedAmount = BigDecimal.ZERO;
      BigDecimal convRate = BigDecimal.ONE;
      BigDecimal convRateDivide = BigDecimal.ONE;
      BigDecimal convertedPaymentAmounts = BigDecimal.ZERO;
      if (as.m_C_Currency_ID != null && !as.m_C_Currency_ID.equals(currency)) {
        conversionDate = OBDateUtils.formatDate(remittanceLineCancel.getAccountingDate());
        ConversionRate cr = FinancialUtils.getConversionRate(
            remittanceLineCancel.getAccountingDate(),
            OBDal.getInstance().get(Currency.class, currency),
            OBDal.getInstance().get(Currency.class, as.m_C_Currency_ID), payment.getOrganization(),
            payment.getClient());
        convRate = cr.getMultipleRateBy();
        convRateDivide = cr.getDivideRateBy();
        convertedConsolidatedAmount = (new BigDecimal(Amounts[AMTTYPE_Gross])).multiply(convRate);
      }

      // Line created for bank entry to associate a different business partner
      DocLine line = new DocLine(DocumentType, Record_ID, Record_ID);
      line.m_C_BPartner_ID = remittanceLineCancel.getRemittance().getPayment() != null
          ? remittanceLineCancel.getRemittance().getPayment().getBusinessPartner().getId()
          : "";

      // consolidated
      if (!remittanceLineCancel.getRemittance().getRemittanceType().isRemitForDiscount()) {
        fact.createLine(null, getAccount(conn, as, false), currency,
            (payment.isReceipt() ? Amounts[AMTTYPE_Gross] : ""),
            (payment.isReceipt() ? "" : Amounts[AMTTYPE_Gross]), Fact_Acct_Group_ID, "999999",
            DocumentType, conversionDate, null, conn);
      } else {
        fact.createLine(line,
            getAccountGLItem(remittanceLineCancel.getRemittance().getRemittanceType().getGLItem(),
                as, payment.isReceipt(), conn),
            currency, (payment.isReceipt() ? Amounts[AMTTYPE_Gross] : ""),
            (payment.isReceipt() ? "" : Amounts[AMTTYPE_Gross]), Fact_Acct_Group_ID, "999999",
            DocumentType, conversionDate, null, conn);
      }

      for (final DocLine_FINPayment docPaymentLine : docPaymentLines) {
        // Select conversion rate defined in order or in invoice, if any, or select conversion date
        // of transaction
        String faCurrency = remittanceLineCancel.getRemittance()
            .getFinancialAccount()
            .getCurrency()
            .getId();
        BigDecimal conversionRate = null;
        String conversionDateTrx = null;
        String lineAmount = docPaymentLine.getAmountExcludingCredit();
        if (as.m_C_Currency_ID != null && !as.m_C_Currency_ID.equals(currency)) {
          if (docPaymentLine.getOrder() != null) {
            conversionDateTrx = OBDateUtils
                .formatDate(remittanceLineCancel.getRemittance().getTransactionDate());
          } else {
            if (docPaymentLine.getInvoice() != null) {
              Invoice invoice = docPaymentLine.getInvoice();
              if (invoice.getCurrencyConversionRateDocList() != null
                  && !invoice.getCurrencyConversionRateDocList().isEmpty()) {
                for (ConversionRateDoc rateDoc : invoice.getCurrencyConversionRateDocList()) {
                  if (currency.equals(rateDoc.getCurrency().getId())
                      && as.m_C_Currency_ID.equals(rateDoc.getToCurrency().getId())) {
                    conversionRate = rateDoc.getRate();
                    break;
                  }
                }
              } else {
                conversionDateTrx = OBDateUtils.formatDate(invoice.getAccountingDate());
              }
            } else {
              if (as.m_C_Currency_ID.equals(faCurrency)
                  && payment.getFinancialTransactionConvertRate() != null) {
                // Select conversion rate of payment header, if applies
                conversionRate = payment.getFinancialTransactionConvertRate();
              }
            }
          }
          if (conversionRate != null || conversionDateTrx != null) {
            BigDecimal convertedAmount = new BigDecimal(0);
            if (conversionRate != null) {
              convertedAmount = new BigDecimal(lineAmount).multiply(conversionRate);
            } else if (conversionDateTrx != null) {
              try {
                convertedAmount = FinancialUtils.getConvertedAmount(new BigDecimal(lineAmount),
                    OBDal.getInstance().get(Currency.class, currency),
                    OBDal.getInstance().get(Currency.class, as.m_C_Currency_ID),
                    OBDateUtils.getDate(conversionDateTrx), payment.getOrganization(),
                    FinancialUtils.PRECISION_PRICE);
              } catch (OBException e) {
                // TODO Auto-generated catch block
                log4j.error("Error in createFact: ", e);
              } catch (ParseException e) {
                // TODO Auto-generated catch block
                log4j.error("Error in createFact: ", e);
              }
            }
            convertedPaymentAmounts = convertedPaymentAmounts.add(convertedAmount);
          }
        }

        // Lines created for payment details to associated different business partner
        DocLine line2 = new DocLine(DocumentType, Record_ID, Record_ID);
        line2.m_C_BPartner_ID = docPaymentLine.getInvoice() != null
            ? docPaymentLine.getInvoice().getBusinessPartner().getId()
            : docPaymentLine.getOrder() != null
                ? docPaymentLine.getOrder().getBusinessPartner().getId()
                : "";
        line2.m_C_Costcenter_ID = docPaymentLine.m_C_Costcenter_ID;
        line2.m_M_Product_ID = docPaymentLine.m_M_Product_ID;
        line2.m_User1_ID = docPaymentLine.m_User1_ID;
        line2.m_User2_ID = docPaymentLine.m_User2_ID;
        line2.m_C_Project_ID = docPaymentLine.m_C_Project_ID;

        fact.createLine(line2, getAccount(conn, as, true), currency,
            (payment.isReceipt() ? "" : lineAmount), (payment.isReceipt() ? lineAmount : ""),
            Fact_Acct_Group_ID, "999999", DocumentType, conversionDateTrx, conversionRate, conn);

        // Cash VAT
        if (docPaymentLine != null && docPaymentLine.getInvoice() != null
            && !"Y".equals(docPaymentLine.getIsPrepayment())) {
          CashVATUtil.createFactCashVAT(as, conn, fact, Fact_Acct_Group_ID, docPaymentLine,
              docPaymentLine.getInvoice(), DocumentType, SeqNo);
        }

      }

      // Manage conversion rates differences gains or losses
      if (as.m_C_Currency_ID != null && !as.m_C_Currency_ID.equals(currency)) {
        BigDecimal convertedAmtDiff = convertedPaymentAmounts.subtract(convertedConsolidatedAmount);
        BigDecimal amtDiff = convertedAmtDiff.multiply(convRateDivide);
        if (!payment.isReceipt() && amtDiff.compareTo(BigDecimal.ZERO) == 1
            || payment.isReceipt() && amtDiff.compareTo(BigDecimal.ZERO) == -1) {
          fact.createLine(null, getAccount(AcctServer.ACCTTYPE_ConvertGainDefaultAmt, as, conn),
              currency, "", amtDiff.abs().toString(), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              DocumentType, conn);
        } else {
          fact.createLine(null, getAccount(AcctServer.ACCTTYPE_ConvertChargeDefaultAmt, as, conn),
              currency, amtDiff.abs().toString(), "", Fact_Acct_Group_ID, nextSeqNo(SeqNo),
              DocumentType, conn);
        }
      }

    } finally {
      OBContext.restorePreviousMode();
    }

    SeqNo = "0";
    return fact;
  }

  public String nextSeqNo(String oldSeqNo) {
    BigDecimal seqNo = new BigDecimal(oldSeqNo);
    SeqNo = (seqNo.add(new BigDecimal("10"))).toString();
    return SeqNo;
  }

  /**
   * Get Source Currency Balance - subtracts line amounts from total - no rounding As this document
   * has just one line always maps the grand total so ZERO is returned
   * 
   * @return positive amount, if total is bigger than lines
   */
  @Override
  public BigDecimal getBalance() {
    BigDecimal retValue = ZERO;
    return retValue;
  } // getBalance

  @Override
  public boolean getDocumentConfirmation(ConnectionProvider conn, String strRecordId) {
    // Add here a check if needed to confirm document posting
    boolean confirmation = true;
    RemittanceLineCancel remittanceLineCancel = OBDal.getInstance()
        .get(RemittanceLineCancel.class, strRecordId);
    if (!remittanceLineCancel.getRemittance().getRemittanceType().isPostingAllowed()) {
      confirmation = false;
    }
    if (!confirmation) {
      setStatus(STATUS_DocumentDisabled);
    }
    return confirmation;
  }

  @Override
  public void loadObjectFieldProvider(ConnectionProvider conn, String strAD_Client_ID, String id)
      throws ServletException {
    RemittanceLineCancel remittanceCancel = OBDal.getInstance().get(RemittanceLineCancel.class, id);
    FIN_Payment payment = remittanceCancel.getPayment();
    FieldProviderFactory[] data = new FieldProviderFactory[1];
    data[0] = new FieldProviderFactory(null);
    FieldProviderFactory.setField(data[0], "AD_Client_ID", remittanceCancel.getClient().getId());
    FieldProviderFactory.setField(data[0], "AD_Org_ID", remittanceCancel.getOrganization().getId());
    FieldProviderFactory.setField(data[0], "C_BPartner_ID",
        payment.getBusinessPartner() != null ? payment.getBusinessPartner().getId() : "");
    // TODO: Review numbering to change for remittance numbering
    FieldProviderFactory.setField(data[0], "DocumentNo", payment.getDocumentNo());
    String dateFormat = OBPropertiesProvider.getInstance()
        .getOpenbravoProperties()
        .getProperty("dateFormat.java");
    SimpleDateFormat outputFormat = new SimpleDateFormat(dateFormat);
    FieldProviderFactory.setField(data[0], "DateAcct",
        outputFormat.format(remittanceCancel.getAccountingDate()));
    // FieldProviderFactory.setField(data[0], "C_DocType_ID", payment.getDocumentType().getId());
    FieldProviderFactory.setField(data[0], "C_Currency_ID", payment.getCurrency().getId());
    FieldProviderFactory.setField(data[0], "Amount", payment.getAmount().toString());
    FieldProviderFactory.setField(data[0], "GeneratedCredit",
        payment.getGeneratedCredit().toString());
    FieldProviderFactory.setField(data[0], "UsedCredit", payment.getUsedCredit().toString());
    FieldProviderFactory.setField(data[0], "WriteOffAmt", payment.getWriteoffAmount().toString());
    FieldProviderFactory.setField(data[0], "Description", payment.getDescription());
    FieldProviderFactory.setField(data[0], "Posted", remittanceCancel.getPosted());
    FieldProviderFactory.setField(data[0], "Processed", remittanceCancel.isProcessed() ? "Y" : "N");
    FieldProviderFactory.setField(data[0], "Processing",
        remittanceCancel.isProcessNow() ? "Y" : "N");
    FieldProviderFactory.setField(data[0], "C_Project_ID",
        payment.getProject() != null ? payment.getProject().getId() : "");
    FieldProviderFactory.setField(data[0], "C_Campaign_ID",
        payment.getSalesCampaign() != null ? payment.getSalesCampaign().getId() : "");
    FieldProviderFactory.setField(data[0], "C_Activity_ID",
        payment.getActivity() != null ? payment.getActivity().getId() : "");
    FieldProviderFactory.setField(data[0], "FIN_Payment_ID", payment.getId());
    setObjectFieldProvider(data);
  }

  /*
   * Retrieves Account for receipt / Payment: both for sent account and canceled account
   */
  public Account getAccount(ConnectionProvider conn, AcctSchema as, boolean bIsSentAccout)
      throws ServletException {
    RemittanceLineCancel remittanceCancel = OBDal.getInstance()
        .get(RemittanceLineCancel.class, Record_ID);
    Remittance remittance = remittanceCancel.getRemittance();
    OBContext.setAdminMode();
    Account account = null;
    try {
      OBCriteria<RemittanceTypeAccount> accounts = OBDal.getInstance()
          .createCriteria(RemittanceTypeAccount.class);
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_ACCOUNTINGSCHEMA,
          OBDal.getInstance()
              .get(org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
                  as.getC_AcctSchema_ID())));
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_REMITTANCETYPE,
          OBDal.getInstance().get(RemittanceType.class, remittance.getRemittanceType().getId())));
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_ACTIVE, true));
      accounts.setFilterOnReadableClients(false);
      accounts.setFilterOnReadableOrganization(false);
      accounts.setMaxResults(1);
      List<RemittanceTypeAccount> accountList = accounts.list();
      if (accountList == null || accountList.size() == 0) {
        return null;
      }
      if (bIsSentAccout) {
        account = Account.getAccount(conn, accountList.get(0).getSentAccount().getId());
      } else {
        account = Account.getAccount(conn, accountList.get(0).getSettlementAccount().getId());
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return account;
  }

  public String getSeqNo() {
    return SeqNo;
  }

  public void setSeqNo(String seqNo) {
    SeqNo = seqNo;
  }

  public String getGeneratedAmount() {
    return generatedAmount;
  }

  public void setGeneratedAmount(String generatedAmount) {
    this.generatedAmount = generatedAmount;
  }

  public String getUsedAmount() {
    return usedAmount;
  }

  public void setUsedAmount(String usedAmount) {
    this.usedAmount = usedAmount;
  }

  private List<DocLine_FINPayment> loadDocLines_FINPayment(final String paymentId) {
    List<DocLine_FINPayment> list = new ArrayList<DocLine_FINPayment>();
    FieldProviderFactory[] data = new DocFINPayment().loadLinesFieldProvider(paymentId);
    if (data == null || data.length == 0) {
      return null;
    }
    for (int i = 0; i < data.length; i++) {
      if (data[i] == null) {
        continue;
      }
      String Line_ID = data[i].getField("FIN_Payment_Detail_ID");
      OBContext.setAdminMode(true);
      try {
        FIN_PaymentDetail detail = OBDal.getInstance().get(FIN_PaymentDetail.class, Line_ID);
        DocLine_FINPayment docLine = new DocLine_FINPayment(DocumentType, Record_ID, Line_ID);
        docLine.loadAttributes(data[i], this);
        docLine.setAmount(data[i].getField("Amount"));
        docLine.setAmountExcludingCredit(data[i].getField("AmountExcludingCredit"));
        docLine.setIsPrepayment(data[i].getField("isprepayment"));
        docLine.setWriteOffAmt(data[i].getField("WriteOffAmt"));
        docLine.setDoubtFulDebtAmount(new BigDecimal(data[i].getField("DoubtFulDebtAmount")));
        docLine.setC_GLItem_ID(data[i].getField("C_GLItem_ID"));
        docLine.setPrepaymentAgainstInvoice(
            "Y".equals(data[i].getField("isPaymentDatePriorToInvoiceDate")) ? true : false);
        docLine.setInvoiceId(detail.getFINPaymentScheduleDetailList() != null
            && detail.getFINPaymentScheduleDetailList().get(0).getInvoicePaymentSchedule() != null
                ? detail.getFINPaymentScheduleDetailList()
                    .get(0)
                    .getInvoicePaymentSchedule()
                    .getInvoice()
                    .getId()
                : null);
        docLine.setOrderId(detail.getFINPaymentScheduleDetailList() != null
            && detail.getFINPaymentScheduleDetailList().get(0).getOrderPaymentSchedule() != null
                ? detail.getFINPaymentScheduleDetailList()
                    .get(0)
                    .getOrderPaymentSchedule()
                    .getOrder()
                    .getId()
                : null);
        docLine.m_Record_Id2 = data[i].getField("recordId2");
        docLine.setInvoiceTaxCashVAT_V(Line_ID);
        docLine.setInvoiceTaxCashVAT_V(data[i].getField("MergedPaymentDetailId"));
        list.add(docLine);
      } finally {
        OBContext.restorePreviousMode();
      }
    }
    return list;
  }

  boolean isPaymentDatePriorToInvoiceDate(FIN_PaymentDetail paymentDetail) {
    List<FIN_PaymentScheduleDetail> schedDetails = paymentDetail.getFINPaymentScheduleDetailList();
    if (schedDetails.size() == 0) {
      return false;
    } else {
      if (schedDetails.get(0).getInvoicePaymentSchedule() != null && schedDetails.get(0)
          .getInvoicePaymentSchedule()
          .getInvoice()
          .getAccountingDate()
          .after(paymentDetail.getFinPayment().getPaymentDate())) {
        return true;
      } else {
        return false;
      }
    }
  }
}
