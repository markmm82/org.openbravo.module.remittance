/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.remittance.ad_forms;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.ConversionRateDoc;
import org.openbravo.model.common.enterprise.AcctSchemaTableDocType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.accounting.Costcenter;
import org.openbravo.model.financialmgmt.accounting.FIN_FinancialAccountAccounting;
import org.openbravo.model.financialmgmt.accounting.UserDimension1;
import org.openbravo.model.financialmgmt.accounting.UserDimension2;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaTable;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.RemittanceTypeAccount;
import org.openbravo.module.remittance.utility.RemittanceUtils;

public class DocREMRemittance extends AcctServer {

  @SuppressWarnings("unused")
  private static final long serialVersionUID = 1L;
  static Logger log4j = Logger.getLogger(DocREMRemittance.class);

  String SeqNo = "0";

  public DocREMRemittance() {
  }

  public DocREMRemittance(String AD_Client_ID, String AD_Org_ID,
      ConnectionProvider connectionProvider) {
    super(AD_Client_ID, AD_Org_ID, connectionProvider);
  }

  @Override
  public boolean loadDocumentDetails(FieldProvider[] data, ConnectionProvider conn) {
    DateDoc = data[0].getField("PaymentDate");
    Amounts[AMTTYPE_Gross] = getTotalAmount();
    loadDocumentType();
    p_lines = loadLines();
    return true;
  }

  public FieldProviderFactory[] loadLinesFieldProvider(String Id) {
    BigDecimal totalAmount = BigDecimal.ZERO;
    OBContext.setAdminMode();
    try {
      Remittance remittance = OBDal.getInstance().get(Remittance.class, Id);
      List<RemittanceLine> lines = remittance.getREMRemittanceLineList();
      ArrayList<Object> list = new ArrayList<Object>();
      Set<String> paymentIds = new TreeSet<String>();
      for (RemittanceLine line : lines) {
        String remmitanceLineID = line.getId();
        FIN_Payment payment = line.getPayment();
        if (paymentIds.contains(payment.getId())) {
          continue;
        }
        paymentIds.add(payment.getId());
        List<String> paymentDetailsIds = FIN_Utility.getOrderedPaymentDetailList(payment.getId());

        if (paymentDetailsIds == null) {
          continue;
        }
        int noOfPaymentDetails = paymentDetailsIds.size();
        final BigDecimal usedCredit = payment.getUsedCredit();
        final BigDecimal creditAmtToAdjust = usedCredit.divide(new BigDecimal(noOfPaymentDetails),
            payment.getCurrency().getPricePrecision().intValue(), RoundingMode.HALF_UP);
        BigDecimal totalCreditAdjusted = BigDecimal.ZERO;

        final FieldProviderFactory[] data = new FieldProviderFactory[noOfPaymentDetails];
        FIN_PaymentSchedule ps = null;
        FIN_PaymentDetail pd = null;

        Object previousObj = null;
        for (int i = 0; i < data.length; i++) {
          FIN_PaymentDetail detail = OBDal.getInstance()
              .get(FIN_PaymentDetail.class, paymentDetailsIds.get(i));
          // Details refunded used credit are excluded as the entry will be created using the credit
          // used
          if (detail.isRefund() && detail.isPrepayment()) {
            continue;
          }
          // If the Payment Detail has already been processed, skip it
          if (detail.equals(pd)) {
            continue;
          }
          pd = detail;

          FIN_PaymentSchedule psi = detail.getFINPaymentScheduleDetailList()
              .get(0)
              .getInvoicePaymentSchedule();
          FIN_PaymentSchedule pso = detail.getFINPaymentScheduleDetailList()
              .get(0)
              .getOrderPaymentSchedule();
          // Related to Issue Issue 19567. Some Payment Detail's amount and writeoff amount are
          // merged into one.
          // https://issues.openbravo.com/view.php?id=19567
          HashMap<String, BigDecimal> amountAndWriteOff = getPaymentDetailIdWriteOffAndAmount(
              paymentDetailsIds, ps, psi, pso, i);
          BigDecimal amount = amountAndWriteOff.get("amount");
          BigDecimal writeOff = amountAndWriteOff.get("writeoff");
          FieldProviderFactory lineData = new FieldProviderFactory(null);
          if (amount == null) {
            lineData = null;
            ps = psi;
            continue;
          } else {
            if (previousObj != null
                && amountAndWriteOff.get("merged").compareTo(BigDecimal.ONE) == 0) {
              // keeps only the current line while merging the amounts
              list.remove(previousObj);
              totalAmount = totalAmount.add(amount);
            } else {
              totalAmount = amount;
            }
            BigDecimal amountExcludingCredit = totalAmount.subtract(creditAmtToAdjust);
            totalCreditAdjusted = totalCreditAdjusted.add(creditAmtToAdjust);
            if (i == noOfPaymentDetails - 1) { // Last Payment Detail
              amountExcludingCredit = amountExcludingCredit
                  .subtract(usedCredit.subtract(totalCreditAdjusted));
            }
            FieldProviderFactory.setField(lineData, "Amount", totalAmount.toString());
            FieldProviderFactory.setField(lineData, "AmountExcludingCredit",
                amountExcludingCredit.toString());
          }
          ps = psi;

          // Related to Issue 37430. While looping through the Payment Details, it is possible that
          // the related Remittance Line is not the same one as the one stored in the variable. In
          // this case the correct one must be retrieved.
          // https://issues.openbravo.com/view.php?id=37430
          String remittanceLineIDFromPaymentDetail = getRemittanceLineFromPaymentDetail(detail,
              remittance);
          if (remittanceLineIDFromPaymentDetail != null) {
            remmitanceLineID = remittanceLineIDFromPaymentDetail;
          }

          FieldProviderFactory.setField(lineData, "AD_Client_ID", detail.getClient().getId());
          FieldProviderFactory.setField(lineData, "AD_Org_ID", detail.getOrganization().getId());
          FieldProviderFactory.setField(lineData, "FIN_Payment_Detail_ID", detail.getId());
          FieldProviderFactory.setField(lineData, "REM_RemittanceLine_ID", remmitanceLineID);
          FieldProviderFactory.setField(lineData, "isReceipt",
              detail.getFinPayment().isReceipt() ? "Y" : "N");
          FieldProviderFactory.setField(lineData, "isprepayment",
              detail.isPrepayment() ? "Y" : "N");
          FieldProviderFactory.setField(lineData, "WriteOffAmt", writeOff.toString());
          FieldProviderFactory.setField(lineData, "C_GLItem_ID",
              detail.getGLItem() != null ? detail.getGLItem().getId() : "");
          FieldProviderFactory.setField(lineData, "Refund", detail.isRefund() ? "Y" : "N");
          FieldProviderFactory.setField(lineData, "isprepayment",
              detail.isPrepayment() ? "Y" : "N");
          FieldProviderFactory.setField(lineData, "cProjectId",
              detail.getFinPayment().getProject() != null
                  ? detail.getFinPayment().getProject().getId()
                  : "");

          Costcenter cCenter = RemittanceUtils.getCostCenterFromPaymentOrInvoiceOrOrder(detail);
          FieldProviderFactory.setField(lineData, "cCostcenterId",
              cCenter != null ? cCenter.getId() : "");
          FieldProviderFactory.setField(lineData, "mProductId",
              detail.getFINPaymentScheduleDetailList().get(0).getProduct() != null
                  ? detail.getFINPaymentScheduleDetailList().get(0).getProduct().getId()
                  : "");
          UserDimension1 userDimension1 = RemittanceUtils
              .getUserDimension1FromPaymentOrInvoiceOrOrder(detail);
          FieldProviderFactory.setField(lineData, "user1Id",
              userDimension1 != null ? userDimension1.getId() : "");
          UserDimension2 userDimension2 = RemittanceUtils
              .getUserDimension2FromPaymentOrInvoiceOrOrder(detail);
          FieldProviderFactory.setField(lineData, "user2Id",
              userDimension2 != null ? userDimension2.getId() : "");

          BusinessPartner bpartner = detail.getFinPayment().getBusinessPartner();
          if (bpartner == null) {
            if (RemittanceUtils.paymentDetailHasRelatedInvoice(detail)) {
              bpartner = detail.getFINPaymentScheduleDetailList()
                  .get(0)
                  .getInvoicePaymentSchedule()
                  .getInvoice()
                  .getBusinessPartner();
            } else if (RemittanceUtils.paymentDetailHasRelatedOrder(detail)) {
              bpartner = detail.getFINPaymentScheduleDetailList()
                  .get(0)
                  .getOrderPaymentSchedule()
                  .getOrder()
                  .getBusinessPartner();
            } else if (detail.getFINPaymentScheduleDetailList()
                .get(0)
                .getBusinessPartner() != null) {
              bpartner = detail.getFINPaymentScheduleDetailList().get(0).getBusinessPartner();
            }
          }
          FieldProviderFactory.setField(lineData, "cBpartnerId",
              bpartner != null ? bpartner.getId() : "");
          FieldProviderFactory.setField(lineData, "cCampaignId",
              detail.getFinPayment().getSalesCampaign() != null
                  ? detail.getFinPayment().getSalesCampaign().getId()
                  : "");
          FieldProviderFactory.setField(lineData, "cActivityId",
              detail.getFinPayment().getActivity() != null
                  ? detail.getFinPayment().getActivity().getId()
                  : "");
          FieldProviderFactory.setField(lineData, "recordId2",
              detail.isPrepayment() ? (pso != null ? pso.getId() : "")
                  : (psi != null ? psi.getId() : ""));

          list.add(lineData);
          previousObj = lineData;
        }

      }
      FieldProviderFactory[] data = new FieldProviderFactory[list.size()];
      return list.toArray(data);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private String getRemittanceLineFromPaymentDetail(final FIN_PaymentDetail detail,
      Remittance remittance) {
    StringBuilder hql = new StringBuilder();
    hql.append(" select remline.id ");
    hql.append(" from REM_RemittanceLine remline");
    hql.append(" where remline.paymentScheduleDetail.paymentDetails.id is not null");
    hql.append(" and remline.paymentScheduleDetail.paymentDetails.id = :paymentDetailsId");
    hql.append(" and remline.remittance.id = :remittanceId");

    Query<String> remitanceLineQuery = OBDal.getInstance()
        .getSession()
        .createQuery(hql.toString(), String.class);
    remitanceLineQuery.setParameter("paymentDetailsId", detail.getId());
    remitanceLineQuery.setParameter("remittanceId", remittance.getId());

    List<String> remlines = remitanceLineQuery.list();

    if (remlines != null && !remlines.isEmpty()) {
      return remlines.get(0);
    } else {
      return null;
    }
  }

  private DocLine[] loadLines() {
    ArrayList<Object> list = new ArrayList<Object>();
    FieldProviderFactory[] data = loadLinesFieldProvider(Record_ID);
    if (data == null || data.length == 0) {
      return null;
    }
    for (int i = 0; i < data.length; i++) {
      if (data[i] == null) {
        continue;
      }
      String lineID = data[i].getField("REM_RemittanceLine_ID");
      DocLine_REMRemittance docLine = new DocLine_REMRemittance(DocumentType, Record_ID, lineID);
      docLine.setFIN_Payment_Detail_ID(data[i].getField("FIN_Payment_Detail_ID"));
      docLine.loadAttributes(data[i], this);
      docLine.setAmount(data[i].getField("Amount"));
      docLine.setAmountExcludingCredit(data[i].getField("AmountExcludingCredit"));
      docLine.setIsPrepayment(data[i].getField("isprepayment"));
      docLine.setIsReceipt(data[i].getField("isReceipt"));
      docLine.setWriteOffAmt(data[i].getField("WriteOffAmt"));
      docLine.setC_GLItem_ID(data[i].getField("C_GLItem_ID"));
      docLine.setM_Record_Id2(data[i].getField("recordId2"));
      list.add(docLine);
    }
    // Return Array
    DocLine_REMRemittance[] dl = new DocLine_REMRemittance[list.size()];
    list.toArray(dl);
    return dl;
  } // loadLines

  @Override
  public Fact createFact(AcctSchema as, ConnectionProvider conn, Connection con,
      VariablesSecureApp vars) throws ServletException {
    // Select specific definition
    String strClassname = "";
    final StringBuilder whereClause = new StringBuilder();
    Fact fact = new Fact(this, as, Fact.POST_Actual);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    OBContext.setAdminMode();
    try {
      whereClause.append(" as astdt ");
      whereClause.append(
          " where astdt.acctschemaTable.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      whereClause.append(" and astdt.acctschemaTable.table.id = '" + AD_Table_ID + "'");
      whereClause.append(" and astdt.documentCategory = '" + DocumentType + "'");

      final OBQuery<AcctSchemaTableDocType> obqParameters = OBDal.getInstance()
          .createQuery(AcctSchemaTableDocType.class, whereClause.toString());
      final List<AcctSchemaTableDocType> acctSchemaTableDocTypes = obqParameters.list();

      if (acctSchemaTableDocTypes != null && acctSchemaTableDocTypes.size() > 0) {
        strClassname = acctSchemaTableDocTypes.get(0).getCreatefactTemplate().getClassname();
      }

      if (strClassname.equals("")) {
        final StringBuilder whereClause2 = new StringBuilder();

        whereClause2.append(" as ast ");
        whereClause2.append(" where ast.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
        whereClause2.append(" and ast.table.id = '" + AD_Table_ID + "'");

        final OBQuery<AcctSchemaTable> obqParameters2 = OBDal.getInstance()
            .createQuery(AcctSchemaTable.class, whereClause2.toString());
        final List<AcctSchemaTable> acctSchemaTables = obqParameters2.list();
        if (acctSchemaTables != null && acctSchemaTables.size() > 0
            && acctSchemaTables.get(0).getCreatefactTemplate() != null) {
          strClassname = acctSchemaTables.get(0).getCreatefactTemplate().getClassname();
        }
      }
      if (!strClassname.equals("")) {
        try {
          DocREMRemittanceTemplate newTemplate = (DocREMRemittanceTemplate) Class
              .forName(strClassname)
              .getDeclaredConstructor()
              .newInstance();
          return newTemplate.createFact(this, as, conn, con, vars);
        } catch (Exception e) {
          log4j.error("Error while creating new instance for DocREMRemittanceTemplate - ", e);
        }
      }

      Set<FIN_Payment> processedPayments = new HashSet<FIN_Payment>();
      for (int i = 0; p_lines != null && i < p_lines.length; i++) {
        DocLine_REMRemittance line = (DocLine_REMRemittance) p_lines[i];
        RemittanceLine remline = OBDal.getInstance().get(RemittanceLine.class, line.getLine_ID());
        FIN_Payment linePayment = OBDal.getInstance()
            .get(FIN_PaymentDetail.class, line.getFIN_Payment_Detail_ID())
            .getFinPayment();
        FIN_PaymentDetail linePaymentDetail = OBDal.getInstance()
            .get(FIN_PaymentDetail.class, line.getFIN_Payment_Detail_ID());
        boolean isReceipt = linePayment.isReceipt();
        boolean isPrepayment = line.getIsPrepayment().equals("Y");
        String bpAmount = line.getAmount();
        String lineAmount = line.getAmountExcludingCredit();

        // Convert payment amounts to accounting schema currency
        String faCurrency = C_Currency_ID;
        String currency = linePayment.getCurrency().getId();
        BigDecimal conversionRate = null;
        String conversionDate = null;
        if (as.m_C_Currency_ID != null && !as.m_C_Currency_ID.equals(currency)) {
          if (linePaymentDetail.getFINPaymentScheduleDetailList() != null
              && !linePaymentDetail.getFINPaymentScheduleDetailList().isEmpty()) {
            if (RemittanceUtils.paymentDetailHasRelatedInvoice(linePaymentDetail)
                && linePaymentDetail.getFINPaymentScheduleDetailList()
                    .get(0)
                    .getInvoicePaymentSchedule()
                    .getInvoice() != null) {
              Invoice invoice = linePaymentDetail.getFINPaymentScheduleDetailList()
                  .get(0)
                  .getInvoicePaymentSchedule()
                  .getInvoice();
              if (invoice.getCurrencyConversionRateDocList() != null
                  && !invoice.getCurrencyConversionRateDocList().isEmpty()) {
                for (ConversionRateDoc rateDoc : invoice.getCurrencyConversionRateDocList()) {
                  if (currency.equals(rateDoc.getCurrency().getId())
                      && as.m_C_Currency_ID.equals(rateDoc.getToCurrency().getId())) {
                    conversionRate = rateDoc.getRate();
                    break;
                  }
                }
              } else {
                conversionDate = OBDateUtils.formatDate(invoice.getAccountingDate());
              }
            } else if (RemittanceUtils.paymentDetailHasRelatedOrder(linePaymentDetail)
                && linePaymentDetail.getFINPaymentScheduleDetailList()
                    .get(0)
                    .getOrderPaymentSchedule()
                    .getOrder() != null) {
              Remittance rem = OBDal.getInstance().get(Remittance.class, Record_ID);
              conversionDate = OBDateUtils.formatDate(rem.getTransactionDate());
            }
          } else {
            if (as.m_C_Currency_ID.equals(faCurrency)
                && linePaymentDetail.getFinPayment().getFinancialTransactionConvertRate() != null) {
              // Select conversion rate of payment header, if applies
              conversionRate = linePaymentDetail.getFinPayment()
                  .getFinancialTransactionConvertRate();
            }
          }
        }

        if (getPaymentConfirmation(conn, linePayment.getId(), remline.getOrigstatus())) {
          fact.createLine(line,
              getAccountPayment(conn, linePayment.getPaymentMethod(), linePayment.getAccount(), as,
                  isReceipt),
              currency, (isReceipt ? "" : lineAmount), (isReceipt ? lineAmount : ""),
              Fact_Acct_Group_ID, "99999", DocumentType, conversionDate, conversionRate, conn);
        } else {
          if (line.WriteOffAmt != null && !line.WriteOffAmt.equals("")
              && new BigDecimal(line.WriteOffAmt).compareTo(ZERO) != 0) {
            fact.createLine(line, getAccount(AcctServer.ACCTTYPE_WriteOffDefault, as, conn),
                currency, (isReceipt ? line.WriteOffAmt : ""), (isReceipt ? "" : line.WriteOffAmt),
                Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType, conversionDate, conversionRate,
                conn);
            bpAmount = new BigDecimal(bpAmount).add(new BigDecimal(line.WriteOffAmt)).toString();
          }
          if ("".equals(line.getC_GLItem_ID())) {
            boolean isDoubtfuldebt = false;
            if (linePaymentDetail.getFINPaymentScheduleDetailList() != null
                && !linePaymentDetail.getFINPaymentScheduleDetailList().isEmpty()) {
              FIN_PaymentScheduleDetail fpsd = linePaymentDetail.getFINPaymentScheduleDetailList()
                  .get(0);
              if (fpsd.getInvoicePaymentSchedule() != null) {
                isDoubtfuldebt = !fpsd.getInvoicePaymentSchedule()
                    .getFINDoubtfulDebtList()
                    .isEmpty();
              } else if (fpsd.getOrderPaymentSchedule() != null) {
                isDoubtfuldebt = !fpsd.getOrderPaymentSchedule().getFINDoubtfulDebtList().isEmpty();
              }
            }

            fact.createLine(line,
                getAccountBPartner(line.m_C_BPartner_ID, as, isReceipt, isPrepayment,
                    isDoubtfuldebt, conn),
                currency, (isReceipt ? "" : bpAmount), (isReceipt ? bpAmount : ""),
                Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType, conversionDate, conversionRate,
                conn);
          } else {
            fact.createLine(line,
                getAccountGLItem(OBDal.getInstance().get(GLItem.class, line.getC_GLItem_ID()), as,
                    isReceipt, conn),
                currency, (isReceipt ? "" : bpAmount), (isReceipt ? bpAmount : ""),
                Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType, conversionDate, conversionRate,
                conn);
          }
        }

        fact.createLine(line, getAccount(conn, as, true), currency, (isReceipt ? lineAmount : ""),
            (isReceipt ? "" : lineAmount), Fact_Acct_Group_ID, "99999", DocumentType,
            conversionDate, conversionRate, conn);
        // Pre-payment is consumed when Used Credit Amount not equals Zero. When consuming
        // Credit no credit is generated
        if (!processedPayments.contains(linePayment)) {
          Remittance rem = OBDal.getInstance().get(Remittance.class, Record_ID);
          processedPayments.add(linePayment);
          if (linePayment.getUsedCredit().compareTo(ZERO) != 0
              && linePayment.getGeneratedCredit().compareTo(ZERO) == 0) {
            fact.createLine(rem.isSinglePayment() ? null : line,
                getAccountBPartner(line.m_C_BPartner_ID, as, linePayment.isReceipt(), true, conn),
                currency, (linePayment.isReceipt() ? linePayment.getUsedCredit().toString() : ""),
                (linePayment.isReceipt() ? "" : linePayment.getUsedCredit().toString()),
                Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType, conversionDate, conversionRate,
                conn);
          }
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    SeqNo = "0";
    return fact;
  }

  public String nextSeqNo(String oldSeqNo) {
    BigDecimal seqNo = new BigDecimal(oldSeqNo);
    SeqNo = (seqNo.add(new BigDecimal("10"))).toString();
    return SeqNo;
  }

  /**
   * Get Source Currency Balance - subtracts line amounts from total - no rounding
   * 
   * @return positive amount, if total is bigger than lines
   */
  @Override
  public BigDecimal getBalance() {
    BigDecimal retValue = ZERO;
    StringBuilder sb = new StringBuilder(" [");
    // Total
    retValue = retValue.add(new BigDecimal(getAmount(AcctServer.AMTTYPE_Gross)));
    sb.append(getAmount(AcctServer.AMTTYPE_Gross));
    // - Lines
    for (int i = 0; i < p_lines.length; i++) {
      DocLine_REMRemittance line = (DocLine_REMRemittance) p_lines[i];
      BigDecimal lineBalance = new BigDecimal(line.Amount);
      retValue = retValue
          .subtract("Y".equals(line.getIsReceipt()) ? lineBalance : lineBalance.negate());
      sb.append("-").append(lineBalance);
    }
    sb.append("]");
    //
    log4j.debug(" Balance=" + retValue + sb.toString());
    return retValue;
  } // getBalance

  @Override
  public boolean getDocumentConfirmation(ConnectionProvider conn, String strRecordId) {
    boolean confirmation = true;
    try {
      OBContext.setAdminMode(false);
      Remittance remittance = OBDal.getInstance().get(Remittance.class, strRecordId);
      if (!remittance.getRemittanceType().isPostingAllowed()) {
        confirmation = false;
      }
      if (!confirmation) {
        setStatus(STATUS_DocumentDisabled);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return confirmation;
  }

  public boolean getPaymentConfirmation(ConnectionProvider conn, String strRecordId,
      String OrigStatus) {
    // Checks if payment with original status 'payment received' is configured to generate
    // accounting
    boolean confirmation = false;
    final String PAYMENT_RECEIVED = "RPR";

    OBContext.setAdminMode();
    try {
      FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, strRecordId);
      // Posting can just happen if payment is in the right status
      if (PAYMENT_RECEIVED.equals(OrigStatus)) {
        OBCriteria<FinAccPaymentMethod> obCriteria = OBDal.getInstance()
            .createCriteria(FinAccPaymentMethod.class);
        obCriteria.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT, payment.getAccount()));
        obCriteria.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD,
            payment.getPaymentMethod()));
        obCriteria.setFilterOnReadableClients(false);
        obCriteria.setFilterOnReadableOrganization(false);
        List<FinAccPaymentMethod> lines = obCriteria.list();
        List<FIN_FinancialAccountAccounting> accounts = payment.getAccount()
            .getFINFinancialAccountAcctList();
        for (FIN_FinancialAccountAccounting account : accounts) {
          if (confirmation) {
            return confirmation;
          }
          if (payment.isReceipt()) {
            if (("INT").equals(lines.get(0).getUponReceiptUse())
                && account.getInTransitPaymentAccountIN() != null) {
              confirmation = true;
            } else if (("DEP").equals(lines.get(0).getUponReceiptUse())
                && account.getDepositAccount() != null) {
              confirmation = true;
            } else if (("CLE").equals(lines.get(0).getUponReceiptUse())
                && account.getClearedPaymentAccount() != null) {
              confirmation = true;
            }
          }
          // For payments with Amount ZERO always create an entry as no transaction will be created
          if (payment.getAmount().compareTo(ZERO) == 0) {
            confirmation = true;
          }
        }
      }
    } catch (Exception e) {
      setStatus(STATUS_DocumentDisabled);
      return confirmation;
    } finally {
      OBContext.restorePreviousMode();
    }
    return confirmation;
  }

  @Override
  public void loadObjectFieldProvider(ConnectionProvider conn, String strAD_Client_ID, String Id)
      throws ServletException {
    Remittance remittance = OBDal.getInstance().get(Remittance.class, Id);
    FieldProviderFactory[] data = new FieldProviderFactory[1];
    data[0] = new FieldProviderFactory(null);
    FieldProviderFactory.setField(data[0], "AD_Client_ID", remittance.getClient().getId());
    FieldProviderFactory.setField(data[0], "AD_Org_ID", remittance.getOrganization().getId());
    FieldProviderFactory.setField(data[0], "DocumentNo", remittance.getDocumentNo());
    String dateFormat = OBPropertiesProvider.getInstance()
        .getOpenbravoProperties()
        .getProperty("dateFormat.java");
    SimpleDateFormat outputFormat = new SimpleDateFormat(dateFormat);
    FieldProviderFactory.setField(data[0], "PaymentDate",
        outputFormat.format(remittance.getTransactionDate()));
    FieldProviderFactory.setField(data[0], "C_DocType_ID", remittance.getDocumentType().getId());
    FieldProviderFactory.setField(data[0], "C_Currency_ID",
        remittance.getFinancialAccount().getCurrency().getId());
    FieldProviderFactory.setField(data[0], "Amount", getTotalAmount());
    FieldProviderFactory.setField(data[0], "Posted", remittance.getPosted());
    FieldProviderFactory.setField(data[0], "Processed", remittance.isProcessed() ? "Y" : "N");
    FieldProviderFactory.setField(data[0], "Processing", remittance.isProcessNow() ? "Y" : "N");
    setObjectFieldProvider(data);
  }

  /*
   * Retrieves Account both for sent account and canceled account
   */
  public Account getAccount(ConnectionProvider conn, AcctSchema as, boolean bIsSentAccout)
      throws ServletException {
    Remittance remittance = OBDal.getInstance().get(Remittance.class, Record_ID);
    OBContext.setAdminMode();
    Account account = null;
    try {
      OBCriteria<RemittanceTypeAccount> accounts = OBDal.getInstance()
          .createCriteria(RemittanceTypeAccount.class);
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_ACCOUNTINGSCHEMA,
          OBDal.getInstance()
              .get(org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
                  as.getC_AcctSchema_ID())));
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_REMITTANCETYPE,
          remittance.getRemittanceType()));
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_ACTIVE, true));
      accounts.setFilterOnReadableClients(false);
      accounts.setFilterOnReadableOrganization(false);
      accounts.setMaxResults(1);
      List<RemittanceTypeAccount> accountList = accounts.list();
      if (accountList == null || accountList.size() == 0) {
        return null;
      }
      if (bIsSentAccout) {
        account = Account.getAccount(conn, accountList.get(0).getSentAccount().getId());
      } else {
        account = Account.getAccount(conn, accountList.get(0).getSettlementAccount().getId());
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return account;
  }

  public Account getAccountPayment(ConnectionProvider conn, FIN_PaymentMethod paymentMethod,
      FIN_FinancialAccount finAccount, AcctSchema as, boolean bIsReceipt) throws ServletException {
    OBContext.setAdminMode();
    Account account = null;
    try {
      OBCriteria<FIN_FinancialAccountAccounting> accounts = OBDal.getInstance()
          .createCriteria(FIN_FinancialAccountAccounting.class);
      accounts.add(Restrictions.eq(FIN_FinancialAccountAccounting.PROPERTY_ACCOUNT, finAccount));
      accounts.add(Restrictions.eq(FIN_FinancialAccountAccounting.PROPERTY_ACCOUNTINGSCHEMA,
          OBDal.getInstance()
              .get(org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
                  as.m_C_AcctSchema_ID)));
      accounts.add(Restrictions.eq(FIN_FinancialAccountAccounting.PROPERTY_ACTIVE, true));
      accounts.setFilterOnReadableClients(false);
      accounts.setFilterOnReadableOrganization(false);
      List<FIN_FinancialAccountAccounting> accountList = accounts.list();
      if (accountList == null || accountList.size() == 0) {
        return null;
      }
      OBCriteria<FinAccPaymentMethod> accPaymentMethod = OBDal.getInstance()
          .createCriteria(FinAccPaymentMethod.class);
      accPaymentMethod.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT, finAccount));
      accPaymentMethod
          .add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD, paymentMethod));
      accPaymentMethod.setFilterOnReadableClients(false);
      accPaymentMethod.setFilterOnReadableOrganization(false);
      List<FinAccPaymentMethod> lines = accPaymentMethod.list();
      if (bIsReceipt) {
        account = getAccount(conn, lines.get(0).getUponReceiptUse(), accountList.get(0),
            bIsReceipt);
      } else {
        account = getAccount(conn, lines.get(0).getUponPaymentUse(), accountList.get(0),
            bIsReceipt);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return account;
  }

  public String getSeqNo() {
    return SeqNo;
  }

  public void setSeqNo(String seqNo) {
    SeqNo = seqNo;
  }

  /*
   * Gets the total amount for the remittance as sum of lines amounts
   */
  String getTotalAmount() {
    OBContext.setAdminMode(true);
    try {
      Remittance remittance = OBDal.getInstance().get(Remittance.class, Record_ID);
      BigDecimal amount = BigDecimal.ZERO;
      Set<FIN_Payment> usedPayments = new HashSet<FIN_Payment>();
      for (RemittanceLine line : remittance.getREMRemittanceLineList()) {
        if (!usedPayments.contains(line.getPayment())) {
          StringBuilder where = new StringBuilder();
          where.append(" select sum(pd." + FIN_PaymentDetail.PROPERTY_AMOUNT + ")");
          where.append(" from " + FIN_PaymentDetail.ENTITY_NAME + " as pd");
          where.append(" where pd." + FIN_PaymentDetail.PROPERTY_FINPAYMENT + ".id = :paymentId");
          Query<BigDecimal> qry = OBDal.getInstance()
              .getSession()
              .createQuery(where.toString(), BigDecimal.class);
          qry.setParameter("paymentId", line.getPayment().getId());
          BigDecimal paymentAmt = qry.uniqueResult();
          amount = amount.add(line.getPayment().isReceipt() ? paymentAmt : paymentAmt.negate());
          usedPayments.add(line.getPayment());
        }
      }
      return amount.toString();
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
