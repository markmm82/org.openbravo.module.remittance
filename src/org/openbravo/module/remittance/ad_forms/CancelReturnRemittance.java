/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2019 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.filter.RequestFilter;
import org.openbravo.base.filter.ValueListFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.ad_combos.OrganizationComboData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.info.SelectorUtility;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SQLReturnObject;
import org.openbravo.erpCommon.utility.TableSQLData;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.ui.Form;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.utility.REM_SettleProtest;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;

public class CancelReturnRemittance extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  private static final String[] colNames = { "remittanceDocumentno", "documentno",
      "businesspartner", "description", "duedate", "amount", "currency", "rowkey" };
  private static final RequestFilter columnFilter = new ValueListFilter(colNames);
  private static final RequestFilter directionFilter = new ValueListFilter("asc", "desc");
  public static final String STATUS_CANCEL = "REM_CANCEL";
  public static final String STATUS_PAYMENTMADE = "PPM";
  public static final String STATUS_PAYMENTRECEIVED = "RPR";

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strOrganizationId = vars.getGlobalVariable("inpOrgId", "CancelReturnRemittance|OrgId",
          "0");
      String strRemittanceId = vars.getGlobalVariable("inpRemittance",
          "CancelReturnRemittance|Remittance", "");
      String strBusinessPartnerId = vars.getStringParameter("inpCBPartnerId", IsIDFilter.instance);
      String strDateFrom = vars.getStringParameter("inpDateFrom", "");
      String strDateTo = vars.getStringParameter("inpDateTo", "");
      String strDescription = vars.getStringParameter("inpdescription", "");
      printPageDataSheet(response, vars, strOrganizationId, strRemittanceId, strBusinessPartnerId,
          strDateFrom, strDateTo, strDescription);

    } else if (vars.commandIn("STRUCTURE")) {
      printGridStructure(response, vars);

    } else if (vars.commandIn("DATA")) {
      String action = vars.getStringParameter("action");
      if (!"getColumnTotals".equals(action)) {

        String strOrgId = vars.getRequestGlobalVariable("inpOrgId", "CancelReturnRemittance|OrgId");
        String strDescription = vars.getStringParameter("inpdescription", "");
        String strDateFrom = vars.getStringParameter("inpDateFrom", "");
        String strDateTo = vars.getStringParameter("inpDateTo", "");
        String strRemittanceId = vars.getRequestGlobalVariable("inpRemittance",
            "CancelReturnRemittance|Remittance");
        if ("".equals(strRemittanceId)) {
          strRemittanceId = vars.getSessionValue("CancelReturnRemittance.remittanceid");
          vars.removeSessionValue("CancelReturnRemittance.remittanceid");
        }
        String strBusinessPartnerId = vars
            .getStringParameter("inpCBPartnerId", IsIDFilter.instance);
        String strNewFilter = vars.getStringParameter("newFilter");
        String strOffset = vars.getStringParameter("offset");
        String strPageSize = vars.getStringParameter("page_size");
        String strSortCols = vars.getInStringParameter("sort_cols", columnFilter);
        String strSortDirs = vars.getInStringParameter("sort_dirs", directionFilter);

        printGridData(response, vars, strSortCols, strSortDirs, strOffset, strPageSize,
            strNewFilter, strOrgId, strRemittanceId, strDescription, strBusinessPartnerId,
            strDateFrom, strDateTo);
      } else {
        // When mouse over a float column in a dojo grid this action is performed automatically
        XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
            "org/openbravo/erpCommon/utility/DataGridTotal").createXmlDocument();
        xmlDocument.setParameter("total", "0");
        response.setContentType("text/xml; charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter out = response.getWriter();
        out.println(xmlDocument.print());
        out.close();
      }
    } else if (vars.commandIn("CALLOUTREMITTANCE")) {
      String strRemittanceId = vars.getRequestGlobalVariable("inpRemittance", "");
      String strOrgId = vars.getRequestGlobalVariable("inpOrgId", "");
      reloadRemittanceCombo(response, strRemittanceId, strOrgId);

    } else if (vars.commandIn("CANCEL") || vars.commandIn("RETURN")) {
      String strOrganizationId = vars.getGlobalVariable("inpOrgId", "CancelReturnRemittance|OrgId",
          "0");
      String strRemittanceId = vars.getGlobalVariable("inpRemittance",
          "CancelReturnRemittance|Remittance", "");
      String strDateAcct = vars.getStringParameter("inpDateAcct");
      String strPayments = vars.getRequestGlobalVariable("inpSelectedRowList", "");
      String strBusinessPartnerId = vars.getStringParameter("inpCBPartnerId", IsIDFilter.instance);
      String strDateFrom = vars.getStringParameter("inpDateFrom", "");
      String strDateTo = vars.getStringParameter("inpDateTo", "");
      String strDescription = vars.getStringParameter("inpdescription", "");
      try {
        if (vars.commandIn("CANCEL")) {
          cancelPayments(vars, strPayments, strDateAcct);
        } else {
          returnPayments(strPayments, strDateAcct);
        }
        final String formClassName = this.getClass().getName();
        final String adFormId = getADFormInfo(formClassName).getId();
        OBError myMessage = new OBError();
        // Message
        myMessage.setType("Success");
        myMessage.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
        vars.setMessage(adFormId, myMessage);
        printPageDataSheet(response, vars, strOrganizationId, strRemittanceId,
            strBusinessPartnerId, strDateFrom, strDateTo, strDescription);
      } catch (Exception e) {
        log4j.error("Error in doPost: ", e);
        final String formClassName = this.getClass().getName();
        final String adFormId = getADFormInfo(formClassName).getId();
        OBError myMessage = Utility.translateError(this, vars, vars.getLanguage(),
            FIN_Utility.getExceptionMessage(e));
        // Message
        myMessage.setType("Error");
        myMessage.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
        vars.setMessage(adFormId, myMessage);
        printPageDataSheet(response, vars, strOrganizationId, strRemittanceId,
            strBusinessPartnerId, strDateFrom, strDateTo, strDescription);
      }

    } else
      pageError(response);

  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strOrganizationId, String strRemittanceId, String strBpartnerId, String strDateFrom,
      String strDateTo, String strDescription) throws IOException, ServletException {
    log4j.debug("Output: CancelReturnRemittance");

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/remittance/ad_forms/CancelReturnRemittance").createXmlDocument();

    final String formClassName = this.getClass().getName();
    final String adFormId = getADFormInfo(formClassName).getId();
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("dateDisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("KeyName", "");
    xmlDocument.setParameter("windowId", adFormId);
    xmlDocument.setParameter("tabId", adFormId);
    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));

    String newOrg = strOrganizationId;
    if (!"0".equals(strOrganizationId)) {
      xmlDocument.setParameter("orgId", strOrganizationId);
    } else {
      FieldProvider[] fp = OrganizationComboData.selectCombo(this, vars.getRole());
      if (fp != null && fp.length > 0) {
        newOrg = fp[0].getField("id");
        xmlDocument.setParameter("orgId", newOrg);
      }
    }
    xmlDocument.setData("reportAD_ORGID", "liststructure",
        OrganizationComboData.selectCombo(this, vars.getRole()));

    List<Remittance> remittances = getOpenRemittances(newOrg);
    if (remittances.size() > 0 && "".equals(strRemittanceId)) {
      vars.setSessionValue("CancelReturnRemittance.remittanceid", remittances.get(0).getId());
    } else if (!"".equals(strRemittanceId)) {
      vars.setSessionValue("CancelReturnRemittance.remittanceid", strRemittanceId);
    }

    // Remittance combobox
    String remittanceComboHtml = FIN_Utility.getOptionsList(remittances, strRemittanceId, true);
    xmlDocument.setParameter("sectionDetailRemittance", remittanceComboHtml);

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CancelReturnRemittance", false, "",
        "", "", false, "ad_forms", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "org.openbravo.module.remittance.ad_forms.CancelReturnRemittance");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());

      NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
          "CancelReturnRemittance.html", classInfo.id, classInfo.type, strReplaceWith,
          tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CancelReturnRemittance.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    xmlDocument.setParameter("strElement_BP", "");
    xmlDocument.setParameter("grid", "20");
    xmlDocument.setParameter("grid_Offset", "");
    xmlDocument.setParameter("grid_SortCols", "1");
    xmlDocument.setParameter("grid_SortDirs", "ASC");
    xmlDocument.setParameter("grid_Default", "0");

    OBError myMessage = vars.getMessage(adFormId);
    vars.removeMessage(adFormId);
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("datedisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("datesaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("strElement_BP", strBpartnerId);
    xmlDocument.setParameter("description", strDescription);

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printGridStructure(HttpServletResponse response, VariablesSecureApp vars)
      throws IOException, ServletException {
    log4j.debug("Output: print page structure");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/utility/DataGridStructure").createXmlDocument();

    SQLReturnObject[] data = getHeaders(vars);
    String type = "Hidden";
    String title = "";
    String description = "";

    xmlDocument.setParameter("type", type);
    xmlDocument.setParameter("title", title);
    xmlDocument.setParameter("description", description);
    xmlDocument.setData("structure1", data);
    xmlDocument.setParameter("backendPageSize", String.valueOf(TableSQLData.maxRowsPerGridPage));
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter out = response.getWriter();
    if (log4j.isDebugEnabled())
      log4j.debug(xmlDocument.print());
    out.println(xmlDocument.print());
    out.close();
  }

  private SQLReturnObject[] getHeaders(VariablesSecureApp vars) {
    SQLReturnObject[] data = null;
    Vector<SQLReturnObject> vAux = new Vector<SQLReturnObject>();
    String[] colWidths = { "100", "100", "150", "350", "85", "100", "50", "0" }; // total 935
    String[] colLabels = { "REM_REMITTANCE_DOCUMENTNO", "APRM_PAYEXECMNGT_DOCUMENTNO",
        "APRM_FATS_BPARTNER", "Description", "REM_ExpectedDate", "Amount", "SOS_CURRENCY", "rowkey" };
    String[] colLabelsFormat = { "string", "string", "string", "string", "string", "float",
        "string", "string" };
    for (int i = 0; i < colNames.length; i++) {
      SQLReturnObject dataAux = new SQLReturnObject();
      dataAux.setData("columnname", colNames[i]);
      dataAux.setData("gridcolumnname", colNames[i]);
      dataAux.setData("adReferenceId", "AD_Reference_ID");
      dataAux.setData("adReferenceValueId", "AD_ReferenceValue_ID");
      dataAux.setData("isidentifier", (colNames[i].equals("rowkey") ? "true" : "false"));
      dataAux.setData("iskey", (colNames[i].equals("rowkey") ? "true" : "false"));
      dataAux.setData("isvisible",
          (colNames[i].endsWith("_id") || colNames[i].equals("rowkey") ? "false" : "true"));
      dataAux.setData("name", Utility.messageBD(this, colLabels[i], vars.getLanguage()));
      dataAux.setData("type", colLabelsFormat[i]);
      dataAux.setData("width", colWidths[i]);
      vAux.addElement(dataAux);
    }
    data = new SQLReturnObject[vAux.size()];
    vAux.copyInto(data);
    return data;
  }

  private void printGridData(HttpServletResponse response, VariablesSecureApp vars,
      String strOrderCols, String strOrderDirs, String strOffset, String strPageSize,
      String strNewFilter, String strOrgId, String strRemittanceId, String strDescription,
      String strBusinessPartner, String strDateFrom, String strDateTo) throws IOException {
    log4j.debug("Output: print page rows");

    int page = 0;
    SQLReturnObject[] headers = getHeaders(vars);
    String type = "Hidden";
    String title = "";
    String description = "";
    String strNumRows = "0";
    String action = vars.getStringParameter("action");
    int offset, pageSize;
    if (action.equalsIgnoreCase("getIdsInRange")) {
      offset = 0;
      pageSize = 100;
    } else {
      offset = Integer.valueOf(strOffset).intValue();
      pageSize = Integer.valueOf(strPageSize).intValue();
    }
    List<FIN_Payment> gridPayments = null;
    String strNewFilterAux = strNewFilter;

    if (headers != null) {
      try {
        // build sql orderBy clause from parameters
        String strOrderBy = SelectorUtility.buildOrderByClause(strOrderCols, strOrderDirs);
        HashMap<String, String> orderByColsMap = new HashMap<String, String>();
        // String[] colNames = { "documentno", "businesspartner", "description", "duedate",
        // "amount", "currency", "rowkey" };

        orderByColsMap.put("remittanceDocumentno", "rl." + RemittanceLine.PROPERTY_REMITTANCE + "."
            + Remittance.PROPERTY_DOCUMENTNO);
        orderByColsMap.put("documentno", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_DOCUMENTNO);
        orderByColsMap.put("businesspartner", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_BUSINESSPARTNER);
        orderByColsMap.put("description", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_DESCRIPTION);
        orderByColsMap.put("duedate", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_PAYMENTDATE);
        orderByColsMap.put("amount", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_AMOUNT);
        orderByColsMap.put("currency", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_CURRENCY);
        orderByColsMap.put("rowkey", "rl." + RemittanceLine.PROPERTY_PAYMENT + "."
            + FIN_Payment.PROPERTY_ID);
        for (int i = 0; i < colNames.length; i++)
          strOrderBy = strOrderBy.replace(colNames[i], orderByColsMap.get(colNames[i]));

        String[] orderByClause = strOrderBy.split(" ");
        String strOrderByProperty = orderByClause[0];
        String strAscDesc = orderByClause[1];

        page = TableSQLData.calcAndGetBackendPage(vars, "CancelReturnRemittance.currentPage");
        if (vars.getStringParameter("movePage", "").length() > 0) {
          // on movePage action force executing countRows again
          strNewFilterAux = "";
        }
        int oldOffset = offset;
        offset = (page * TableSQLData.maxRowsPerGridPage) + offset;
        log4j.debug("relativeOffset: " + oldOffset + " absoluteOffset: " + offset);
        if (strNewFilterAux.equals("1") || strNewFilterAux.equals("")) { // New filter or first
                                                                         // load
          gridPayments = getRemittanceLineRowCount(
              OBDal.getInstance().get(Organization.class, strOrgId),
              OBDal.getInstance().get(Remittance.class, strRemittanceId), strDescription, OBDal
                  .getInstance().get(BusinessPartner.class, strBusinessPartner), strDateFrom,
              strDateTo, offset, pageSize, null, null);
          strNumRows = Integer.toString(gridPayments.size());

          vars.setSessionValue("CancelReturnRemittance.numrows", strNumRows);
        } else {
          strNumRows = vars.getSessionValue("CancelReturnRemittance.numrows");
        }

        gridPayments = getRemittanceLineRowCount(
            OBDal.getInstance().get(Organization.class, strOrgId),
            OBDal.getInstance().get(Remittance.class, strRemittanceId), strDescription, OBDal
                .getInstance().get(BusinessPartner.class, strBusinessPartner), strDateFrom,
            strDateTo, offset, pageSize, strOrderByProperty, strAscDesc);
        strNumRows = Integer.toString(gridPayments != null ? gridPayments.size() : 0);
      } catch (ServletException e) {
        log4j.error("Error in print page data: ", e);
        OBError myError = Utility.translateError(this, vars, vars.getLanguage(), e.getMessage());
        if (!myError.isConnectionAvailable()) {
          bdErrorAjax(response, "Error", "Connection Error", "No database connection");
          return;
        } else {
          type = myError.getType();
          title = myError.getTitle();
          if (!myError.getMessage().startsWith("<![CDATA["))
            description = "<![CDATA[" + myError.getMessage() + "]]>";
          else
            description = myError.getMessage();
        }
      } catch (Exception e) {
        log4j.debug("Error obtaining rows data");
        type = "Error";
        title = "Error";
        if (e.getMessage() != null && e.getMessage().startsWith("<![CDATA[")) {
          description = "<![CDATA[" + e.getMessage() + "]]>";
        } else {
          description = e.getMessage() != null ? e.getMessage() : "";
        }
        log4j.error("Error in print grid data: ", e);
      }
    }
    if (!type.startsWith("<![CDATA["))
      type = "<![CDATA[" + type + "]]>";
    if (!title.startsWith("<![CDATA["))
      title = "<![CDATA[" + title + "]]>";
    if (!description.startsWith("<![CDATA["))
      description = "<![CDATA[" + description + "]]>";
    StringBuffer strRowsData = new StringBuffer();
    if (action.equalsIgnoreCase("getIdsInRange")) {
      strRowsData.append("<xml-rangeid>\n");
    } else {
      strRowsData.append("<xml-data>\n");
    }
    strRowsData.append("  <status>\n");
    strRowsData.append("    <type>").append(type).append("</type>\n");
    strRowsData.append("    <title>").append(title).append("</title>\n");
    strRowsData.append("    <description>").append(description).append("</description>\n");
    strRowsData.append("  </status>\n");
    if (action.equalsIgnoreCase("getIdsInRange")) {
      int minOffset = Integer.valueOf(vars.getStringParameter("minOffset")).intValue();
      int maxOffset = Integer.valueOf(vars.getStringParameter("maxOffset")).intValue();
      for (int i = minOffset; i <= maxOffset; i++) {
        strRowsData.append("      <id><![CDATA[" + gridPayments.get(i).getId().toString());
        strRowsData.append("]]></id>\n");
      }
    } else {
      strRowsData.append("  <rows numRows=\"").append(strNumRows)
          .append("\" backendPage=\"" + page + "\">\n");
      if (gridPayments != null && gridPayments.size() > 0) {

        final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(vars.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
        dfs.setGroupingSeparator(vars.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
        final DecimalFormat numberFormat = new DecimalFormat(
            vars.getSessionValue("#AD_ReportNumberFormat"), dfs);
        try {
          OBContext.setAdminMode(true);
          for (FIN_Payment pay : gridPayments) {
            strRowsData.append("    <tr>\n");
            for (int k = 0; k < headers.length; k++) {
              strRowsData.append("      <td><![CDATA[");
              // "remittanceDocumentno", "documentno", "businesspartner", "description", "duedate",
              // "amount", "currency", "rowkey"
              String columnData = "";
              switch (k) {
              case 0: // remittance document no
                columnData = REM_SettleProtest.getPaymentRemittance(pay).getDocumentNo();
                break;
              case 1: // payment documentno
                columnData = pay.getDocumentNo();
                break;
              case 2: // business partner
                columnData = pay.getBusinessPartner() != null ? pay.getBusinessPartner()
                    .getIdentifier() : "";
                break;
              case 3: // description
                if (pay.getDescription() != null)
                  columnData = pay.getDescription();
                break;
              case 4: // due date
                columnData = Utility.formatDate(pay.getPaymentDate(), vars.getJavaDateFormat());
                break;
              case 5: // amount
                columnData = numberFormat.format(pay.getAmount());
                break;
              case 6: // currency
                columnData = pay.getCurrency().getISOCode();
                break;
              case 7: // row key
                columnData = pay.getId().toString();
                break;
              default: // invalid
                log4j.error("Invalid column");
                break;
              }

              if (columnData != "") {
                if (headers[k].getField("adReferenceId").equals("32"))
                  strRowsData.append(strReplaceWith).append("/images/");
                strRowsData.append(columnData.replaceAll("<b>", "").replaceAll("<B>", "")
                    .replaceAll("</b>", "").replaceAll("</B>", "").replaceAll("<i>", "")
                    .replaceAll("<I>", "").replaceAll("</i>", "").replaceAll("</I>", "")
                    .replaceAll("<p>", "&nbsp;").replaceAll("<P>", "&nbsp;")
                    .replaceAll("<br>", "&nbsp;").replaceAll("<BR>", "&nbsp;"));
              } else {
                if (headers[k].getField("adReferenceId").equals("32")) {
                  strRowsData.append(strReplaceWith).append("/images/blank.gif");
                } else
                  strRowsData.append("&nbsp;");
              }
              strRowsData.append("]]></td>\n");
            }
            strRowsData.append("    </tr>\n");
          }
        } finally {
          OBContext.restorePreviousMode();
        }
      }
      strRowsData.append("  </rows>\n");
    }
    if (action.equalsIgnoreCase("getIdsInRange")) {
      strRowsData.append("</xml-rangeid>\n");
    } else {
      strRowsData.append("</xml-data>\n");
    }
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter out = response.getWriter();
    if (log4j.isDebugEnabled())
      log4j.debug(strRowsData.toString());
    out.print(strRowsData.toString());
    out.close();
  }

  private void reloadRemittanceCombo(HttpServletResponse response, String strRemittance,
      String strOrgId) throws IOException, ServletException {
    List<Remittance> remittances = getOpenRemittances(strOrgId);
    String remittanceComboHtml = FIN_Utility.getOptionsList(remittances, strRemittance, false);
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(remittanceComboHtml.replaceAll("\"", "\\'"));
    out.close();
  }

  private Form getADFormInfo(String formClassName) {
    OBContext.setAdminMode();
    try {
      OBCriteria<Form> obc = OBDal.getInstance().createCriteria(Form.class);
      obc.add(Restrictions.eq(Form.PROPERTY_JAVACLASSNAME, formClassName));
      if (obc.list() == null || obc.list().size() == 0) {
        throw new OBException(formClassName + ": Error on window data");
      }
      return obc.list().get(0);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private List<Remittance> getOpenRemittances(String strOrganizationId) {
    OBContext.setAdminMode();
    try {
      final StringBuilder whereClause = new StringBuilder();

      whereClause.append(" as rl ");
      whereClause.append(" where rl.remittance.processed = " + Boolean.TRUE);
      whereClause.append(" and not exists ");
      whereClause
          .append(" (select 1 from REM_RemittanceLineCancel rlc where rlc.payment.id = rl.payment.id and rlc.remittance.id = rl.remittance.id)");
      whereClause.append(" and not exists ");
      whereClause
          .append(" (select 1 from REM_RemittanceLineReturn rlr where rlr.payment.id = rl.payment.id and rlr.remittance.id = rl.remittance.id)");
      whereClause.append(" and rl.organization.id in (");
      whereClause.append(Utility.getInStrSet(OBContext.getOBContext()
          .getOrganizationStructureProvider().getChildTree(strOrganizationId, true)));
      whereClause.append(") ");
      whereClause.append("order by rl.remittance.documentNo");
      final OBQuery<RemittanceLine> obqRemLine = OBDal.getInstance().createQuery(
          RemittanceLine.class, whereClause.toString());
      obqRemLine.setFilterOnReadableClients(false);
      obqRemLine.setFilterOnReadableOrganization(false);
      List<Remittance> remittances = new ArrayList<Remittance>();
      for (RemittanceLine rl : obqRemLine.list()) {
        Remittance remittanceToAdd = rl.getRemittance();
        if (!remittances.contains(remittanceToAdd)) {
          remittances.add(rl.getRemittance());
        }
      }

      return remittances;

    } finally {
      OBContext.restorePreviousMode();
    }

  }

  /**
   * This method returns list of Remittance Lines Payments to be canceled/returned and filtered by
   * the following parameters.
   * 
   * @param organization
   *          Organization
   * @param remittanceId
   *          Remittance
   * @param offset
   *          Starting register number.
   * @param pageSize
   *          Limited the max number of results.
   * @param strOrderByProperty
   *          Property used for ordering the results.
   * @param strAscDesc
   *          if true order by asc, if false order by desc
   * @return Filtered Remittance Line Payment list.
   */
  private List<FIN_Payment> getRemittanceLineRowCount(Organization organization,
      Remittance remittance, String description, BusinessPartner businessPartner,
      String strDateFrom, String strDateTo, int offset, int pageSize, String strOrderByProperty,
      String strAscDesc) {
    if (organization == null || pageSize == 0) {
      return new ArrayList<FIN_Payment>();
    }
    List<FIN_Payment> payments = new ArrayList<FIN_Payment>();
    for (RemittanceLine remittanceLine : getRemittanceLine(organization, remittance, description,
        businessPartner, strDateFrom, strDateTo, offset, pageSize, strOrderByProperty, strAscDesc)) {
      if (!payments.contains(remittanceLine.getPayment())) {
        payments.add(remittanceLine.getPayment());
      }
    }
    return new ArrayList<FIN_Payment>(payments);
  }

  /**
   * This method returns list of Remittance Lines to be canceled/returned and filtered by the
   * following parameters.
   * 
   * @param organization
   *          Organization
   * @param remittanceId
   *          Remittance
   * @param offset
   *          Starting register number.
   * @param pageSize
   *          Limited the max number of results.
   * @param strOrderByProperty
   *          Property used for ordering the results.
   * @param strAscDesc
   *          if true order by asc, if false order by desc
   * @return Filtered Remittance Line list.
   */
  private List<RemittanceLine> getRemittanceLine(Organization organization, Remittance remittance,
      String description, BusinessPartner businessPartner, String strDateFrom, String strDateTo,
      int offset, int pageSize, String strOrderByProperty, String strAscDesc) {
    if (organization == null || pageSize == 0) {
      return new ArrayList<RemittanceLine>();
    }
    Date datefrom = FIN_Utility.getDate(strDateFrom);
    Date dateto = FIN_Utility.getDate(strDateTo);
    try {
      OBContext.setAdminMode();
      final StringBuilder whereClause = new StringBuilder();
      final Map<String, Object> parameters = new HashMap<>();

      whereClause.append(" as rl ");
      whereClause.append(" where rl.remittance.processed = " + Boolean.TRUE);
      if (remittance != null) {
        whereClause.append(" and rl.remittance.id = '");
        whereClause.append(remittance.getId());
        whereClause.append("'");
      }
      whereClause.append(" and not exists ");
      whereClause
          .append(" (select 1 from REM_RemittanceLineCancel rlc where rlc.payment.id = rl.payment.id ");
      if (remittance != null) {
        whereClause.append(" and rlc.remittance.id = '");
        whereClause.append(remittance.getId());
        whereClause.append("'");
      } else {
        whereClause.append(" and rlc.remittance.id = rl.remittance.id ");
      }
      whereClause.append(") ");
      whereClause.append(" and not exists ");
      whereClause
          .append(" (select 1 from REM_RemittanceLineReturn rlr where rlr.payment.id = rl.payment.id ");
      if (remittance != null) {
        whereClause.append(" and rlr.remittance.id = '");
        whereClause.append(remittance.getId());
        whereClause.append("'");
      } else {
        whereClause.append(" and rlr.remittance.id = rl.remittance.id ");
      }
      whereClause.append(") ");
      if (description != null && !"".equals(description)) {
        whereClause.append("and rl.payment.description like :description");
        parameters.put("description", "%" + description + "%");
      }
      if (businessPartner != null) {
        whereClause.append("and rl.payment.businessPartner.id = '");
        whereClause.append(businessPartner.getId());
        whereClause.append("' ");
      }
      if (!"".equals(strDateFrom)) {
        whereClause.append("and rl.payment.paymentDate >= :dateFrom ");
        parameters.put("dateFrom", datefrom);
      }
      if (!"".equals(strDateTo)) {
        whereClause.append("and rl.payment.paymentDate <= :dateTo ");
        parameters.put("dateTo", dateto);
      }
      if (businessPartner != null) {
        whereClause.append("and rl.payment.businessPartner.id = '");
        whereClause.append(businessPartner.getId());
        whereClause.append("' ");
      }
      whereClause.append("and rl.payment.organization.id in (");
      whereClause.append(Utility.getInStrSet(OBContext.getOBContext()
          .getOrganizationStructureProvider().getChildTree(organization.getId(), true)));
      whereClause.append(") ");
      if (strOrderByProperty != null && !strOrderByProperty.isEmpty()) {
        whereClause.append("order by ");
        whereClause.append(strOrderByProperty);
        whereClause.append(" ");
      }
      if (strAscDesc != null && !strAscDesc.isEmpty())
        whereClause.append(strAscDesc);

      final OBQuery<RemittanceLine> obqP = OBDal.getInstance().createQuery(RemittanceLine.class,
          whereClause.toString());
      obqP.setNamedParameters(parameters);
      obqP.setFilterOnReadableOrganization(false);

      obqP.setFirstResult(offset);

      return obqP.list();

    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void cancelPayments(VariablesSecureApp vars, String strPayments, String strDateAcct)
      throws Exception {
    int i = 0;
    String _strPayments = strPayments;
    if (_strPayments.startsWith("(")) {
      _strPayments = _strPayments.substring(1, strPayments.length() - 1);
    }
    if (!_strPayments.equals("")) {
      _strPayments = Replace.replace(_strPayments, "'", "");
      StringTokenizer st = new StringTokenizer(_strPayments, ",", false);
      while (st.hasMoreTokens()) {
        i++;
        FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, st.nextToken().trim());
        REM_SettleProtest.cancelPayment(payment,
            new SimpleDateFormat(vars.getJavaDateFormat()).parse(strDateAcct));
        if (i % 100 == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
        }
      }
    }
  }

  private void returnPayments(String strPayments, String strDateAcct) {

    int i = 0;
    String _strPayments = strPayments;
    if (_strPayments.startsWith("(")) {
      _strPayments = _strPayments.substring(1, _strPayments.length() - 1);
    }
    if (!_strPayments.equals("")) {
      _strPayments = Replace.replace(_strPayments, "'", "");
      StringTokenizer st = new StringTokenizer(_strPayments, ",", false);
      while (st.hasMoreTokens()) {
        i++;
        FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, st.nextToken().trim());
        REM_SettleProtest.returnPayment(payment, strDateAcct);
        if (i % 100 == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
        }
      }
    }
  }

}
