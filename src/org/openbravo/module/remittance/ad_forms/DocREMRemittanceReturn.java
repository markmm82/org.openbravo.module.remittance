/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.remittance.ad_forms;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.Account;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.DocLine_FINPayment;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.ConversionRateDoc;
import org.openbravo.model.common.enterprise.AcctSchemaTableDocType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.accounting.Costcenter;
import org.openbravo.model.financialmgmt.accounting.FIN_FinancialAccountAccounting;
import org.openbravo.model.financialmgmt.accounting.UserDimension1;
import org.openbravo.model.financialmgmt.accounting.UserDimension2;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaTable;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.RemittanceLineReturn;
import org.openbravo.module.remittance.RemittanceType;
import org.openbravo.module.remittance.RemittanceTypeAccount;
import org.openbravo.module.remittance.utility.RemittanceUtils;

public class DocREMRemittanceReturn extends AcctServer {

  @SuppressWarnings("unused")
  private static final long serialVersionUID = 1L;
  static Logger log4j = Logger.getLogger(DocREMRemittanceReturn.class);
  public static final String DOCTYPE_RemittanceReturn = "REM_REMRETURN";

  String SeqNo = "0";
  String generatedAmount = "";
  String usedAmount = "";

  public DocREMRemittanceReturn() {
  }

  public DocREMRemittanceReturn(String AD_Client_ID, String AD_Org_ID,
      ConnectionProvider connectionProvider) {
    super(AD_Client_ID, AD_Org_ID, connectionProvider);
  }

  @Override
  public boolean loadDocumentDetails(FieldProvider[] data, ConnectionProvider conn) {
    DocumentType = DOCTYPE_RemittanceReturn;
    DateDoc = data[0].getField("PaymentDate");
    Amounts[AMTTYPE_Gross] = data[0].getField("Amount");
    generatedAmount = data[0].getField("GeneratedCredit");
    usedAmount = data[0].getField("UsedCredit");
    loadDocumentType();
    p_lines = loadLines();
    return true;
  }

  public FieldProviderFactory[] loadLinesFieldProvider(String Id) {
    RemittanceLineReturn rlr = OBDal.getInstance().get(RemittanceLineReturn.class, Id);
    FIN_Payment payment = rlr.getPayment();
    List<FIN_PaymentDetail> paymentDetails = payment.getFINPaymentDetailList();
    if (paymentDetails == null) {
      return null;
    }

    int noOfPaymentDetails = paymentDetails.size();
    final BigDecimal usedCredit = payment.getUsedCredit();
    final BigDecimal creditAmtToAdjust = usedCredit.divide(new BigDecimal(noOfPaymentDetails),
        payment.getCurrency().getPricePrecision().intValue(), RoundingMode.HALF_UP);
    BigDecimal totalCreditAdjusted = BigDecimal.ZERO;
    final FieldProviderFactory[] data = new FieldProviderFactory[noOfPaymentDetails];
    OBContext.setAdminMode();
    try {
      for (int i = 0; i < data.length; i++) {
        FIN_PaymentDetail paymentDetail = paymentDetails.get(i);
        // Details refunded used credit are excluded as the entry will be created using the credit
        // used
        if (paymentDetail.isRefund() && paymentDetail.isPrepayment()) {
          continue;
        }
        data[i] = new FieldProviderFactory(null);
        FieldProviderFactory.setField(data[i], "AD_Client_ID", paymentDetail.getClient().getId());
        FieldProviderFactory.setField(data[i], "AD_Org_ID",
            paymentDetail.getOrganization().getId());
        FieldProviderFactory.setField(data[i], "FIN_Payment_Detail_ID", paymentDetail.getId());
        FieldProviderFactory.setField(data[i], "Amount", paymentDetail.getAmount().toString());
        BigDecimal amountExcludingCredit = paymentDetail.getAmount().subtract(creditAmtToAdjust);
        totalCreditAdjusted = totalCreditAdjusted.add(creditAmtToAdjust);
        if (i == noOfPaymentDetails - 1) {
          amountExcludingCredit = amountExcludingCredit
              .subtract(usedCredit.subtract(totalCreditAdjusted));
        }
        FieldProviderFactory.setField(data[i], "AmountExcludingCredit",
            amountExcludingCredit.toString());
        FieldProviderFactory.setField(data[i], "isprepayment",
            paymentDetail.isPrepayment() ? "Y" : "N");
        FieldProviderFactory.setField(data[i], "WriteOffAmt",
            paymentDetail.getWriteoffAmount().toString());
        BusinessPartner bpartner = paymentDetail.getFinPayment().getBusinessPartner();
        if (bpartner == null) {
          if (paymentDetail.getFINPaymentScheduleDetailList()
              .get(0)
              .getInvoicePaymentSchedule() != null) {
            bpartner = paymentDetail.getFINPaymentScheduleDetailList()
                .get(0)
                .getInvoicePaymentSchedule()
                .getInvoice()
                .getBusinessPartner();
          } else if (paymentDetail.getFINPaymentScheduleDetailList()
              .get(0)
              .getOrderPaymentSchedule() != null) {
            bpartner = paymentDetail.getFINPaymentScheduleDetailList()
                .get(0)
                .getOrderPaymentSchedule()
                .getOrder()
                .getBusinessPartner();
          }
        }
        FieldProviderFactory.setField(data[i], "cBpartnerId",
            bpartner != null ? bpartner.getId() : "");
        FieldProviderFactory.setField(data[i], "C_GLItem_ID",
            paymentDetail.getGLItem() != null ? paymentDetail.getGLItem().getId() : "");
        FieldProviderFactory.setField(data[i], "Refund", paymentDetail.isRefund() ? "Y" : "N");
        FieldProviderFactory.setField(data[i], "isprepayment",
            paymentDetail.isPrepayment() ? "Y" : "N");

        FieldProviderFactory.setField(data[i], "cProjectId",
            payment.getProject() != null ? payment.getProject().getId() : "");
        Costcenter cCenter = RemittanceUtils
            .getCostCenterFromPaymentOrInvoiceOrOrder(paymentDetail);
        FieldProviderFactory.setField(data[i], "cCostcenterId",
            cCenter != null ? cCenter.getId() : "");
        FieldProviderFactory.setField(data[i], "mProductId",
            paymentDetail.getFINPaymentScheduleDetailList().get(0).getProduct() != null
                ? paymentDetail.getFINPaymentScheduleDetailList().get(0).getProduct().getId()
                : "");
        UserDimension1 userDimension1 = RemittanceUtils
            .getUserDimension1FromPaymentOrInvoiceOrOrder(paymentDetail);
        FieldProviderFactory.setField(data[i], "user1Id",
            userDimension1 != null ? userDimension1.getId() : "");
        UserDimension2 userDimension2 = RemittanceUtils
            .getUserDimension2FromPaymentOrInvoiceOrOrder(paymentDetail);
        FieldProviderFactory.setField(data[i], "user2Id",
            userDimension2 != null ? userDimension2.getId() : "");
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return data;
  }

  private DocLine[] loadLines() {
    ArrayList<Object> list = new ArrayList<Object>();
    FieldProviderFactory[] data = loadLinesFieldProvider(Record_ID);
    if (data == null || data.length == 0) {
      return null;
    }
    for (int i = 0; i < data.length; i++) {
      if (data[i] == null) {
        continue;
      }
      String Line_ID = data[i].getField("FIN_Payment_Detail_ID");
      DocLine_FINPayment docLine = new DocLine_FINPayment(DocumentType, Record_ID, Line_ID);
      docLine.loadAttributes(data[i], this);
      docLine.setAmount(data[i].getField("Amount"));
      docLine.setAmountExcludingCredit(data[i].getField("AmountExcludingCredit"));
      docLine.setIsPrepayment(data[i].getField("isprepayment"));
      docLine.setWriteOffAmt(data[i].getField("WriteOffAmt"));
      docLine.setC_GLItem_ID(data[i].getField("C_GLItem_ID"));
      list.add(docLine);
    }
    // Return Array
    DocLine_FINPayment[] dl = new DocLine_FINPayment[list.size()];
    list.toArray(dl);
    return dl;
  } // loadLines

  @Override
  public Fact createFact(AcctSchema as, ConnectionProvider conn, Connection con,
      VariablesSecureApp vars) throws ServletException {
    // Select specific definition
    String strClassname = "";
    final StringBuilder whereClause = new StringBuilder();
    Fact fact = new Fact(this, as, Fact.POST_Actual);
    String Fact_Acct_Group_ID = SequenceIdData.getUUID();
    OBContext.setAdminMode();
    try {
      whereClause.append(" as astdt ");
      whereClause.append(
          " where astdt.acctschemaTable.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
      whereClause.append(" and astdt.acctschemaTable.table.id = '" + AD_Table_ID + "'");
      whereClause.append(" and astdt.documentCategory = '" + DocumentType + "'");

      final OBQuery<AcctSchemaTableDocType> obqParameters = OBDal.getInstance()
          .createQuery(AcctSchemaTableDocType.class, whereClause.toString());
      final List<AcctSchemaTableDocType> acctSchemaTableDocTypes = obqParameters.list();

      if (acctSchemaTableDocTypes != null && acctSchemaTableDocTypes.size() > 0) {
        strClassname = acctSchemaTableDocTypes.get(0).getCreatefactTemplate().getClassname();
      }

      if (strClassname.equals("")) {
        final StringBuilder whereClause2 = new StringBuilder();

        whereClause2.append(" as ast ");
        whereClause2.append(" where ast.accountingSchema.id = '" + as.m_C_AcctSchema_ID + "'");
        whereClause2.append(" and ast.table.id = '" + AD_Table_ID + "'");

        final OBQuery<AcctSchemaTable> obqParameters2 = OBDal.getInstance()
            .createQuery(AcctSchemaTable.class, whereClause2.toString());
        final List<AcctSchemaTable> acctSchemaTables = obqParameters2.list();
        if (acctSchemaTables != null && acctSchemaTables.size() > 0
            && acctSchemaTables.get(0).getCreatefactTemplate() != null) {
          strClassname = acctSchemaTables.get(0).getCreatefactTemplate().getClassname();
        }
      }
      if (!strClassname.equals("")) {
        try {
          DocREMRemittanceReturnTemplate newTemplate = (DocREMRemittanceReturnTemplate) Class
              .forName(strClassname)
              .getDeclaredConstructor()
              .newInstance();
          return newTemplate.createFact(this, as, conn, con, vars);
        } catch (Exception e) {
          log4j.error("Error while creating new instance for DocREMRemittanceReturnTemplate - ", e);
        }
      }

      RemittanceLineReturn rlr = OBDal.getInstance().get(RemittanceLineReturn.class, Record_ID);
      final Set<FIN_Payment> processedPayments = new HashSet<FIN_Payment>();
      for (int i = 0; p_lines != null && i < p_lines.length; i++) {
        DocLine_FINPayment line = (DocLine_FINPayment) p_lines[i];

        final FIN_PaymentDetail pd = OBDal.getInstance()
            .get(FIN_PaymentDetail.class, line.getLine_ID());
        final FIN_Payment payment = pd.getFinPayment();
        final boolean isReceipt = payment.isReceipt();
        final boolean isPrepayment = line.getIsPrepayment().equals("Y");

        OBCriteria<RemittanceLine> rmLineList = OBDal.getInstance()
            .createCriteria(RemittanceLine.class);
        rmLineList.add(Restrictions.eq(RemittanceLine.PROPERTY_PAYMENT, payment));

        String bpAmount = line.getAmount();
        String lineAmount = line.getAmountExcludingCredit();

        // Select conversion rate defined in order or in invoice, if any, or select conversion date
        // of transaction
        String faCurrency = rlr.getRemittance().getFinancialAccount().getCurrency().getId();
        String currency = C_Currency_ID;
        BigDecimal conversionRate = null;
        String conversionDate = null;
        if (as.m_C_Currency_ID != null && !as.m_C_Currency_ID.equals(currency)) {
          if (pd.getFINPaymentScheduleDetailList().get(0).getOrderPaymentSchedule() != null
              && pd.getFINPaymentScheduleDetailList()
                  .get(0)
                  .getOrderPaymentSchedule()
                  .getOrder() != null) {
            conversionDate = OBDateUtils.formatDate(rlr.getRemittance().getTransactionDate());
          } else {
            if (pd.getFINPaymentScheduleDetailList() != null
                && !pd.getFINPaymentScheduleDetailList().isEmpty()) {
              if (pd.getFINPaymentScheduleDetailList().get(0).getInvoicePaymentSchedule() != null
                  && pd.getFINPaymentScheduleDetailList()
                      .get(0)
                      .getInvoicePaymentSchedule()
                      .getInvoice() != null) {
                Invoice invoice = pd.getFINPaymentScheduleDetailList()
                    .get(0)
                    .getInvoicePaymentSchedule()
                    .getInvoice();
                if (invoice.getCurrencyConversionRateDocList() != null
                    && !invoice.getCurrencyConversionRateDocList().isEmpty()) {
                  for (ConversionRateDoc rateDoc : invoice.getCurrencyConversionRateDocList()) {
                    if (currency.equals(rateDoc.getCurrency().getId())
                        && as.m_C_Currency_ID.equals(rateDoc.getToCurrency().getId())) {
                      conversionRate = rateDoc.getRate();
                      break;
                    }
                  }
                } else {
                  conversionDate = OBDateUtils.formatDate(invoice.getAccountingDate());
                }
              } else {
                if (as.m_C_Currency_ID.equals(faCurrency)
                    && payment.getFinancialTransactionConvertRate() != null) {
                  // Select conversion rate of payment header, if applies
                  conversionRate = payment.getFinancialTransactionConvertRate();
                }
              }
            }
          }
        }

        if (getPaymentConfirmation(conn, payment.getId(),
            rmLineList.list().get(0).getOrigstatus())) {
          fact.createLine(line,
              getAccountPayment(conn, payment.getPaymentMethod(), payment.getAccount(), as,
                  isReceipt),
              currency, (isReceipt ? line.getAmount() : ""), (isReceipt ? "" : line.getAmount()),
              Fact_Acct_Group_ID, "99999", DocumentType, conversionDate, conversionRate, conn);
        } else {
          if (line.getWriteOffAmt() != null && !line.getWriteOffAmt().equals("")
              && new BigDecimal(line.getWriteOffAmt()).compareTo(ZERO) != 0) {
            fact.createLine(line, getAccount(AcctServer.ACCTTYPE_WriteOffDefault, as, conn),
                currency, (!isReceipt ? line.getWriteOffAmt() : ""),
                (!isReceipt ? "" : line.getWriteOffAmt()), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                DocumentType, conversionDate, conversionRate, conn);
            bpAmount = new BigDecimal(bpAmount).add(new BigDecimal(line.getWriteOffAmt()))
                .toString();
          }
          if ("".equals(line.getC_GLItem_ID())) {
            fact.createLine(line,
                getAccountBPartner((line.m_C_BPartner_ID == null || line.m_C_BPartner_ID.equals(""))
                    ? this.C_BPartner_ID
                    : line.m_C_BPartner_ID, as, isReceipt, isPrepayment, conn),
                currency, (!isReceipt ? "" : bpAmount), (!isReceipt ? bpAmount : ""),
                Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType, conversionDate, conversionRate,
                conn);
          } else {
            fact.createLine(line,
                getAccountGLItem(OBDal.getInstance().get(GLItem.class, line.getC_GLItem_ID()), as,
                    isReceipt, conn),
                currency, (!isReceipt ? "" : bpAmount), (!isReceipt ? bpAmount : ""),
                Fact_Acct_Group_ID, nextSeqNo(SeqNo), DocumentType, conversionDate, conversionRate,
                conn);
          }
        }

        fact.createLine(line, getAccount(conn, payment, as, true), currency,
            (!isReceipt ? lineAmount : ""), (!isReceipt ? "" : lineAmount), Fact_Acct_Group_ID,
            "999999", DocumentType, conversionDate, conversionRate, conn);
        // TODO:Review and check if required here
        // Pre-payment is consumed when Used Credit Amount not equals Zero. When consuming Credit
        // no credit is generated
        if (!processedPayments.contains(payment)) {
          processedPayments.add(payment);
          if (new BigDecimal(usedAmount).compareTo(ZERO) != 0
              && new BigDecimal(generatedAmount).compareTo(ZERO) == 0) {
            fact.createLine(null,
                getAccountBPartner(C_BPartner_ID, as, payment.isReceipt(), true, conn),
                C_Currency_ID, (!payment.isReceipt() ? usedAmount : ""),
                (!payment.isReceipt() ? "" : usedAmount), Fact_Acct_Group_ID, nextSeqNo(SeqNo),
                DocumentType, conn);
          }
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    SeqNo = "0";
    return fact;
  }

  public String nextSeqNo(String oldSeqNo) {
    BigDecimal seqNo = new BigDecimal(oldSeqNo);
    SeqNo = (seqNo.add(new BigDecimal("10"))).toString();
    return SeqNo;
  }

  /**
   * Get Source Currency Balance - subtracts line amounts from total - no rounding As this document
   * has just one line always maps the grand total so ZERO is returned
   * 
   * @return positive amount, if total is bigger than lines
   */
  @Override
  public BigDecimal getBalance() {
    BigDecimal retValue = ZERO;
    return retValue;
  } // getBalance

  @Override
  public boolean getDocumentConfirmation(ConnectionProvider conn, String strRecordId) {
    boolean confirmation = true;
    RemittanceLineReturn remittanceLineReturn = OBDal.getInstance()
        .get(RemittanceLineReturn.class, strRecordId);
    if (!remittanceLineReturn.getRemittance().getRemittanceType().isPostingAllowed()) {
      confirmation = false;
    }
    if (!confirmation) {
      setStatus(STATUS_DocumentDisabled);
    }
    return confirmation;
  }

  public boolean getPaymentConfirmation(ConnectionProvider conn, String strRecordId,
      String OrigStatus) {
    // Checks if payment with original status 'payment received' is configured to generate
    // accounting
    boolean confirmation = false;
    final String PAYMENT_RECEIVED = "RPR";

    OBContext.setAdminMode();
    try {
      FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, strRecordId);
      // Posting can just happen if payment is in the right status
      if (PAYMENT_RECEIVED.equals(OrigStatus)) {
        OBCriteria<FinAccPaymentMethod> obCriteria = OBDal.getInstance()
            .createCriteria(FinAccPaymentMethod.class);
        obCriteria.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT, payment.getAccount()));
        obCriteria.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD,
            payment.getPaymentMethod()));
        obCriteria.setFilterOnReadableClients(false);
        obCriteria.setFilterOnReadableOrganization(false);
        List<FinAccPaymentMethod> lines = obCriteria.list();
        List<FIN_FinancialAccountAccounting> accounts = payment.getAccount()
            .getFINFinancialAccountAcctList();
        for (FIN_FinancialAccountAccounting account : accounts) {
          if (confirmation) {
            return confirmation;
          }
          if (payment.isReceipt()) {
            if (("INT").equals(lines.get(0).getUponReceiptUse())
                && account.getInTransitPaymentAccountIN() != null) {
              confirmation = true;
            } else if (("DEP").equals(lines.get(0).getUponReceiptUse())
                && account.getDepositAccount() != null) {
              confirmation = true;
            } else if (("CLE").equals(lines.get(0).getUponReceiptUse())
                && account.getClearedPaymentAccount() != null) {
              confirmation = true;
            }
          }
          // For payments with Amount ZERO always create an entry as no transaction will be created
          if (payment.getAmount().compareTo(ZERO) == 0) {
            confirmation = true;
          }
        }
      }
    } catch (Exception e) {
      setStatus(STATUS_DocumentDisabled);
      return confirmation;
    } finally {
      OBContext.restorePreviousMode();
    }
    return confirmation;
  }

  @Override
  public void loadObjectFieldProvider(ConnectionProvider conn, String strAD_Client_ID, String Id)
      throws ServletException {
    RemittanceLineReturn rlr = OBDal.getInstance().get(RemittanceLineReturn.class, Id);
    FIN_Payment payment = rlr.getPayment();
    FieldProviderFactory[] data = new FieldProviderFactory[1];
    data[0] = new FieldProviderFactory(null);
    FieldProviderFactory.setField(data[0], "AD_Client_ID", payment.getClient().getId());
    FieldProviderFactory.setField(data[0], "AD_Org_ID", payment.getOrganization().getId());
    FieldProviderFactory.setField(data[0], "C_BPartner_ID",
        payment.getBusinessPartner() != null ? payment.getBusinessPartner().getId() : "");
    FieldProviderFactory.setField(data[0], "DocumentNo", payment.getDocumentNo());
    String dateFormat = OBPropertiesProvider.getInstance()
        .getOpenbravoProperties()
        .getProperty("dateFormat.java");
    SimpleDateFormat outputFormat = new SimpleDateFormat(dateFormat);
    FieldProviderFactory.setField(data[0], "PaymentDate",
        outputFormat.format(payment.getPaymentDate()));
    FieldProviderFactory.setField(data[0], "DateAcct",
        outputFormat.format(rlr.getAccountingDate()));
    // FieldProviderFactory.setField(data[0], "C_DocType_ID", payment.getDocumentType().getId());
    FieldProviderFactory.setField(data[0], "C_Currency_ID", payment.getCurrency().getId());
    FieldProviderFactory.setField(data[0], "Amount", payment.getAmount().toString());
    FieldProviderFactory.setField(data[0], "GeneratedCredit",
        payment.getGeneratedCredit().toString());
    FieldProviderFactory.setField(data[0], "UsedCredit", payment.getUsedCredit().toString());
    FieldProviderFactory.setField(data[0], "WriteOffAmt", payment.getWriteoffAmount().toString());
    FieldProviderFactory.setField(data[0], "Description", payment.getDescription());
    FieldProviderFactory.setField(data[0], "Posted", rlr.getPosted());
    FieldProviderFactory.setField(data[0], "Processed", rlr.isProcessed() ? "Y" : "N");
    FieldProviderFactory.setField(data[0], "Processing", rlr.isProcessNow() ? "Y" : "N");
    FieldProviderFactory.setField(data[0], "C_Project_ID",
        payment.getProject() != null ? payment.getProject().getId() : "");
    FieldProviderFactory.setField(data[0], "C_Campaign_ID",
        payment.getSalesCampaign() != null ? payment.getSalesCampaign().getId() : "");
    FieldProviderFactory.setField(data[0], "C_Activity_ID",
        payment.getActivity() != null ? payment.getActivity().getId() : "");
    // This lines can be uncommented when User1 and User2 are implemented
    // FieldProviderFactory.setField(data[0], "User1_ID", payment.getStDimension().getId());
    // FieldProviderFactory.setField(data[0], "User2_ID", payment.getNdDimension().getId());
    setObjectFieldProvider(data);
  }

  /*
   * Retrieves Account for receipt / Payment: both for sent account and canceled account
   */
  public Account getAccount(ConnectionProvider conn, FIN_Payment payment, AcctSchema as,
      boolean bIsSentAccout) throws ServletException {
    RemittanceLineReturn remittanceReturn = OBDal.getInstance()
        .get(RemittanceLineReturn.class, Record_ID);
    Remittance remittance = remittanceReturn.getRemittance();
    OBContext.setAdminMode();
    Account account = null;
    try {
      OBCriteria<RemittanceTypeAccount> accounts = OBDal.getInstance()
          .createCriteria(RemittanceTypeAccount.class);
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_ACCOUNTINGSCHEMA,
          OBDal.getInstance()
              .get(org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
                  as.getC_AcctSchema_ID())));
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_REMITTANCETYPE,
          OBDal.getInstance().get(RemittanceType.class, remittance.getRemittanceType().getId())));
      accounts.add(Restrictions.eq(RemittanceTypeAccount.PROPERTY_ACTIVE, true));
      accounts.setFilterOnReadableClients(false);
      accounts.setFilterOnReadableOrganization(false);
      accounts.setMaxResults(1);
      List<RemittanceTypeAccount> accountList = accounts.list();
      if (accountList == null || accountList.size() == 0) {
        return null;
      }
      if (bIsSentAccout) {
        account = Account.getAccount(conn, accountList.get(0).getSentAccount().getId());
      } else {
        account = Account.getAccount(conn, accountList.get(0).getSettlementAccount().getId());
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return account;
  }

  public Account getAccountPayment(ConnectionProvider conn, FIN_PaymentMethod paymentMethod,
      FIN_FinancialAccount finAccount, AcctSchema as, boolean bIsReceipt) throws ServletException {
    OBContext.setAdminMode();
    Account account = null;
    try {
      OBCriteria<FIN_FinancialAccountAccounting> accounts = OBDal.getInstance()
          .createCriteria(FIN_FinancialAccountAccounting.class);
      accounts.add(Restrictions.eq(FIN_FinancialAccountAccounting.PROPERTY_ACCOUNT, finAccount));
      accounts.add(Restrictions.eq(FIN_FinancialAccountAccounting.PROPERTY_ACCOUNTINGSCHEMA,
          OBDal.getInstance()
              .get(org.openbravo.model.financialmgmt.accounting.coa.AcctSchema.class,
                  as.m_C_AcctSchema_ID)));
      accounts.add(Restrictions.eq(FIN_FinancialAccountAccounting.PROPERTY_ACTIVE, true));
      accounts.setFilterOnReadableClients(false);
      accounts.setFilterOnReadableOrganization(false);
      List<FIN_FinancialAccountAccounting> accountList = accounts.list();
      if (accountList == null || accountList.size() == 0) {
        return null;
      }
      OBCriteria<FinAccPaymentMethod> accPaymentMethod = OBDal.getInstance()
          .createCriteria(FinAccPaymentMethod.class);
      accPaymentMethod.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT, finAccount));
      accPaymentMethod
          .add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD, paymentMethod));
      accPaymentMethod.setFilterOnReadableClients(false);
      accPaymentMethod.setFilterOnReadableOrganization(false);
      List<FinAccPaymentMethod> lines = accPaymentMethod.list();
      if (bIsReceipt) {
        account = getAccount(conn, lines.get(0).getUponReceiptUse(), accountList.get(0),
            bIsReceipt);
      } else {
        account = getAccount(conn, lines.get(0).getUponPaymentUse(), accountList.get(0),
            bIsReceipt);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return account;
  }

  public String getSeqNo() {
    return SeqNo;
  }

  public void setSeqNo(String seqNo) {
    SeqNo = seqNo;
  }

  public String getGeneratedAmount() {
    return generatedAmount;
  }

  public void setGeneratedAmount(String generatedAmount) {
    this.generatedAmount = generatedAmount;
  }

  public String getUsedAmount() {
    return usedAmount;
  }

  public void setUsedAmount(String usedAmount) {
    this.usedAmount = usedAmount;
  }
}
