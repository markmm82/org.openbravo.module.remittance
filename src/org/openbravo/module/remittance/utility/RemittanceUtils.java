/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.remittance.utility;

import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.accounting.Costcenter;
import org.openbravo.model.financialmgmt.accounting.UserDimension1;
import org.openbravo.model.financialmgmt.accounting.UserDimension2;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;

/**
 * This class provides general static methods that can be used by other classes
 */
public class RemittanceUtils {

  /**
   * This method is used to know if the Payment Detail has a related {@link Invoice} (by checking if
   * there is a related {@link FIN_PaymentSchedule} associated to an Invoice)
   * 
   * @param detail
   *          A {@link FIN_PaymentDetail} object
   * @return True if there is an {@link Invoice} related to the {@link FIN_PaymentDetail} Parameter.
   *         False otherwise
   */
  public static boolean paymentDetailHasRelatedInvoice(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getInvoicePaymentSchedule() != null;
  }

  /**
   * This method is used to know if the Payment Detail has a related {@link Order} (by checking if
   * there is a related {@link FIN_PaymentSchedule} associated to an Order)
   * 
   * @param detail
   *          A {@link FIN_PaymentDetail} object
   * @return True if there is an {@link Order} related to the {@link FIN_PaymentDetail} Parameter.
   *         False otherwise
   */
  public static boolean paymentDetailHasRelatedOrder(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getOrderPaymentSchedule() != null;
  }

  /**
   * This Method retrieves the {@link Costcenter} that can be associated to the
   * {@link FIN_PaymentDetail} Parameter. For that it checks:
   * <ul>
   * <li>1. If there is a {@link FIN_Payment} with a {@link Costcenter} associated, it is returned</li>
   * <li>2. If there is a {@link Invoice} with a {@link Costcenter} associated, it is returned</li>
   * <li>3. If there is a {@link Order} with a {@link Costcenter} associated, it is returned</li>
   * <li>4. If not, it returns null</li>
   * </ul>
   * 
   * @param detail
   *          A {@link FIN_PaymentDetail} object
   * @return A {@link Costcenter} or null if there is no Cost Center associated to either the
   *         {@link FIN_Payment}, the {@link Invoice} or the {@link Order}
   */
  public static Costcenter getCostCenterFromPaymentOrInvoiceOrOrder(FIN_PaymentDetail detail) {
    if (paymentHasCostCenter(detail)) {
      return getCostCenterFromPayment(detail);
    } else if (paymentDetailHasRelatedInvoice(detail) && invoiceHasCostCenter(detail)) {
      return getCostCenterFromInvoice(detail);
    } else if (paymentDetailHasRelatedOrder(detail) && orderHasCostCenter(detail)) {
      return getCostCenterFromOrder(detail);
    } else {
      return null;
    }
  }

  private static boolean paymentHasCostCenter(FIN_PaymentDetail detail) {
    return getCostCenterFromPayment(detail) != null;
  }

  private static Costcenter getCostCenterFromPayment(FIN_PaymentDetail detail) {
    return detail.getFinPayment().getCostCenter();
  }

  private static boolean invoiceHasCostCenter(FIN_PaymentDetail detail) {
    return getCostCenterFromInvoice(detail) != null;
  }

  private static Costcenter getCostCenterFromInvoice(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getInvoicePaymentSchedule().getInvoice()
        .getCostcenter();
  }

  private static boolean orderHasCostCenter(FIN_PaymentDetail detail) {
    return getCostCenterFromOrder(detail) != null;
  }

  private static Costcenter getCostCenterFromOrder(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getOrderPaymentSchedule().getOrder()
        .getCostcenter();
  }

  /**
   * This Method retrieves the {@link UserDimension1} that can be associated to the
   * {@link FIN_PaymentDetail} Parameter. For that it checks:
   * <ul>
   * <li>1. If there is a {@link FIN_Payment} with a {@link UserDimension1} associated, it is
   * returned</li>
   * <li>2. If there is a {@link Invoice} with a {@link UserDimension1} associated, it is returned</li>
   * <li>3. If there is a {@link Order} with a {@link UserDimension1} associated, it is returned</li>
   * <li>4. If not, it returns the {@link UserDimension1} associated with the
   * {@link FIN_PaymentDetail} Parameter. This can be null</li>
   * </ul>
   * 
   * @param detail
   *          A {@link UserDimension1} object
   * @return A {@link UserDimension1} or null if there is no User Dimension 1 associated to either
   *         the {@link FIN_Payment}, the {@link Invoice}, the {@link Order} or the
   *         {@link FIN_PaymentDetail} Parameter
   */
  public static UserDimension1 getUserDimension1FromPaymentOrInvoiceOrOrder(FIN_PaymentDetail detail) {
    if (paymentHasUserDimension1(detail)) {
      return getUserDimension1FromPayment(detail);
    } else if (paymentDetailHasRelatedInvoice(detail) && invoiceHasUserDimension1(detail)) {
      return getUserDimension1FromInvoice(detail);
    } else if (paymentDetailHasRelatedOrder(detail) && orderHasUserDimension1(detail)) {
      return getUserDimension1FromOrder(detail);
    } else {
      return getUserDimension1FromDetail(detail);
    }
  }

  private static boolean paymentHasUserDimension1(FIN_PaymentDetail detail) {
    return getUserDimension1FromPayment(detail) != null;
  }

  private static UserDimension1 getUserDimension1FromPayment(FIN_PaymentDetail detail) {
    return detail.getFinPayment().getStDimension();
  }

  private static boolean invoiceHasUserDimension1(FIN_PaymentDetail detail) {
    return getUserDimension1FromInvoice(detail) != null;
  }

  private static UserDimension1 getUserDimension1FromInvoice(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getInvoicePaymentSchedule().getInvoice()
        .getStDimension();
  }

  private static boolean orderHasUserDimension1(FIN_PaymentDetail detail) {
    return getUserDimension1FromOrder(detail) != null;
  }

  private static UserDimension1 getUserDimension1FromOrder(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getOrderPaymentSchedule().getOrder()
        .getStDimension();
  }

  private static UserDimension1 getUserDimension1FromDetail(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getStDimension();
  }

  /**
   * This Method retrieves the {@link UserDimension2} that can be associated to the
   * {@link FIN_PaymentDetail} Parameter. For that it checks:
   * <ul>
   * <li>1. If there is a {@link FIN_Payment} with a {@link UserDimension2} associated, it is
   * returned</li>
   * <li>2. If there is a {@link Invoice} with a {@link UserDimension2} associated, it is returned</li>
   * <li>3. If there is a {@link Order} with a {@link UserDimension2} associated, it is returned</li>
   * <li>4. If not, it returns the {@link UserDimension2} associated with the
   * {@link FIN_PaymentDetail} Parameter. This can be null</li>
   * </ul>
   * 
   * @param detail
   *          A {@link UserDimension2} object
   * @return A {@link UserDimension2} or null if there is no User Dimension 1 associated to either
   *         the {@link FIN_Payment}, the {@link Invoice}, the {@link Order} or the
   *         {@link FIN_PaymentDetail} Parameter
   */
  public static UserDimension2 getUserDimension2FromPaymentOrInvoiceOrOrder(FIN_PaymentDetail detail) {
    if (paymentHasUserDimension2(detail)) {
      return getUserDimension2FromPayment(detail);
    } else if (paymentDetailHasRelatedInvoice(detail) && invoiceHasUserDimension2(detail)) {
      return getUserDimension2FromInvoice(detail);
    } else if (paymentDetailHasRelatedOrder(detail) && orderHasUserDimension2(detail)) {
      return getUserDimension2FromOrder(detail);
    } else {
      return getUserDimension2FromDetail(detail);
    }
  }

  private static boolean paymentHasUserDimension2(FIN_PaymentDetail detail) {
    return getUserDimension1FromPayment(detail) != null;
  }

  private static UserDimension2 getUserDimension2FromPayment(FIN_PaymentDetail detail) {
    return detail.getFinPayment().getNdDimension();
  }

  private static boolean invoiceHasUserDimension2(FIN_PaymentDetail detail) {
    return getUserDimension1FromInvoice(detail) != null;
  }

  private static UserDimension2 getUserDimension2FromInvoice(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getInvoicePaymentSchedule().getInvoice()
        .getNdDimension();
  }

  private static boolean orderHasUserDimension2(FIN_PaymentDetail detail) {
    return getUserDimension1FromOrder(detail) != null;
  }

  private static UserDimension2 getUserDimension2FromOrder(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getOrderPaymentSchedule().getOrder()
        .getNdDimension();
  }

  private static UserDimension2 getUserDimension2FromDetail(FIN_PaymentDetail detail) {
    return detail.getFINPaymentScheduleDetailList().get(0).getNdDimension();
  }

}
