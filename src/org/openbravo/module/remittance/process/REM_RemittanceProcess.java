/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2019 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.businesspartner.BankAccount;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;
import org.openbravo.module.remittance.Instruction;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalConnectionProvider;

public class REM_RemittanceProcess implements org.openbravo.scheduling.Process {
  private static AdvPaymentMngtDao dao;
  public static final String STATUS_SENT = "REM_SENT";
  public static final String STATUS_AWAITINGEXECUTION = "RPAE";
  public static final String STATUS_RECEIVED = "RPR";
  // Processing actions
  private static final String GENERATE_PAYMENT = "P";
  private static final String GENERATE_GROUPED_PAYMENT = "GGP";
  private static final String GENERATED_GROUPED_PAYMENT_DUEDATE = "GGPD";
  private static final String GENERATED_GROUPED_PAYMENT_INVOICE = "GGPI";
  private static final String GENERATED_GROUPED_PAYMENT_INVOICE_DUEDATE = "GGID";
  private String strRemittanceId = "";

  private OBError error = new OBError();

  private long line = 0l;

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    dao = new AdvPaymentMngtDao();
    OBError msg = new OBError();
    msg.setType("Success");
    msg.setTitle(
        Utility.messageBD(bundle.getConnection(), "Success", bundle.getContext().getLanguage()));

    OBContext.setAdminMode();
    try {
      // retrieve custom params
      final String strAction = (String) bundle.getParams().get("action");

      // retrieve standard params
      final String recordID = (String) bundle.getParams().get("REM_Remittance_ID");
      strRemittanceId = recordID;
      Remittance remittance = dao.getObject(Remittance.class, recordID);

      final VariablesSecureApp vars = bundle.getContext().toVars();
      final ConnectionProvider conProvider = bundle.getConnection();

      boolean process = strAction.equals(GENERATE_PAYMENT)
          || strAction.equals(GENERATE_GROUPED_PAYMENT)
          || strAction.equals(GENERATED_GROUPED_PAYMENT_DUEDATE)
          || strAction.equals(GENERATED_GROUPED_PAYMENT_INVOICE)
          || strAction.equals(GENERATED_GROUPED_PAYMENT_INVOICE_DUEDATE);
      if (process) {
        // Check that there is no repeated payments in the remittance
        final String hql = "select payment.id from REM_RemittanceLine where remittance.id = :remittanceId group by payment.id "
            + " having count(payment.id) > 1";
        final Session session = OBDal.getInstance().getSession();
        final Query<String> query = session.createQuery(hql, String.class);
        query.setParameter("remittanceId", recordID);
        final ScrollableResults scroller = query.scroll(ScrollMode.FORWARD_ONLY);
        try {
          if (scroller.next()) {
            FIN_Payment payment = (FIN_Payment) OBDal.getInstance()
                .get(FIN_Payment.ENTITY_NAME, (String) scroller.get()[0]);
            msg.setType("Error");
            msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
            msg.setMessage(String.format(
                Utility.messageBD(conProvider, "REM_PaymentDuplicated", vars.getLanguage()),
                payment.getDocumentNo()));
            bundle.setResult(msg);
            return;
          }
        } finally {
          scroller.close();
        }
      }

      boolean isReceipt = remittance.getRemittanceType().getPaymentMethod().isPayinAllow();
      remittance.setProcessNow(true);
      OBDal.getInstance().save(remittance);
      OBDal.getInstance().flush();
      if (process) {
        // Check lines exist
        if (remittance.getREMRemittanceLineList().size() == 0) {
          msg.setType("Error");
          msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
          msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
              "@REM_RemittanceNoLines@" + ": " + remittance.getDocumentNo()));
          bundle.setResult(msg);
          return;
        }

        // Check remittances created from orders/invoices do not have an out of range payment amount
        for (RemittanceLine remLine : remittance.getREMRemittanceLineList()) {
          if (remLine.getPaymentScheduleDetail() != null && remLine.getPaymentScheduleDetail()
              .getAmount()
              .abs()
              .compareTo(remLine.getAmount().abs()) < 0) {
            msg.setType("Error");
            msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
            msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
                String.format(FIN_Utility.messageBD("REM_OutOfRangeAmount"), remLine.getLineNo())));
            bundle.setResult(msg);
            return;
          }
        }

        // if (FIN_Utility.getOneInstance(RemittanceLine.class, new Value(
        // RemittanceLine.PROPERTY_PAYMENT, null, "!="), new Value(
        // RemittanceLine.PROPERTY_REMITTANCE, remittance)) != null
        // && remittance.isSinglePayment()) {
        // // Check lines not payment
        // msg.setType("Error");
        // msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
        // msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
        // "@REM_RemittancePaymentsNotAllowed@" + ": " + remittance.getDocumentNo()));
        // bundle.setResult(msg);
        // return;
        // }

        // Discount remittance cannot be processed if the discount date is empty
        if (remittance.getRemittanceType().isRemitForDiscount()
            && remittance.getChargedate() == null) {
          msg.setType("Error");
          msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
          msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
              "@REM_DiscountDate_Mandatory@"));
          bundle.setResult(msg);
          return;
        }

        // Check PM setup: special configuration needed for remittances
        if (isWrongRemTypePayMethodConfig(remittance)) {
          msg.setType("Error");
          msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
          msg.setMessage(String.format(
              Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
                  "@REM_RemittanceType_WrongPMConfig@"),
              remittance.getRemittanceType().getPaymentMethod().getName(),
              remittance.getFinancialAccount().getName()));
          bundle.setResult(msg);
          return;
        }

        if (strAction.equals(GENERATE_PAYMENT)) {
          createPayments(remittance, bundle);
        } else if (strAction.equals(GENERATE_GROUPED_PAYMENT)) {
          createGroupedPayments(remittance, false, false);
        } else if (strAction.equals(GENERATED_GROUPED_PAYMENT_DUEDATE)) {
          createGroupedPayments(remittance, true, false);
        } else if (strAction.equals(GENERATED_GROUPED_PAYMENT_INVOICE)) {
          createGroupedPayments(remittance, false, true);
        } else if (strAction.equals(GENERATED_GROUPED_PAYMENT_INVOICE_DUEDATE)) {
          createGroupedPayments(remittance, true, true);
        }

        if (!error.getMessage().equalsIgnoreCase("")) {
          msg.setType("Warning");
          msg.setMessage(msg.getMessage() + "\n" + error.getMessage());
        }

        remittance = OBDal.getInstance().get(Remittance.class, strRemittanceId);
        if (remittance.getRemittanceType().isRemitForDiscount() && isReceipt) {
          FIN_PaymentMethod pm = remittance.getRemittanceType().getPaymentMethodForDiscount();
          List<FinAccPaymentMethod> financialAccountPMs = remittance.getFinancialAccount()
              .getFinancialMgmtFinAccPaymentMethodList();
          List<FIN_PaymentMethod> paymentMethods = new ArrayList<FIN_PaymentMethod>();
          for (FinAccPaymentMethod faPM : financialAccountPMs) {
            if (!paymentMethods.contains(faPM.getPaymentMethod())) {
              paymentMethods.add(faPM.getPaymentMethod());
            }
          }
          if (!paymentMethods.contains(pm)) {
            throw new OBException(Utility.messageBD(conProvider,
                "REM_PMConsolidateNotInFinancialAccount", vars.getLanguage()), false);
          }
          if (remittance.getFinancialAccount().getBusinessPartner() == null) {
            throw new OBException(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
                "@REM_NoFinancialAccountBPartner@" + ": "
                    + remittance.getFinancialAccount().getIdentifier()),
                false);
          }
          FIN_Payment payment = createConsolidatedPayment(remittance);
          if (payment != null) {
            remittance.setPayment(payment);
            OBDal.getInstance().save(payment);
            OBDal.getInstance().flush();
          }
        }
        remittance.setProcessRemittance("RE");
        remittance.setProcessed(true);
        OBDal.getInstance().save(remittance);
        OBDal.getInstance().flush();

        // ***********************
        // Reactivate Remittance
        // ***********************
      } else if (strAction.equals("R")) {
        // Already Posted Document
        if ("Y".equals(remittance.getPosted())) {
          msg.setType("Error");
          msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
          msg.setMessage(Utility.parseTranslation(conProvider, vars, vars.getLanguage(),
              "@PostedDocument@" + ": " + remittance.getDocumentNo()));
          bundle.setResult(msg);
          return;
        }
        if (!remittanceCanBeReactivated(remittance)) {
          msg.setType("Error");
          msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
          msg.setMessage(String.format(OBMessageUtils.messageBD("REM_CanNotReactivateRemittance"),
              remittance.getDocumentNo()));
          bundle.setResult(msg);
          return;
        }
        // Return/Cancel exists
        if (remittance.getREMRemittanceLineCancelList().size() > 0
            || remittance.getREMRemittanceLineReturnList().size() > 0) {
          msg.setType("Error");
          msg.setTitle(Utility.messageBD(conProvider, "Error", vars.getLanguage()));
          msg.setMessage(Utility.messageBD(conProvider, "@REM_RemittanceWithCanceledReturned@",
              vars.getLanguage()));
          bundle.setResult(msg);
          return;
        }
        remittance.setProcessed(false);
        OBDal.getInstance().save(remittance);
        OBDal.getInstance().flush();
        if (remittance.getPayment() != null) {
          try {
            FIN_Payment consolidatedPayment = remittance.getPayment();
            ConnectionProvider conn = new DalConnectionProvider();
            // Consolidate payment
            OBError err = FIN_AddPayment
                .processPayment(
                    new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
                        OBContext.getOBContext().getCurrentClient().getId(),
                        OBContext.getOBContext().getCurrentOrganization().getId(),
                        OBContext.getOBContext().getRole().getId()),
                    conn, "R", consolidatedPayment);
            if ("Error".equals(err.getType())) {
              throw new OBException(err.getMessage(), false);
            }
            remittance.setPayment(null);
            OBDal.getInstance().save(remittance);
            OBDal.getInstance().flush();
            OBDal.getInstance().remove(consolidatedPayment);
            OBDal.getInstance().flush();
          } catch (Exception e) {
            throw new OBException(Utility.messageBD(conProvider,
                "@REM_ConsolidatedPaymentCanNotBeDeleted@", vars.getLanguage()), false);
          }
        }
        remittance.setProcessRemittance("P");
        OBDal.getInstance().save(remittance);
        OBDal.getInstance().flush();

        Set<String> paymentIdToRemove = new HashSet<String>();
        for (RemittanceLine remLine : remittance.getREMRemittanceLineList()) {
          if (remLine.getPayment() == null) {
            continue;
          }
          FIN_Payment payment = OBDal.getInstance()
              .get(FIN_Payment.class, remLine.getPayment().getId());
          if (remLine.getPaymentScheduleDetail() != null) {
            if (payment == null) {
              continue;
            }
            paymentIdToRemove.add(payment.getId()); // To be removed later
            // Clean all lines which point to this payment
            for (String relatedLineId : getRemittanceLines(payment.getId(), remittance.getId())) {
              RemittanceLine relatedLine = OBDal.getInstance()
                  .get(RemittanceLine.class, relatedLineId);
              relatedLine.setPayment(null);
              OBDal.getInstance().save(relatedLine);
              OBDal.getInstance().flush();
            }
            if (payment.isProcessed()) {
              // Set status to Awaiting Execution before reactivating. This way the payment process
              // method does not update the payment monitor neither the customer credit.
              payment.setStatus("RPAE");
              OBDal.getInstance().save(payment);
              OBDal.getInstance().flush();
              ConnectionProvider conn = new DalConnectionProvider();
              OBError err = FIN_AddPayment
                  .processPayment(new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
                      OBContext.getOBContext().getCurrentClient().getId(),
                      OBContext.getOBContext().getCurrentOrganization().getId(),
                      OBContext.getOBContext().getRole().getId()), conn, "R", payment);
              if ("Error".equals(err.getType())) {
                throw new OBException(err.getMessage(), false);
              }
            }
          } else {
            if (STATUS_RECEIVED.equals(remLine.getOrigstatus())) {
              payment.setStatus(STATUS_RECEIVED);
            } else {
              payment.setStatus(STATUS_AWAITINGEXECUTION);
            }
            OBDal.getInstance().save(payment);
            OBDal.getInstance().flush();
          }
        }
        // Remove payments
        for (String paymentId : paymentIdToRemove) {
          FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, paymentId);
          OBDal.getInstance().remove(payment);
        }
        // Remove instructions
        for (Instruction instr : remittance.getREMInstructionList()) {
          OBDal.getInstance().remove(instr);
        }
        remittance.getREMInstructionList().clear();
      }
      remittance.setProcessNow(false);
      OBDal.getInstance().save(remittance);
      OBDal.getInstance().flush();
      bundle.setResult(msg);
    } catch (final Exception e) {
      msg.setType("Error");
      msg.setTitle(
          Utility.messageBD(bundle.getConnection(), "Error", bundle.getContext().getLanguage()));
      msg.setMessage(FIN_Utility.getExceptionMessage(e));
      bundle.setResult(msg);
      OBDal.getInstance().rollbackAndClose();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * The remittance can not be reactivated if exists a line in other non processed remittances,
   * linked to the same order and invoice of lines in this remittance
   */
  private boolean remittanceCanBeReactivated(Remittance remittance) {
    // @formatter:off
    String hqlStr = "select 1 from REM_Remittance r join r.rEMRemittanceLineList rl " 
        + " where r.id = :remittanceId " 
        + " and exists ( " 
        + " select 1 from REM_RemittanceLine rll where  " 
        + " rll.paymentScheduleDetail.orderPaymentSchedule.order.id = rl.paymentScheduleDetail.orderPaymentSchedule.order.id " 
        + " and rll.paymentScheduleDetail.invoicePaymentSchedule.invoice.id = rl.paymentScheduleDetail.invoicePaymentSchedule.invoice.id "
        + " and rll.remittance.id <> r.id and rll.remittance.processed = false ) "; 
    // @formatter:on

    Query<Integer> query = OBDal.getInstance().getSession().createQuery(hqlStr, Integer.class);
    query.setParameter("remittanceId", remittance.getId());
    query.setMaxResults(1);

    return query.uniqueResult() == null;
  }

  /**
   * It creates payments grouping remittance lines by business partner
   * 
   * @param remittance
   *          Remittance.
   * @param bundle
   *          Process bundle.
   */
  private void createGroupedPayments(Remittance remittance, boolean isDateGroup,
      boolean isInvoiceGroup) throws Exception {
    StringBuilder whereClause = new StringBuilder();
    Remittance rem = remittance;
    FIN_PaymentMethod pm = rem.getRemittanceType().getPaymentMethod();

    // Remittance lines of payments cannot be grouped
    // Create new bank instruction line for each one
    for (String lineId : getRemittanceLines(rem.getId())) {
      RemittanceLine remLine = OBDal.getInstance().get(RemittanceLine.class, lineId);
      FIN_Payment payment = remLine.getPayment();
      if (payment != null) {
        HashMap<String, String> remHash = REM_AddRemittance.existLineInSettledRemittance(payment);
        if (!remHash.isEmpty()) {
          throw new OBException(
              String.format(FIN_Utility.messageBD("REM_JustOnceInRemittanceLines"),
                  remHash.get("documentno"), remHash.get("lineno")),
              false);
        }
        addRemittanceBankInstruction(rem, payment);
      }
    }
    whereClause.append(" select remline.id from REM_RemittanceLine ");
    // Get remittance lines ordered by business partner of the invoice/order
    whereClause.append(" as remline join remline.paymentScheduleDetail as psd");
    whereClause.append(" left outer join psd.orderPaymentSchedule as ops");
    whereClause.append(" left outer join ops.order as ord");
    whereClause.append(" left outer join psd.invoicePaymentSchedule as ips");
    whereClause.append(" left outer join ips.invoice as inv");
    whereClause.append(" where remline.remittance.id = :remittanceId");
    whereClause.append(" order by coalesce(inv.currency, ord.currency), ");
    whereClause.append(" coalesce(inv.businessPartner, ord.businessPartner), ");
    whereClause.append(" coalesce(ips.expectedDate, ops.expectedDate), ");
    whereClause.append(" coalesce(inv.salesTransaction, ord.salesTransaction), ");
    whereClause.append(" coalesce(inv.id, ord.id) ");

    final Query<String> remitanceLineQuery = OBDal.getInstance()
        .getSession()
        .createQuery(whereClause.toString(), String.class);
    remitanceLineQuery.setParameter("remittanceId", rem.getId());

    List<String> lines = remitanceLineQuery.list();
    List<FIN_PaymentScheduleDetail> psdList = new ArrayList<FIN_PaymentScheduleDetail>();

    List<RemittanceLine> remLines = new ArrayList<RemittanceLine>();
    HashMap<String, BigDecimal> hm = new HashMap<String, BigDecimal>();
    BigDecimal currentPaymentTotal = BigDecimal.ZERO;
    Date expectedDate = null;
    Currency faCurrency = rem.getFinancialAccount().getCurrency();
    BigDecimal txnConvAmount = currentPaymentTotal;
    int size = lines.size();
    for (int i = 0; i < size; i++) {
      rem = OBDal.getInstance().get(Remittance.class, strRemittanceId);
      RemittanceLine remLine = OBDal.getInstance()
          .get(RemittanceLine.class, lines.get(i).toString());
      Currency nextCurrency = null;
      BusinessPartner nextbp = null;
      Invoice nextInvoice = null;
      Date nextExpectedDate = null;
      String nextIsReceipt = null;
      Currency currentCurrency = remLine.getPaymentScheduleDetail()
          .getInvoicePaymentSchedule() != null
              ? remLine.getPaymentScheduleDetail()
                  .getInvoicePaymentSchedule()
                  .getInvoice()
                  .getCurrency()
              : remLine.getPaymentScheduleDetail()
                  .getOrderPaymentSchedule()
                  .getOrder()
                  .getCurrency();
      BusinessPartner currentbp = remLine.getPaymentScheduleDetail()
          .getInvoicePaymentSchedule() != null
              ? remLine.getPaymentScheduleDetail()
                  .getInvoicePaymentSchedule()
                  .getInvoice()
                  .getBusinessPartner()
              : remLine.getPaymentScheduleDetail()
                  .getOrderPaymentSchedule()
                  .getOrder()
                  .getBusinessPartner();
      Invoice currentInvoice = remLine.getPaymentScheduleDetail()
          .getInvoicePaymentSchedule() != null
              ? remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule().getInvoice()
              : null;
      Date currentExpectedDate = remLine.getPaymentScheduleDetail()
          .getInvoicePaymentSchedule() != null
              ? remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule().getExpectedDate()
              : remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getExpectedDate();
      String currentIsReceipt = remLine.getPaymentScheduleDetail()
          .getInvoicePaymentSchedule() != null
              ? remLine.getPaymentScheduleDetail()
                  .getInvoicePaymentSchedule()
                  .getInvoice()
                  .isSalesTransaction()
                  .toString()
              : remLine.getPaymentScheduleDetail()
                  .getOrderPaymentSchedule()
                  .getOrder()
                  .isSalesTransaction()
                  .toString();
      Date lineExpectedDate = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
          ? remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule().getExpectedDate()
          : remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getExpectedDate();
      if (expectedDate == null || lineExpectedDate.compareTo(expectedDate) > 0 || isDateGroup) {
        expectedDate = lineExpectedDate;
      }
      if ((i + 1) < size) { // Is not the last Item
        RemittanceLine nextline = OBDal.getInstance()
            .get(RemittanceLine.class, lines.get(i + 1).toString());
        nextCurrency = nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? nextline.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .getCurrency()
            : nextline.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .getCurrency();
        nextbp = nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? nextline.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .getBusinessPartner()
            : nextline.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .getBusinessPartner();
        nextInvoice = nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule().getInvoice()
            : null;
        nextExpectedDate = nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule().getExpectedDate()
            : nextline.getPaymentScheduleDetail().getOrderPaymentSchedule().getExpectedDate();
        nextIsReceipt = nextline.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? nextline.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .isSalesTransaction()
                .toString()
            : nextline.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .isSalesTransaction()
                .toString();
      }

      hm.put(remLine.getPaymentScheduleDetail().getId(), remLine.getAmount());
      // Payment Schedule Detail already linked to a payment detail.
      if (remLine.getPaymentScheduleDetail().getPaymentDetails() != null) {
        String exceptionMessage = "The following line (" + remLine.getIdentifier()
            + ") cannot be included in the remittance because is already paid in other payment "
            + remLine.getPaymentScheduleDetail().getIdentifier();
        throw new OBException(exceptionMessage, false);
      }
      psdList.add(remLine.getPaymentScheduleDetail());
      remLines.add(remLine);
      // Totals are calculated separately for sales / purchase flows
      // Always sum amounts
      currentPaymentTotal = currentPaymentTotal.add(remLine.getAmount());
      // If the currency of remittance lines is different from the currency of the financial account
      // of the remittance header, calculate the conversion rate and the converted amount
      if (currentCurrency != null && !faCurrency.equals(currentCurrency)) {
        BigDecimal transactionConverted = new BigDecimal(0);
        if (remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            && remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice() != null) {
          Invoice invoice = remLine.getPaymentScheduleDetail()
              .getInvoicePaymentSchedule()
              .getInvoice();
          if (invoice.getCurrencyConversionRateDocList() != null
              && !invoice.getCurrencyConversionRateDocList().isEmpty()) {
            transactionConverted = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                invoice.getCurrency(), faCurrency, invoice.getInvoiceDate(),
                invoice.getOrganization(), FinancialUtils.PRECISION_PRICE,
                invoice.getCurrencyConversionRateDocList());
          } else {
            transactionConverted = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                invoice.getCurrency(), faCurrency, invoice.getInvoiceDate(),
                invoice.getOrganization(), FinancialUtils.PRECISION_PRICE);
          }
        } else {
          if (remLine.getPaymentScheduleDetail().getOrderPaymentSchedule() != null
              && remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder() != null) {
            Order order = remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder();
            transactionConverted = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                order.getCurrency(), faCurrency, order.getOrderDate(), order.getOrganization(),
                FinancialUtils.PRECISION_PRICE);
          }

        }
        txnConvAmount = txnConvAmount.add(transactionConverted);
      }

      // Process the payment if is the last item of the list or the business partner or currency has
      // changed
      if ((nextCurrency == null || !currentCurrency.getId().equals(nextCurrency.getId())
          || !currentIsReceipt.equals(nextIsReceipt))
          || (nextbp == null || !currentbp.getId().equals(nextbp.getId())
              || !currentIsReceipt.equals(nextIsReceipt))
          || (isDateGroup
              && (nextExpectedDate == null || currentExpectedDate.compareTo(nextExpectedDate) != 0))
          || (isInvoiceGroup && (nextInvoice == null || currentInvoice == null
              || !currentInvoice.getId().equals(nextInvoice.getId())))) {
        FIN_Payment payment = null;
        if (!rem.isSinglePayment()) {
          boolean isReceipt = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
              ? remLine.getPaymentScheduleDetail()
                  .getInvoicePaymentSchedule()
                  .getInvoice()
                  .isSalesTransaction()
              : remLine.getPaymentScheduleDetail()
                  .getOrderPaymentSchedule()
                  .getOrder()
                  .isSalesTransaction();

          DocumentType docType = FIN_Utility.getDocumentType(remLine.getOrganization(),
              isReceipt ? "ARR" : "APP");
          // get DocumentNo
          String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
              docType.getTable() != null ? docType.getTable().getDBTableName() : "");
          if (expectedDate.compareTo(rem.getTransactionDate()) < 0) {
            expectedDate = rem.getTransactionDate();
          }
          if (currentCurrency != null) {
            BigDecimal txnConvRate = new BigDecimal(1);
            if (!faCurrency.equals(currentCurrency)) {
              if (currentPaymentTotal.compareTo(BigDecimal.ZERO) != 0) {
                txnConvRate = txnConvAmount.divide(currentPaymentTotal, 6, RoundingMode.HALF_UP);
              }
            }
            payment = FIN_AddPayment.savePayment(null, isReceipt, docType, strPaymentDocumentNo,
                currentbp, pm, rem.getFinancialAccount(), currentPaymentTotal.toString(),
                expectedDate, remLine.getOrganization(), "", psdList, hm, false, false,
                currentCurrency, txnConvRate, txnConvAmount, false, null);
          } else {
            payment = FIN_AddPayment.savePayment(null, isReceipt, docType, strPaymentDocumentNo,
                currentbp, pm, rem.getFinancialAccount(), currentPaymentTotal.toString(),
                expectedDate, remLine.getOrganization(), "", psdList, hm, false, false, false);
          }
          OBDal.getInstance().save(payment);
          OBDal.getInstance().flush();
          ConnectionProvider conn = new DalConnectionProvider();
          OBError err = FIN_AddPayment.processPayment(
              new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
                  OBContext.getOBContext().getCurrentClient().getId(),
                  OBContext.getOBContext().getCurrentOrganization().getId(),
                  OBContext.getOBContext().getRole().getId()),
              conn, isReceipt ? (!pm.isAutomaticDeposit() ? "P" : "D")
                  : (!pm.isAutomaticWithdrawn() ? "P" : "D"),
              payment);
          if ("Error".equals(err.getType())) {
            throw new OBException(err.getMessage(), false);
          }

          // Update payment for grouped lines
          for (RemittanceLine rl : remLines) {
            rl.setPayment(payment);
            OBDal.getInstance().save(rl);
          }

          OBDal.getInstance().save(payment);
          OBDal.getInstance().flush();
        }
        Instruction instruction = addRemittanceBankInstruction(rem, psdList, hm, expectedDate);
        instruction.setPayment(payment);
        OBDal.getInstance().save(instruction);
        OBDal.getInstance().flush();
        // Clear data structures, new business partner starts
        psdList = new ArrayList<FIN_PaymentScheduleDetail>();
        OBDal.getInstance().getSession().clear();
        hm = new HashMap<String, BigDecimal>();
        remLines = new ArrayList<RemittanceLine>();
        currentPaymentTotal = BigDecimal.ZERO;
        txnConvAmount = currentPaymentTotal;
        expectedDate = null;
      }

    }
    rem = OBDal.getInstance().get(Remittance.class, strRemittanceId);
    if (rem.isSinglePayment()) {
      createSinglePayment(rem);
    }
    // Update payment status to sent via remittance
    Set<FIN_Payment> payments = new HashSet<FIN_Payment>();
    for (RemittanceLine remLine : rem.getREMRemittanceLineList()) {
      FIN_Payment payment = remLine.getPayment();
      if (payments.contains(payment)) {
        continue;
      } else {
        payments.add(payment);
      }

      payment.setStatus(STATUS_SENT);
      // Associate to remittance payment method & financial account
      associatePaymentWithRemittancePaymentMethod(payment, rem);
    }
  }

  private void createPayments(Remittance remittance, ProcessBundle bundle) throws Exception {
    FIN_PaymentMethod pm = remittance.getRemittanceType().getPaymentMethod();
    Currency faCurrency = remittance.getFinancialAccount().getCurrency();
    final ProcessBundle pb = new ProcessBundle("6255BE488882480599C81284B70CD9B3",
        bundle.getContext().toVars()).init(bundle.getConnection());
    int i = 0;

    for (String lineId : getRemittanceLines(remittance.getId())) {
      RemittanceLine remLine = OBDal.getInstance().get(RemittanceLine.class, lineId);

      if (remLine.getPayment() != null) {
        FIN_Payment payment = remLine.getPayment();
        HashMap<String, String> remHash = REM_AddRemittance.existLineInSettledRemittance(payment);
        if (!remHash.isEmpty()) {
          throw new OBException(
              String.format(FIN_Utility.messageBD("REM_JustOnceInRemittanceLines"),
                  remHash.get("documentno"), remHash.get("lineno")),
              false);
        }
        payment.setStatus(STATUS_SENT);
        // Associate to remittance payment method & financial account
        if (!STATUS_RECEIVED.equals(remLine.getOrigstatus())) {
          associatePaymentWithRemittancePaymentMethod(payment, remittance);
        }

        Instruction instruction = addRemittanceBankInstruction(remittance, payment);
        instruction.setPayment(payment);
        OBDal.getInstance().save(instruction);
        continue;
      }
      Instruction instruction = addRemittanceBankInstruction(remittance,
          remLine.getPaymentScheduleDetail(), remLine.getAmount());

      if (!remittance.isSinglePayment()) {
        boolean isReceipt = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .isSalesTransaction()
            : remLine.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .isSalesTransaction();
        Date expectedDate = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule().getExpectedDate()
            : remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getExpectedDate();
        DocumentType docType = FIN_Utility.getDocumentType(remLine.getOrganization(),
            isReceipt ? "ARR" : "APP");
        // get DocumentNo
        String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
            docType.getTable() != null ? docType.getTable().getDBTableName() : "");
        HashMap<String, BigDecimal> hm = new HashMap<String, BigDecimal>();
        List<FIN_PaymentScheduleDetail> psdList = new ArrayList<FIN_PaymentScheduleDetail>();
        hm.put(remLine.getPaymentScheduleDetail().getId(), remLine.getAmount());
        // Payment Schedule Detail already linked to a payment detail.
        if (remLine.getPaymentScheduleDetail().getPaymentDetails() != null) {
          String exceptionMessage = "The following line (" + remLine.getIdentifier()
              + ") cannot be included in the remittance because is already paid in other payment: "
              + remLine.getPaymentScheduleDetail().getIdentifier();
          throw new OBException(exceptionMessage, false);
        }
        psdList.add(remLine.getPaymentScheduleDetail());
        Currency currency = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .getCurrency()
            : remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder().getCurrency();
        BusinessPartner bp = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .getBusinessPartner()
            : remLine.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .getBusinessPartner();
        if (expectedDate.compareTo(remittance.getTransactionDate()) < 0) {
          expectedDate = remittance.getTransactionDate();
        }
        FIN_Payment payment = null;
        if (currency != null && !faCurrency.equals(currency)) {
          BigDecimal txnConvRate = new BigDecimal(1);
          BigDecimal txnConvAmount = new BigDecimal(0);
          if (remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
              && remLine.getPaymentScheduleDetail()
                  .getInvoicePaymentSchedule()
                  .getInvoice() != null) {
            Invoice invoice = remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice();
            if (invoice.getCurrencyConversionRateDocList() != null
                && !invoice.getCurrencyConversionRateDocList().isEmpty()) {
              txnConvAmount = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                  invoice.getCurrency(), faCurrency, invoice.getInvoiceDate(),
                  invoice.getOrganization(), FinancialUtils.PRECISION_PRICE,
                  invoice.getCurrencyConversionRateDocList());
            } else {
              txnConvAmount = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                  invoice.getCurrency(), faCurrency, invoice.getInvoiceDate(),
                  invoice.getOrganization(), FinancialUtils.PRECISION_PRICE);
            }
          } else {
            if (remLine.getPaymentScheduleDetail().getOrderPaymentSchedule() != null
                && remLine.getPaymentScheduleDetail()
                    .getOrderPaymentSchedule()
                    .getOrder() != null) {
              Order order = remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder();
              txnConvAmount = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                  order.getCurrency(), faCurrency, remittance.getTransactionDate(),
                  order.getOrganization(), FinancialUtils.PRECISION_PRICE);
            }
          }
          if (!remLine.getAmount().equals(new BigDecimal(0))) {
            txnConvRate = txnConvAmount.divide(remLine.getAmount(), 6, RoundingMode.HALF_UP);
          }

          payment = FIN_AddPayment.savePayment(null, isReceipt, docType, strPaymentDocumentNo, bp,
              pm, remittance.getFinancialAccount(), remLine.getAmount().toString(), expectedDate,
              remLine.getOrganization(), "", psdList, hm, false, false, currency, txnConvRate,
              txnConvAmount, false, null);
        } else {
          payment = FIN_AddPayment.savePayment(null, isReceipt, docType, strPaymentDocumentNo, bp,
              pm, remittance.getFinancialAccount(), remLine.getAmount().toString(), expectedDate,
              remLine.getOrganization(), "", psdList, hm, false, false, false);
        }
        OBDal.getInstance().save(payment);
        instruction.setPayment(payment);
        OBDal.getInstance().save(instruction);
        OBDal.getInstance().flush();

        OBError err = FIN_AddPayment.processPayment(pb,
            isReceipt ? (!pm.isAutomaticDeposit() ? "P" : "D")
                : (!pm.isAutomaticWithdrawn() ? "P" : "D"),
            payment, null, null, false);
        if ("Error".equals(err.getType())) {
          throw new OBException(err.getMessage(), false);
        }

        payment.setStatus(STATUS_SENT);
        remLine.setPayment(payment);

        i++;
        if (i % 100 == 0) {
          OBDal.getInstance().flush();
        }
      }

    }
    // Flush last lines
    OBDal.getInstance().flush();
    if (remittance.isSinglePayment()) {
      createSinglePayment(remittance);
    }
  }

  private FIN_Payment createConsolidatedPayment(Remittance remittance) throws Exception {
    BigDecimal amount = BigDecimal.ZERO;
    List<RemittanceLine> lines = remittance.getREMRemittanceLineList();
    Set<FIN_Payment> processedPayments = new HashSet<FIN_Payment>();
    for (RemittanceLine remLine : lines) {
      // Do proper currency conversions for the consolidated payment
      Currency faCurrency = remittance.getFinancialAccount().getCurrency();
      if (!processedPayments.contains(remLine.getPayment())) {
        BigDecimal lineAmount = remLine.getPayment().isReceipt() ? remLine.getPayment().getAmount()
            : remLine.getPayment().getAmount().negate();
        if (faCurrency != null && remLine.getPayment().getCurrency() != null
            && !remLine.getPayment().getCurrency().equals(faCurrency)) {
          lineAmount = FinancialUtils.getConvertedAmount(lineAmount,
              remLine.getPayment().getCurrency(), faCurrency, remittance.getChargedate(),
              remLine.getPayment().getOrganization(), FinancialUtils.PRECISION_PRICE);
        }
        amount = amount.add(lineAmount);
        processedPayments.add(remLine.getPayment());
      }
    }
    if (amount.signum() == 0) {
      return null;
    }
    boolean isReceipt = amount.signum() > 0;
    DocumentType docType = FIN_Utility.getDocumentType(remittance.getOrganization(),
        isReceipt ? "ARR" : "APP");
    // get DocumentNo
    String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
        docType.getTable() != null ? docType.getTable().getDBTableName() : "");
    FIN_Payment payment = dao.getNewPayment(isReceipt, remittance.getOrganization(), docType,
        strPaymentDocumentNo, remittance.getFinancialAccount().getBusinessPartner(),
        remittance.getRemittanceType().getPaymentMethodForDiscount(),
        remittance.getFinancialAccount(), amount.abs().toString(), remittance.getChargedate(), "");
    payment.setDescription("");
    OBDal.getInstance().save(payment);
    OBDal.getInstance().flush();
    // add GL Item as payment detail
    FIN_AddPayment.saveGLItem(payment, amount.abs(), remittance.getRemittanceType().getGLItem());
    ConnectionProvider conn = new DalConnectionProvider();
    OBError err = FIN_AddPayment.processPayment(
        new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
            OBContext.getOBContext().getCurrentClient().getId(),
            OBContext.getOBContext().getCurrentOrganization().getId(),
            OBContext.getOBContext().getRole().getId()),
        conn,
        isReceipt
            ? (!remittance.getRemittanceType().getPaymentMethodForDiscount().isAutomaticDeposit()
                ? "P"
                : "D")
            : (!remittance.getRemittanceType().getPaymentMethodForDiscount().isAutomaticWithdrawn()
                ? "P"
                : "D"),
        payment);
    if ("Error".equals(err.getType())) {
      throw new OBException(err.getMessage(), false);
    }
    return payment;
  }

  private FIN_Payment createSinglePayment(Remittance remittance) throws Exception {
    List<FIN_PaymentScheduleDetail> psdList = new ArrayList<FIN_PaymentScheduleDetail>();
    HashMap<String, BigDecimal> hm = new HashMap<String, BigDecimal>();
    BigDecimal amount = BigDecimal.ZERO;
    Set<String> lineIds = new HashSet<String>();
    boolean isReceipt = true;
    int i = 0;
    Currency currency = null;
    Currency faCurrency = remittance.getFinancialAccount().getCurrency();
    BigDecimal txnConvAmount = amount;
    for (RemittanceLine remLine : remittance.getREMRemittanceLineList()) {
      if (remLine.getPayment() != null) {
        continue;
      }
      if (i == 0) {
        isReceipt = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .isSalesTransaction()
            : remLine.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .isSalesTransaction();
        i++;
      } else {
        boolean lineIsReceipt = remLine.getPaymentScheduleDetail()
            .getInvoicePaymentSchedule() != null
                ? remLine.getPaymentScheduleDetail()
                    .getInvoicePaymentSchedule()
                    .getInvoice()
                    .isSalesTransaction()
                : remLine.getPaymentScheduleDetail()
                    .getOrderPaymentSchedule()
                    .getOrder()
                    .isSalesTransaction();
        if (isReceipt != lineIsReceipt) {
          throw new OBException(FIN_Utility.messageBD("@REM_SinglePaymentCanNotMix@"), false);
        }
      }
      if (currency != null) {
        Currency lineCurrency = remLine.getPaymentScheduleDetail()
            .getInvoicePaymentSchedule() != null
                ? remLine.getPaymentScheduleDetail()
                    .getInvoicePaymentSchedule()
                    .getInvoice()
                    .getCurrency()
                : remLine.getPaymentScheduleDetail()
                    .getOrderPaymentSchedule()
                    .getOrder()
                    .getCurrency();
        if (!currency.equals(lineCurrency)) {
          throw new OBException(FIN_Utility.messageBD("@REM_SinglePaymentCanNotMixCurrencies@"),
              false);
        }
      } else {
        currency = remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            ? remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .getCurrency()
            : remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder().getCurrency();
      }
      lineIds.add(remLine.getId());
      // Payment Schedule Detail already linked to a payment detail.
      if (remLine.getPaymentScheduleDetail().getPaymentDetails() != null) {
        String exceptionMessage = "The following line (" + remLine.getIdentifier()
            + ") cannot be included in the remittance because is already paid in other payment: "
            + remLine.getPaymentScheduleDetail().getIdentifier();
        throw new OBException(exceptionMessage, false);
      }
      psdList.add(remLine.getPaymentScheduleDetail());
      hm.put(remLine.getPaymentScheduleDetail().getId(), remLine.getAmount());
      amount = amount.add(remLine.getAmount());
      // If the currency of remittance lines is different from the currency of the financial account
      // of the remittance header, calculate the conversion rate and the converted amount
      if (currency != null && !faCurrency.equals(currency)) {
        BigDecimal transactionConverted = new BigDecimal(0);
        if (remLine.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null
            && remLine.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice() != null) {
          Invoice invoice = remLine.getPaymentScheduleDetail()
              .getInvoicePaymentSchedule()
              .getInvoice();
          if (invoice.getCurrencyConversionRateDocList() != null
              && !invoice.getCurrencyConversionRateDocList().isEmpty()) {
            transactionConverted = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                invoice.getCurrency(), faCurrency, invoice.getInvoiceDate(),
                invoice.getOrganization(), FinancialUtils.PRECISION_PRICE,
                invoice.getCurrencyConversionRateDocList());
          } else {
            transactionConverted = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                invoice.getCurrency(), faCurrency, invoice.getInvoiceDate(),
                invoice.getOrganization(), FinancialUtils.PRECISION_PRICE);
          }
        } else {
          if (remLine.getPaymentScheduleDetail().getOrderPaymentSchedule() != null
              && remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder() != null) {
            Order order = remLine.getPaymentScheduleDetail().getOrderPaymentSchedule().getOrder();
            transactionConverted = FinancialUtils.getConvertedAmount(remLine.getAmount(),
                order.getCurrency(), faCurrency, order.getOrderDate(), order.getOrganization(),
                FinancialUtils.PRECISION_PRICE);
          }

        }
        txnConvAmount = txnConvAmount.add(transactionConverted);
      }
    }
    if (psdList.size() == 0) {
      throw new OBException("@REM_UnableToProcessOnlyPayments@", false);
    }
    DocumentType docType = FIN_Utility.getDocumentType(remittance.getOrganization(),
        isReceipt ? "ARR" : "APP");
    // get DocumentNo
    String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
        docType.getTable() != null ? docType.getTable().getDBTableName() : "");
    FIN_Payment payment = null;
    if (currency != null) {
      BigDecimal txnConvRate = new BigDecimal(1);
      if (!faCurrency.equals(currency)) {
        if (!amount.equals(new BigDecimal(0))) {
          txnConvRate = txnConvAmount.divide(amount, 6, RoundingMode.HALF_UP);
        }
      }
      payment = FIN_AddPayment.savePayment(null, isReceipt, docType, strPaymentDocumentNo, null,
          remittance.getRemittanceType().getPaymentMethod(), remittance.getFinancialAccount(),
          amount.toString(), remittance.getDueDate(), remittance.getOrganization(), "", psdList, hm,
          false, false, currency, txnConvRate, txnConvAmount, false, null);
    } else {
      payment = FIN_AddPayment.savePayment(null, isReceipt, docType, strPaymentDocumentNo, null,
          remittance.getRemittanceType().getPaymentMethod(), remittance.getFinancialAccount(),
          amount.toString(), remittance.getDueDate(), remittance.getOrganization(), "", psdList, hm,
          false, false, false);
    }
    OBDal.getInstance().save(payment);
    OBDal.getInstance().flush();
    ConnectionProvider conn = new DalConnectionProvider();
    OBError err = FIN_AddPayment.processPayment(
        new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
            OBContext.getOBContext().getCurrentClient().getId(),
            OBContext.getOBContext().getCurrentOrganization().getId(),
            OBContext.getOBContext().getRole().getId()),
        conn, !remittance.getRemittanceType().getPaymentMethod().isAutomaticWithdrawn() ? "P" : "D",
        payment);
    if ("Error".equals(err.getType())) {
      throw new OBException(err.getMessage(), false);
    }
    payment.setStatus(STATUS_SENT);
    OBDal.getInstance().save(payment);
    OBDal.getInstance().flush();
    for (Instruction instruction : remittance.getREMInstructionList()) {
      instruction.setPayment(payment);
      OBDal.getInstance().save(instruction);
    }
    for (String lineId : lineIds) {
      RemittanceLine remLine = OBDal.getInstance().get(RemittanceLine.class, lineId);
      remLine.setPayment(payment);
      OBDal.getInstance().save(remLine);
    }
    OBDal.getInstance().save(remittance);
    OBDal.getInstance().flush();
    return payment;
  }

  private Instruction addRemittanceBankInstruction(Remittance remittance, long lineNo,
      BusinessPartner bp, String accountNo, BigDecimal amount, String description,
      Date expectedDate, Date creditExecutionDate) {
    Instruction bankInstruction = addRemittanceBankInstruction(remittance, lineNo, bp, accountNo,
        amount, description, expectedDate, creditExecutionDate, null);
    return bankInstruction;
  }

  private Instruction addRemittanceBankInstruction(Remittance remittance, long lineNo,
      BusinessPartner bp, String accountNo, BigDecimal amount, String description,
      Date expectedDate, Date creditExecutionDate, FIN_Payment payment) {
    Instruction bankInstruction = OBProvider.getInstance().get(Instruction.class);
    bankInstruction.setOrganization(remittance.getOrganization());
    bankInstruction.setAccountNo(accountNo);
    bankInstruction.setAmount(amount);
    bankInstruction.setBusinessPartner(bp);
    String truncateDescription = "";
    if (description != null && description.length() > 0) {
      truncateDescription = (description.length() > 255)
          ? description.substring(0, 252).concat("...").toString()
          : description.toString();
    }
    Date dueDate = expectedDate.compareTo(remittance.getTransactionDate()) < 0
        ? remittance.getTransactionDate()
        : expectedDate;
    bankInstruction.setDescription(truncateDescription);
    bankInstruction.setDueDate(dueDate);
    bankInstruction.setLineNo(lineNo);
    bankInstruction.setNewOBObject(true);
    bankInstruction.setCreditExecutionDate(creditExecutionDate);
    bankInstruction.setRemittance(remittance);
    if (payment != null) {
      bankInstruction.setPayment(payment);
    }
    OBDal.getInstance().save(bankInstruction);
    return bankInstruction;
  }

  private Instruction addRemittanceBankInstruction(Remittance remittance,
      FIN_PaymentScheduleDetail psd, BigDecimal amount) {
    BusinessPartner bp = psd.getInvoicePaymentSchedule() != null
        ? psd.getInvoicePaymentSchedule().getInvoice().getBusinessPartner()
        : psd.getOrderPaymentSchedule().getOrder().getBusinessPartner();
    String invoiceDocNo = "";
    String orderDocNo = "";
    if (psd.getInvoicePaymentSchedule() != null) {
      invoiceDocNo = FIN_Utility.getDesiredDocumentNo(remittance.getOrganization(),
          psd.getInvoicePaymentSchedule().getInvoice());
    }
    if (psd.getOrderPaymentSchedule() != null) {
      orderDocNo = psd.getOrderPaymentSchedule().getOrder().getDocumentNo();
    }
    StringBuffer description = new StringBuffer();
    if (!StringUtils.isEmpty(invoiceDocNo) && !StringUtils.isEmpty(orderDocNo)) {
      description.append(FIN_Utility.messageBD("InvoiceDocumentno"));
      description.append(": ").append(invoiceDocNo).append(" ");
      description.append(FIN_Utility.messageBD("OrderDocumentno"));
      description.append(": ").append(orderDocNo);
      description.append("\n");
    } else if (StringUtils.isEmpty(invoiceDocNo) && !StringUtils.isEmpty(orderDocNo)) {
      description.append(FIN_Utility.messageBD("OrderDocumentno"));
      description.append(": ").append(orderDocNo);
      description.append("\n");
    } else if (!StringUtils.isEmpty(invoiceDocNo) && StringUtils.isEmpty(orderDocNo)) {
      description.append(FIN_Utility.messageBD("InvoiceDocumentno"));
      description.append(": ").append(invoiceDocNo);
      description.append("\n");
    }
    Date expectedDate = psd.getInvoicePaymentSchedule() != null
        ? psd.getInvoicePaymentSchedule().getExpectedDate()
        : psd.getOrderPaymentSchedule().getExpectedDate();
    if (expectedDate.compareTo(remittance.getTransactionDate()) < 0) {
      expectedDate = remittance.getTransactionDate();
    }
    List<BankAccount> bankAccounts = getActiveBAList(bp.getBusinessPartnerBankAccountList());
    return addRemittanceBankInstruction(remittance, getNextInstructionLineNo(remittance), bp,
        !bankAccounts.isEmpty() ? getAcountNumber(bankAccounts.get(0)) : "00000000000000000000",
        amount, description.toString(), expectedDate,
        psd.getInvoicePaymentSchedule() != null
            ? psd.getInvoicePaymentSchedule().getInvoice().getInvoiceDate()
            : psd.getOrderPaymentSchedule().getOrder().getOrderDate());
  }

  private Instruction addRemittanceBankInstruction(Remittance remittance, FIN_Payment payment) {
    List<BankAccount> bankAccounts = new ArrayList<BankAccount>();
    if (payment.getBusinessPartner() != null) {
      bankAccounts = getActiveBAList(
          payment.getBusinessPartner().getBusinessPartnerBankAccountList());
    } else {
      error.setType("Warning");
      error.setMessage(String.format(
          Utility.messageBD(new DalConnectionProvider(), "REM_MissingBPartner",
              OBContext.getOBContext().getLanguage().getLanguage()),
          String.valueOf(getNextInstructionLineNo(remittance))));
    }

    return addRemittanceBankInstruction(remittance, getNextInstructionLineNo(remittance),
        payment.getBusinessPartner(),
        payment.getBusinessPartner() != null
            ? (!bankAccounts.isEmpty() ? getAcountNumber(bankAccounts.get(0))
                : "00000000000000000000")
            : "00000000000000000000",
        payment.getAmount(), payment.getDescription(), payment.getPaymentDate(),
        getCreditExecutionDate(payment), payment);
  }

  private Instruction addRemittanceBankInstruction(Remittance remittance,
      List<FIN_PaymentScheduleDetail> psdlist, HashMap<String, BigDecimal> amounts,
      Date expectedDate) {
    BusinessPartner bp = psdlist.get(0).getInvoicePaymentSchedule() != null
        ? psdlist.get(0).getInvoicePaymentSchedule().getInvoice().getBusinessPartner()
        : psdlist.get(0).getOrderPaymentSchedule().getOrder().getBusinessPartner();
    BigDecimal instructionAmount = BigDecimal.ZERO;
    StringBuffer description = new StringBuffer();
    Set<String> invoiceDocNos = new HashSet<String>();
    Set<String> orderDocNos = new HashSet<String>();
    Date creditExecutionDate = null;
    for (FIN_PaymentScheduleDetail psd : psdlist) {
      instructionAmount = instructionAmount.add(amounts.get(psd.getId()));
      if (psd.getInvoicePaymentSchedule() != null) {
        invoiceDocNos.add(FIN_Utility.getDesiredDocumentNo(remittance.getOrganization(),
            psd.getInvoicePaymentSchedule().getInvoice()));
        if (creditExecutionDate == null) {
          creditExecutionDate = psd.getInvoicePaymentSchedule().getInvoice().getInvoiceDate();
        } else if (creditExecutionDate
            .after(psd.getInvoicePaymentSchedule().getInvoice().getInvoiceDate())) {
          creditExecutionDate = psd.getInvoicePaymentSchedule().getInvoice().getInvoiceDate();
        }
      }
      if (psd.getOrderPaymentSchedule() != null) {
        orderDocNos.add(psd.getOrderPaymentSchedule().getOrder().getDocumentNo());
        if (creditExecutionDate == null) {
          creditExecutionDate = psd.getOrderPaymentSchedule().getOrder().getOrderDate();
        } else if (creditExecutionDate
            .after(psd.getOrderPaymentSchedule().getOrder().getOrderDate())) {
          creditExecutionDate = psd.getOrderPaymentSchedule().getOrder().getOrderDate();
        }
      }
    }
    if (!invoiceDocNos.isEmpty()) {
      description.append(FIN_Utility.messageBD("InvoiceDocumentno"));
      description.append(": ")
          .append(invoiceDocNos.toString().substring(1, invoiceDocNos.toString().length() - 1));
    }
    if (!orderDocNos.isEmpty()) {
      if (!invoiceDocNos.isEmpty()) {
        description.append(" ");
      }
      description.append(FIN_Utility.messageBD("OrderDocumentno"));
      description.append(": ")
          .append(orderDocNos.toString().substring(1, orderDocNos.toString().length() - 1));
      description.append("\n");
    }

    List<BankAccount> bankAccounts = getActiveBAList(bp.getBusinessPartnerBankAccountList());
    return addRemittanceBankInstruction(remittance, getNextInstructionLineNo(remittance), bp,
        !bankAccounts.isEmpty() ? getAcountNumber(bankAccounts.get(0)) : "00000000000000000000",
        instructionAmount, description.toString(), expectedDate, creditExecutionDate);
  }

  private long getNextInstructionLineNo(Remittance remittance) {
    line = line + 10l;
    return line;
  }

  private List<String> getRemittanceLines(String paymentId, String remittanceId) {
    OBContext.setAdminMode();
    try {
      final StringBuilder whereClause = new StringBuilder();
      whereClause.append(" select rl." + RemittanceLine.PROPERTY_ID);
      whereClause.append(" from " + RemittanceLine.ENTITY_NAME + " as rl");
      whereClause.append(" where rl." + RemittanceLine.PROPERTY_PAYMENT + ".id = :paymentId");
      whereClause.append(" and rl." + RemittanceLine.PROPERTY_REMITTANCE + ".id = :remittanceId");
      whereClause.append(" and rl." + RemittanceLine.PROPERTY_ACTIVE + " = true");
      Query<String> query = OBDal.getInstance()
          .getSession()
          .createQuery(whereClause.toString(), String.class);
      query.setParameter("paymentId", paymentId);
      query.setParameter("remittanceId", remittanceId);
      return query.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("hiding")
  private boolean isWrongRemTypePayMethodConfig(Remittance remittance) {
    boolean in = false;
    boolean out = false;
    boolean showError = false;

    for (RemittanceLine line : remittance.getREMRemittanceLineList()) {
      if (in && out) {
        break;
      }

      if (line.getPaymentScheduleDetail() != null) {
        if (line.getPaymentScheduleDetail().getInvoicePaymentSchedule() != null) {
          if (!in) {
            in = line.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .isSalesTransaction();
          }
          if (!out) {
            out = !line.getPaymentScheduleDetail()
                .getInvoicePaymentSchedule()
                .getInvoice()
                .isSalesTransaction();
          }
        } else if (line.getPaymentScheduleDetail().getOrderPaymentSchedule() != null) {
          if (!in) {
            in = line.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .isSalesTransaction();
          }
          if (!out) {
            out = !line.getPaymentScheduleDetail()
                .getOrderPaymentSchedule()
                .getOrder()
                .isSalesTransaction();
          }
        }
      } else if (line.getPayment() != null) {
        if (!in) {
          in = line.getPayment().isReceipt();
        }
        if (!out) {
          out = !line.getPayment().isReceipt();
        }
      }
    }
    FinAccPaymentMethod fapm = null;
    OBContext.setAdminMode();
    try {
      OBCriteria<FinAccPaymentMethod> obc = OBDal.getInstance()
          .createCriteria(FinAccPaymentMethod.class);
      obc.setFilterOnReadableOrganization(false);
      obc.add(
          Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT, remittance.getFinancialAccount()));
      obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD,
          remittance.getRemittanceType().getPaymentMethod()));
      fapm = (FinAccPaymentMethod) obc.uniqueResult();
    } finally {
      OBContext.restorePreviousMode();
    }

    if (in) {
      showError = fapm == null || !fapm.isPayinAllow() || !"A".equals(fapm.getPayinExecutionType())
          || fapm.getPayinExecutionProcess() == null || !fapm.isPayinDeferred();
    }
    if (!showError && out) {
      showError = fapm == null || !fapm.isPayoutAllow()
          || !"A".equals(fapm.getPayoutExecutionType()) || fapm.getPayoutExecutionProcess() == null
          || !fapm.isPayoutDeferred();
    }

    return showError;
  }

  private List<BankAccount> getActiveBAList(List<BankAccount> totalList) {
    List<BankAccount> activeList = new ArrayList<BankAccount>();
    for (BankAccount item : totalList) {
      if (item.isActive()) {
        activeList.add(item);
      }
    }
    return activeList;
  }

  private Date getCreditExecutionDate(FIN_Payment payment) {
    Date earliestDate = null;
    Date auxDate = null;
    final OBCriteria<FIN_PaymentScheduleDetail> obc = OBDal.getInstance()
        .createCriteria(FIN_PaymentScheduleDetail.class);
    obc.add(Restrictions.in(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS,
        payment.getFINPaymentDetailList()));
    for (FIN_PaymentScheduleDetail fpsd : obc.list()) {
      if (fpsd.getOrderPaymentSchedule() != null) {
        auxDate = fpsd.getOrderPaymentSchedule().getOrder().getOrderDate();
        if (earliestDate == null) {
          earliestDate = auxDate;
        } else if (earliestDate.after(earliestDate)) {
          earliestDate = auxDate;
        }
      } else if (fpsd.getInvoicePaymentSchedule() != null) {
        auxDate = fpsd.getInvoicePaymentSchedule().getInvoice().getInvoiceDate();
        if (earliestDate == null) {
          earliestDate = auxDate;
        } else if (earliestDate.after(earliestDate)) {
          earliestDate = auxDate;
        }
      } else {
        auxDate = payment.getPaymentDate();
      }
    }
    return auxDate;
  }

  private String getAcountNumber(BankAccount bankAccount) {
    if (bankAccount.getAccountNo() != null) {
      // If there is an Account Number, return it
      return bankAccount.getAccountNo();
    } else if ("IBAN".equals(bankAccount.getBankFormat())) {
      // If the Account is defined to use IBAN as it's identifier, return it
      return bankAccount.getIBAN();
    } else {
      // Else return 00000000000000000000
      return "00000000000000000000";
    }
  }

  private List<String> getRemittanceLines(String remittanceId) {
    OBContext.setAdminMode();
    try {
      final StringBuilder whereClause = new StringBuilder();
      whereClause.append(" select rl." + RemittanceLine.PROPERTY_ID);
      whereClause.append(" from " + RemittanceLine.ENTITY_NAME + " as rl");
      whereClause.append(" where rl." + RemittanceLine.PROPERTY_REMITTANCE + ".id = :remittanceId");
      whereClause.append(" and rl." + RemittanceLine.PROPERTY_ACTIVE + " = true");
      Query<String> query = OBDal.getInstance()
          .getSession()
          .createQuery(whereClause.toString(), String.class);
      query.setParameter("remittanceId", remittanceId);
      return query.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void associatePaymentWithRemittancePaymentMethod(FIN_Payment payment, Remittance rem) {
    payment.setPaymentMethod(rem.getRemittanceType().getPaymentMethod());
    payment.setAccount(rem.getFinancialAccount());
    OBDal.getInstance().save(payment);
  }
}
