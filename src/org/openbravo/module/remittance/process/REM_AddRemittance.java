/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.process;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletException;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentPropDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.PaymentPriority;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;
import org.openbravo.module.remittance.RemittanceLineCancel;
import org.openbravo.module.remittance.RemittanceType;

public class REM_AddRemittance {

  public static Remittance getRemittance(FIN_Payment payment) {
    OBContext.setAdminMode();
    try {
      OBCriteria<Remittance> rem = OBDal.getInstance().createCriteria(Remittance.class);
      rem.createAlias(Remittance.PROPERTY_REMITTANCETYPE, "rt");
      rem.add(Restrictions.eq("rt." + RemittanceType.PROPERTY_PAYMENTMETHOD,
          payment.getPaymentMethod()));
      rem.add(Restrictions.eq(Remittance.PROPERTY_FINANCIALACCOUNT, payment.getAccount()));
      rem.add(Restrictions.eq(Remittance.PROPERTY_PROCESSED, false));
      rem.add(Restrictions.in("organization.id", OBContext.getOBContext()
          .getOrganizationStructureProvider()
          .getParentTree(payment.getOrganization().getId(), true)));
      List<Remittance> openRemittances = rem.list();
      if (openRemittances.size() > 0) {
        return openRemittances.get(0);
      } else
        return newRemmitance(payment);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private static Remittance newRemmitance(FIN_Payment payment) {
    RemittanceType remittanceType = getRemittanceType(payment);
    if (remittanceType == null) {
      throw new OBException(String.format(FIN_Utility.messageBD("REM_RemittanceTypeNotExist"),
          payment.getPaymentMethod().getIdentifier()));
    }
    Remittance remittance = OBProvider.getInstance().get(Remittance.class);
    remittance.setOrganization(payment.getOrganization());
    remittance.setClient(payment.getClient());
    DocumentType docType = FIN_Utility.getDocumentType(payment.getOrganization(), "REM_REM");
    remittance.setDocumentType(docType);
    remittance.setDocumentNo(FIN_Utility.getDocumentNo(docType, "DocumentNo_REM_Remittance"));
    remittance.setDueDate(payment.getPaymentDate());
    remittance.setTransactionDate(payment.getPaymentDate());
    remittance.setName(payment.getAccount().getName());
    remittance.setFinancialAccount(payment.getAccount());
    remittance.setRemittanceType(remittanceType);
    OBDal.getInstance().save(remittance);
    OBDal.getInstance().flush();
    return remittance;
  }

  public static RemittanceLine newRemittanceLine(Remittance remittance, FIN_Payment payment) {
    RemittanceLine remittanceLine = OBProvider.getInstance().get(RemittanceLine.class);
    remittanceLine.setOrganization(payment.getOrganization());
    remittanceLine.setClient(payment.getClient());
    remittanceLine.setPayment(payment);
    remittanceLine.setRemittance(remittance);
    remittanceLine.setLineNo(getLineNo(remittance));
    remittanceLine.setAmount(payment.getAmount());
    OBDal.getInstance().save(remittanceLine);
    OBDal.getInstance().flush();
    return remittanceLine;
  }

  private static RemittanceType getRemittanceType(FIN_Payment payment) {
    OBContext.setAdminMode();
    RemittanceType remittanceType = null;
    try {
      OBCriteria<RemittanceType> rt = OBDal.getInstance().createCriteria(RemittanceType.class);
      rt.add(Restrictions.eq(RemittanceType.PROPERTY_PAYMENTMETHOD, payment.getPaymentMethod()));
      rt.add(Restrictions.in("organization.id", OBContext.getOBContext()
          .getOrganizationStructureProvider().getNaturalTree(payment.getOrganization().getId())));
      List<RemittanceType> remittanceTypes = rt.list();
      if (remittanceTypes.size() > 0) {
        remittanceType = remittanceTypes.get(0);
      } else
        return null;
    } finally {
      OBContext.restorePreviousMode();
    }
    return remittanceType;
  }

  private static long getLineNo(Remittance remittance) {
    OBContext.setAdminMode();
    try {
      OBCriteria<RemittanceLine> rl = OBDal.getInstance().createCriteria(RemittanceLine.class);
      rl.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE, remittance));
      rl.addOrder(Order.desc(RemittanceLine.PROPERTY_LINENO));
      rl.setMaxResults(1);
      List<RemittanceLine> remittanceLines = rl.list();
      if (remittanceLines.size() > 0) {
        return (remittanceLines.get(0).getLineNo() == null) ? 10l : remittanceLines.get(0)
            .getLineNo() + 10l;
      } else
        return 10l;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns a List of FIN_PaymentScheduleDetails related to the lines of the given Remittance.
   * 
   * @param remittance
   */
  public static List<FIN_PaymentScheduleDetail> getSelectedPendingPaymentsFromRemittance(
      Remittance remittance) {
    List<FIN_PaymentScheduleDetail> existingPaymentScheduleDetail = new ArrayList<FIN_PaymentScheduleDetail>();
    for (RemittanceLine line : remittance.getREMRemittanceLineList()) {
      if (line.getPaymentScheduleDetail() != null) {
        existingPaymentScheduleDetail.add(line.getPaymentScheduleDetail());
      }
    }

    return existingPaymentScheduleDetail;
  }

  /**
   * Builds a FieldProvider with a set of Payment Schedule Details based on the
   * selectedScheduledPaymentDetails and filteredScheduledPaymentDetails Lists. When the firstLoad
   * parameter is true the "paymentAmount" field is loaded from the corresponding Remittance Line if
   * it exists, when false it gets the amount from session.
   * 
   * @param vars
   *          VariablesSecureApp containing the Session data.
   * @param selectedScheduledPaymentDetails
   *          List of FIN_PaymentScheduleDetails that need to be selected by default.
   * @param filteredScheduledPaymentDetails
   *          List of FIN_PaymentScheduleDetails that need to be unselected by default.
   * @param firstLoad
   *          Boolean to set if the PaymentAmount is gotten from the Remittance (true) or from
   *          Session (false)
   * @param remittance
   *          Remittance used to get the amount when firstLoad is true.
   * @return a FieldProvider object with all the given FIN_PaymentScheduleDetails.
   */
  public static FieldProvider[] getShownScheduledPaymentDetails(VariablesSecureApp vars,
      String _strSelectedRecords, List<FIN_PaymentScheduleDetail> selectedScheduledPaymentDetails,
      List<FIN_PaymentScheduleDetail> filteredScheduledPaymentDetails, boolean firstLoad,
      Remittance remittance, String strDocumentType) throws ServletException {
    // Remove "(" ")"
    String strSelectedRecords = _strSelectedRecords;
    strSelectedRecords = strSelectedRecords.replace("(", "");
    strSelectedRecords = strSelectedRecords.replace(")", "");
    final List<FIN_PaymentScheduleDetail> shownScheduledPaymentDetails = new ArrayList<FIN_PaymentScheduleDetail>();
    shownScheduledPaymentDetails.addAll(selectedScheduledPaymentDetails);
    shownScheduledPaymentDetails.addAll(filteredScheduledPaymentDetails);
    FIN_PaymentScheduleDetail[] FIN_PaymentScheduleDetails = new FIN_PaymentScheduleDetail[0];
    FIN_PaymentScheduleDetails = shownScheduledPaymentDetails.toArray(FIN_PaymentScheduleDetails);
    // FieldProvider[] data = FieldProviderFactory.getFieldProviderArray(FIN_PaymentSchedules);

    // FieldProvider[] data = new FieldProviderFactory[selectedScheduledPayments.size()];
    FieldProvider[] data = FieldProviderFactory.getFieldProviderArray(shownScheduledPaymentDetails);
    String dateFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty("dateFormat.java");
    SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {

      for (int i = 0; i < data.length; i++) {
        String selectedId = (selectedScheduledPaymentDetails
            .contains(FIN_PaymentScheduleDetails[i])) ? FIN_PaymentScheduleDetails[i].getId() : "";
        // If selectedId belongs to a grouping selection calculate whether it should be selected or
        // not
        if (!"".equals(selectedId)) {
          StringTokenizer records = new StringTokenizer(strSelectedRecords, "'");
          Set<String> recordSet = new LinkedHashSet<String>();
          while (records.hasMoreTokens()) {
            recordSet.add(records.nextToken());
          }
          if (recordSet.contains(selectedId)) {
            FieldProviderFactory.setField(data[i], "finSelectedPaymentDetailId", selectedId);
          } else {
            String selectedRecord = FIN_PaymentScheduleDetails[i].getId();
            // Find record which contains psdId
            Set<String> psdIdSet = new LinkedHashSet<String>();
            for (String record : recordSet) {
              if (record.contains(selectedId)) {
                selectedRecord = record;
                StringTokenizer st = new StringTokenizer(record, ",");
                while (st.hasMoreTokens()) {
                  psdIdSet.add(st.nextToken());
                }
              }
            }
            String psdAmount = vars.getNumericParameter("inpPaymentAmount" + selectedRecord, "");
            if ("".equals(psdAmount)) {
              psdAmount = getRemittanceLineAmount(FIN_PaymentScheduleDetails[i], remittance);
            }
            if (!"".equals(psdAmount)) {
              HashMap<String, BigDecimal> idsAmounts = calculateAmounts(new BigDecimal(psdAmount),
                  psdIdSet);
              if (idsAmounts.get(selectedId).compareTo(BigDecimal.ZERO) != 0) {
                FieldProviderFactory.setField(data[i], "finSelectedPaymentDetailId", selectedId);
                FieldProviderFactory.setField(data[i], "paymentAmount", idsAmounts.get(selectedId)
                    .toString());
              }
            }
          }
        }
        FieldProviderFactory.setField(data[i], "finScheduledPaymentDetailId",
            FIN_PaymentScheduleDetails[i].getId());
        if (FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule() != null)
          FieldProviderFactory.setField(data[i], "orderPaymentScheduleId",
              FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule().getId());
        else
          FieldProviderFactory.setField(data[i], "orderPaymentScheduleId", "");
        if (FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule() != null)
          FieldProviderFactory.setField(data[i], "orderNr", FIN_PaymentScheduleDetails[i]
              .getOrderPaymentSchedule().getOrder().getDocumentNo());
        else
          FieldProviderFactory.setField(data[i], "orderNr", "");
        if (FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule() != null)
          FieldProviderFactory.setField(data[i], "invoiceNr", FIN_PaymentScheduleDetails[i]
              .getInvoicePaymentSchedule().getInvoice().getDocumentNo());
        else
          FieldProviderFactory.setField(data[i], "invoiceNr", "");
        if (FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule() != null)
          FieldProviderFactory.setField(data[i], "orderNrTrunc", FIN_PaymentScheduleDetails[i]
              .getOrderPaymentSchedule().getOrder().getDocumentNo());
        else
          FieldProviderFactory.setField(data[i], "orderNrTrunc", "");
        if (FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule() != null)
          FieldProviderFactory.setField(data[i], "invoiceNrTrunc", FIN_PaymentScheduleDetails[i]
              .getInvoicePaymentSchedule().getInvoice().getDocumentNo());
        else
          FieldProviderFactory.setField(data[i], "invoiceNrTrunc", "");
        if (FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule() != null)
          FieldProviderFactory.setField(data[i], "invoicePaymentScheduleId",
              FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule().getId());
        else
          FieldProviderFactory.setField(data[i], "invoicePaymentScheduleId", "");
        if (FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule() != null) {
          FieldProviderFactory.setField(
              data[i],
              "expectedDate",
              dateFormater.format(
                  FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule().getExpectedDate())
                  .toString());
          FieldProviderFactory.setField(
              data[i],
              "dueDate",
              dateFormater.format(
                  FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule().getDueDate())
                  .toString());
          FieldProviderFactory.setField(data[i], "invoicedAmount", FIN_PaymentScheduleDetails[i]
              .getInvoicePaymentSchedule().getInvoice().getGrandTotalAmount().toString());
          FieldProviderFactory.setField(data[i], "expectedAmount", FIN_PaymentScheduleDetails[i]
              .getInvoicePaymentSchedule().getAmount().toString());

          // Truncate Business Partner
          String businessPartner = FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule()
              .getInvoice().getBusinessPartner().getIdentifier();
          String truncateBusinessPartner = (businessPartner.length() > 18) ? businessPartner
              .substring(0, 15).concat("...").toString() : businessPartner;
          FieldProviderFactory.setField(data[i], "businessPartnerName",
              (businessPartner.length() > 18) ? businessPartner : "");
          FieldProviderFactory.setField(data[i], "businessPartnerNameTrunc",
              truncateBusinessPartner);

          // Truncate Currency
          String currency = FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule().getInvoice()
              .getCurrency().getIdentifier();
          String truncateCurrency = (currency.length() > 18) ? currency.substring(0, 15)
              .concat("...").toString() : currency;
          FieldProviderFactory.setField(data[i], "currencyName",
              (currency.length() > 18) ? currency : "");
          FieldProviderFactory.setField(data[i], "currencyNameTrunc", truncateCurrency);

          // Truncate Payment Method
          String paymentMethodName = FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule()
              .getFinPaymentmethod().getName();
          String truncatePaymentMethodName = (paymentMethodName.length() > 18) ? paymentMethodName
              .substring(0, 15).concat("...").toString() : paymentMethodName;
          FieldProviderFactory.setField(data[i], "paymentMethodName",
              (paymentMethodName.length() > 18) ? paymentMethodName : "");
          FieldProviderFactory.setField(data[i], "paymentMethodNameTrunc",
              truncatePaymentMethodName);

          if (FIN_PaymentScheduleDetails[i].getInvoicePaymentSchedule().getFINPaymentPriority() != null) {
            FieldProviderFactory.setField(data[i], "gridLineColor", FIN_PaymentScheduleDetails[i]
                .getInvoicePaymentSchedule().getFINPaymentPriority().getColor());
          }
        } else {
          FieldProviderFactory.setField(
              data[i],
              "expectedDate",
              dateFormater.format(
                  FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule().getExpectedDate())
                  .toString());
          FieldProviderFactory.setField(
              data[i],
              "dueDate",
              dateFormater.format(
                  FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule().getDueDate()).toString());
          FieldProviderFactory.setField(data[i], "invoicedAmount", "");
          FieldProviderFactory.setField(data[i], "expectedAmount", FIN_PaymentScheduleDetails[i]
              .getOrderPaymentSchedule().getAmount().toString());

          // Truncate Business Partner
          String businessPartner = FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule()
              .getOrder().getBusinessPartner().getIdentifier();
          String truncateBusinessPartner = (businessPartner.length() > 18) ? businessPartner
              .substring(0, 15).concat("...").toString() : businessPartner;
          FieldProviderFactory.setField(data[i], "businessPartnerName",
              (businessPartner.length() > 18) ? businessPartner : "");
          FieldProviderFactory.setField(data[i], "businessPartnerNameTrunc",
              truncateBusinessPartner);

          // Truncate Currency
          String currency = FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule().getOrder()
              .getCurrency().getIdentifier();
          String truncateCurrency = (currency.length() > 18) ? currency.substring(0, 15)
              .concat("...").toString() : currency;
          FieldProviderFactory.setField(data[i], "currencyName",
              (currency.length() > 18) ? currency : "");
          FieldProviderFactory.setField(data[i], "currencyNameTrunc", truncateCurrency);

          // Truncate Payment Method
          String paymentMethodName = FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule()
              .getFinPaymentmethod().getName();
          String truncatePaymentMethodName = (paymentMethodName.length() > 18) ? paymentMethodName
              .substring(0, 15).concat("...").toString() : paymentMethodName;
          FieldProviderFactory.setField(data[i], "paymentMethodName",
              (paymentMethodName.length() > 18) ? paymentMethodName : "");
          FieldProviderFactory.setField(data[i], "paymentMethodNameTrunc",
              truncatePaymentMethodName);

          if (FIN_PaymentScheduleDetails[i].getOrderPaymentSchedule().getFINPaymentPriority() != null) {
            FieldProviderFactory.setField(data[i], "gridLineColor", FIN_PaymentScheduleDetails[i]
                .getOrderPaymentSchedule().getFINPaymentPriority().getColor());
          }
        }
        FieldProviderFactory.setField(data[i], "outstandingAmount", FIN_PaymentScheduleDetails[i]
            .getAmount().toString());

        String strPaymentAmt = "";
        String strDifference = "";
        if (firstLoad && (selectedScheduledPaymentDetails.contains(FIN_PaymentScheduleDetails[i]))
            && remittance != null)
          strPaymentAmt = getRemittanceLineAmount(FIN_PaymentScheduleDetails[i], remittance);
        else
          strPaymentAmt = vars.getNumericParameter("inpPaymentAmount"
              + FIN_PaymentScheduleDetails[i].getId(), "");
        if (!"".equals(strPaymentAmt))
          strDifference = FIN_PaymentScheduleDetails[i].getAmount()
              .subtract(new BigDecimal(strPaymentAmt)).toString();
        if (data[i].getField("paymentAmount") == null
            || "".equals(data[i].getField("paymentAmount"))) {
          FieldProviderFactory.setField(data[i], "paymentAmount", strPaymentAmt);
        }
        FieldProviderFactory.setField(data[i], "difference", strDifference);
        FieldProviderFactory.setField(data[i], "rownum", String.valueOf(i));

      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return data;
  }

  private static String getRemittanceLineAmount(FIN_PaymentScheduleDetail finPaymentScheduleDetail,
      Remittance remittance) {
    String amount = "";
    for (RemittanceLine line : remittance.getREMRemittanceLineList())
      if (line.getPaymentScheduleDetail() == finPaymentScheduleDetail)
        amount = line.getAmount().toString();

    return amount;
  }

  /**
   * Adds new Details to the given Payment Proposal based on the List of Payment Schedule Details.
   * 
   * @param remittance
   *          FIN_PaymentProposal where new Details are added.
   * @param selectedPaymentScheduleDetails
   *          List of FIN_PaymentScheduleDetail that needs to be added to the Payment Proposal.
   * @param selectedPaymentScheduleDetailAmounts
   *          HashMap with the Amount to be paid for each Scheduled Payment Detail.
   * @param writeOffAmt
   *          Total amount to be written off.
   */
  public static void saveRemittance(Remittance remittance,
      List<FIN_PaymentScheduleDetail> selectedPaymentScheduleDetails,
      HashMap<String, BigDecimal> selectedPaymentScheduleDetailAmounts, BigDecimal writeOffAmt) {
    for (FIN_PaymentScheduleDetail paymentScheduleDetail : selectedPaymentScheduleDetails) {
      BigDecimal detailWriteOffAmt = BigDecimal.ZERO;
      // if (writeOffAmt != null)
      // detailWriteOffAmt = paymentScheduleDetail.getAmount().subtract(
      // selectedPaymentScheduleDetailAmounts.get(paymentScheduleDetail.getId()));
      BigDecimal amount = selectedPaymentScheduleDetailAmounts.get(paymentScheduleDetail.getId());

      if (amount.compareTo(BigDecimal.ZERO) != 0) {
        getNewRemittanceLine(remittance, paymentScheduleDetail,
            selectedPaymentScheduleDetailAmounts.get(paymentScheduleDetail.getId()),
            detailWriteOffAmt, null);
      }
    }
  }

  private static RemittanceLine getNewRemittanceLine(Remittance remittance,
      FIN_PaymentScheduleDetail paymentScheduleDetail, BigDecimal amount,
      BigDecimal writeoffamount, GLItem glitem) {
    final RemittanceLine newRemittanceLine = OBProvider.getInstance().get(RemittanceLine.class);
    newRemittanceLine.setOrganization(remittance.getOrganization());
    newRemittanceLine.setAmount(amount);
    newRemittanceLine.setLineNo(getLineNo(remittance));
    // Write off feature prepared but not found the need
    // if (writeoffamount != null)
    // newRemittanceLine.setWriteoffAmount(writeoffamount);
    newRemittanceLine.setPaymentScheduleDetail(paymentScheduleDetail);

    List<RemittanceLine> lines = remittance.getREMRemittanceLineList();
    lines.add(newRemittanceLine);
    remittance.setREMRemittanceLineList(lines);
    newRemittanceLine.setRemittance(remittance);

    OBDal.getInstance().save(newRemittanceLine);
    OBDal.getInstance().save(remittance);
    OBDal.getInstance().flush();

    return newRemittanceLine;
  }

  public static List<FIN_PaymentScheduleDetail> getFilteredScheduledPaymentDetails(
      Organization organization, Currency currency, Date dueDateFrom, Date dueDateTo,
      String strTransactionType, FIN_PaymentMethod paymentMethod, String isReceipt,
      String strBPartner, List<FIN_PaymentScheduleDetail> selectedScheduledPaymentDetails) {
    return getFilteredScheduledPaymentDetails(organization, null, false, dueDateFrom, dueDateTo,
        strTransactionType, paymentMethod, isReceipt, strBPartner, selectedScheduledPaymentDetails);
  }

  public static List<FIN_PaymentScheduleDetail> getFilteredScheduledPaymentDetails(
      Organization organization, Remittance remittance, boolean showAlternativeCurrency,
      Date dueDateFrom, Date dueDateTo, String strTransactionType, FIN_PaymentMethod paymentMethod,
      String isReceipt, String strBPartner,
      List<FIN_PaymentScheduleDetail> selectedScheduledPaymentDetails) {
    // If it does not allow IN/OUT just return empty list
    if ("".equals(isReceipt)) {
      return new ArrayList<FIN_PaymentScheduleDetail>();
    }
    final StringBuilder whereClause = new StringBuilder();
    final Map<String, Object> parameters = new HashMap<>();
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {

      whereClause.append(" as psd "); // pending scheduled payments //
      whereClause.append(" left outer join psd.orderPaymentSchedule as ops");
      whereClause.append(" left outer join ops.order as ord ");
      whereClause.append(" left outer join ord.businessPartner as obp ");
      whereClause.append(" left outer join ops.fINPaymentPriority as opriority ");
      whereClause.append(" left outer join psd.invoicePaymentSchedule ips ");
      whereClause.append(" left outer join ips.invoice as inv ");
      whereClause.append(" left outer join inv.businessPartner as ibp ");
      whereClause.append(" left outer join ips.fINPaymentPriority as ipriority ");
      whereClause.append(" where psd.");
      whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS);
      whereClause.append(" is null");
      whereClause.append(" and psd.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORGANIZATION);
      whereClause.append(".id in (");
      whereClause.append(Utility.getInStrSet(OBContext.getOBContext()
          .getOrganizationStructureProvider().getChildTree(organization.getId(), true)));
      whereClause.append(")");

      // remove selected payments
      if (selectedScheduledPaymentDetails != null && selectedScheduledPaymentDetails.size() > 0) {
        String strSelectedPaymentDetails = Utility.getInStrList(selectedScheduledPaymentDetails);
        whereClause.append(" and psd not in (");
        whereClause.append(strSelectedPaymentDetails);
        whereClause.append(")");
      }

      // block schedule payments in other payment proposal
      final OBCriteria<FIN_PaymentPropDetail> obc = OBDal.getInstance().createCriteria(
          FIN_PaymentPropDetail.class);
      obc.add(Restrictions.isNotNull(FIN_PaymentPropDetail.PROPERTY_FINPAYMENTSCHEDULEDETAIL));
      if (obc.list() != null && obc.list().size() > 0) {
        List<FIN_PaymentScheduleDetail> aux = new ArrayList<FIN_PaymentScheduleDetail>();
        for (FIN_PaymentPropDetail ppd : obc.list()) {
          aux.add(ppd.getFINPaymentScheduledetail());
        }
        whereClause.append(" and psd.id not in (" + Utility.getInStrList(aux) + ")");
      }

      // Transaction type filter
      whereClause.append(" and (");
      if (strTransactionType.equals("I") || strTransactionType.equals("B")) {
        whereClause.append(" (inv is not null");
        if (paymentMethod != null) {
          whereClause.append(" and ips.");
          whereClause.append(FIN_PaymentSchedule.PROPERTY_FINPAYMENTMETHOD);
          whereClause.append(".id = '");
          whereClause.append(paymentMethod.getId());
          whereClause.append("'");
        }
        if (remittance != null) {
          whereClause.append(" and (");
          whereClause.append("inv.");
          whereClause.append(Invoice.PROPERTY_CURRENCY);
          whereClause.append(".id = '");
          whereClause.append(remittance.getFinancialAccount().getCurrency().getId());
          whereClause.append("'");
          // Select invoices in multiple currencies
          if (showAlternativeCurrency) {
            whereClause.append(" or rem_showalternativecurrency('");
            whereClause.append(remittance.getId());
            whereClause.append("', null, inv.id, null)='Y'");
          }
          whereClause.append(")");
        }
        whereClause.append(")");
        // Business Partner
        if (!"".equals(strBPartner)) {
          whereClause.append(" and inv.");
          whereClause.append(Invoice.PROPERTY_BUSINESSPARTNER);
          whereClause.append(".id = '");
          whereClause.append(strBPartner);
          whereClause.append("'");
        }
        // IsReceipt
        if ("Y".equals(isReceipt) || "N".equals(isReceipt)) {
          whereClause.append(" and inv.");
          whereClause.append(Invoice.PROPERTY_SALESTRANSACTION);
          whereClause.append(" = ");
          whereClause.append("Y".equals(isReceipt));
        }
      }
      if (strTransactionType.equals("B"))
        whereClause.append(" or ");
      if (strTransactionType.equals("O") || strTransactionType.equals("B")) {
        whereClause.append(" (ord is not null");
        if (paymentMethod != null) {
          whereClause.append(" and ops.");
          whereClause.append(FIN_PaymentSchedule.PROPERTY_FINPAYMENTMETHOD);
          whereClause.append(".id = '");
          whereClause.append(paymentMethod.getId());
          whereClause.append("'");
        }
        if (remittance != null) {
          whereClause.append(" and (");
          whereClause.append("ord.");
          whereClause.append(org.openbravo.model.common.order.Order.PROPERTY_CURRENCY);
          whereClause.append(".id = '");
          whereClause.append(remittance.getFinancialAccount().getCurrency().getId());
          whereClause.append("'");
          // Select orders in multiple currencies
          if (showAlternativeCurrency) {
            whereClause.append(" or rem_showalternativecurrency('");
            whereClause.append(remittance.getId());
            whereClause.append("', null, null, ord.id)='Y'");
          }
          whereClause.append(")");
        }
        whereClause.append(")");
        // Business Partner
        if (!"".equals(strBPartner)) {
          whereClause.append(" and ord.");
          whereClause.append(Invoice.PROPERTY_BUSINESSPARTNER);
          whereClause.append(".id = '");
          whereClause.append(strBPartner);
          whereClause.append("'");
        }
        // IsReceipt
        if ("Y".equals(isReceipt) || "N".equals(isReceipt)) {
          whereClause.append(" and ord.");
          whereClause.append(org.openbravo.model.common.order.Order.PROPERTY_SALESTRANSACTION);
          whereClause.append(" = ");
          whereClause.append("Y".equals(isReceipt));
        }
      }
      whereClause.append(")");
      // dateFrom
      if (dueDateFrom != null) {
        whereClause.append(" and COALESCE(ips.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
        whereClause.append(", ops.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
        whereClause.append(") >= :dueDateFrom");
        parameters.put("dueDateFrom", dueDateFrom);
      }
      // dateTo
      if (dueDateTo != null) {
        whereClause.append(" and COALESCE(ips.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
        whereClause.append(", ops.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
        whereClause.append(") < :dueDateTo");
        parameters.put("dueDateTo", dueDateTo);
      }
      // TODO: Add order to show first scheduled payments from invoices and later scheduled payments
      // from not invoiced orders.
      whereClause.append(" order by");
      whereClause.append(" COALESCE(ipriority.");
      whereClause.append(PaymentPriority.PROPERTY_PRIORITY);
      whereClause.append(", opriority.");
      whereClause.append(PaymentPriority.PROPERTY_PRIORITY);
      whereClause.append(")");
      whereClause.append(", ");
      whereClause.append(" COALESCE(ips.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
      whereClause.append(", ops.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
      whereClause.append(")");
      whereClause.append(", COALESCE(");
      whereClause.append("ibp.").append(BusinessPartner.PROPERTY_NAME);
      whereClause.append(", obp.");
      whereClause.append(BusinessPartner.PROPERTY_NAME);
      whereClause.append(") desc");
      whereClause.append(", COALESCE(inv.");
      whereClause.append(Invoice.PROPERTY_DOCUMENTNO);
      whereClause.append(", ord.");
      whereClause.append(org.openbravo.model.common.order.Order.PROPERTY_DOCUMENTNO);
      whereClause.append(")");
      whereClause.append(", COALESCE(ord.");
      whereClause.append(org.openbravo.model.common.order.Order.PROPERTY_DOCUMENTNO);
      whereClause.append(", inv.");
      whereClause.append(Invoice.PROPERTY_DOCUMENTNO);
      whereClause.append(")");
      whereClause.append(", psd.");
      whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_AMOUNT);
      final OBQuery<FIN_PaymentScheduleDetail> obqPSD = OBDal.getInstance().createQuery(
          FIN_PaymentScheduleDetail.class, whereClause.toString());

      obqPSD.setNamedParameters(parameters);
      return obqPSD.list();

    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Creates a HashMap with the FIN_PaymentScheduleDetail id's and the amount gotten from the
   * Session.
   * 
   * The amounts are stored in Session like "inpPaymentAmount"+paymentScheduleDetail.Id
   * 
   * @param vars
   *          VariablseSecureApp with the session data.
   * @param _strSelectedScheduledPaymentDetailIds
   *          List of quoted id's of FIN_PaymentScheduleDetails that need to be included in the
   *          HashMap.
   * @return A HashMap mapping the FIN_PaymentScheduleDetail's Id with the corresponding amount.
   */
  public static HashMap<String, BigDecimal> getSelectedPaymentDetailsAndAmount(
      VariablesSecureApp vars, String _strSelectedScheduledPaymentDetailIds)
      throws ServletException {
    String strSelectedScheduledPaymentDetailIds = _strSelectedScheduledPaymentDetailIds;
    // Remove "(" ")"
    strSelectedScheduledPaymentDetailIds = strSelectedScheduledPaymentDetailIds.replace("(", "");
    strSelectedScheduledPaymentDetailIds = strSelectedScheduledPaymentDetailIds.replace(")", "");
    HashMap<String, BigDecimal> selectedPaymentScheduleDetailsAmounts = new HashMap<String, BigDecimal>();
    // As selected items may contain records with multiple IDs we as well need the records list as
    // amounts are related to records
    StringTokenizer records = new StringTokenizer(strSelectedScheduledPaymentDetailIds, "'");
    Set<String> recordSet = new LinkedHashSet<String>();
    while (records.hasMoreTokens()) {
      recordSet.add(records.nextToken());
    }
    for (String record : recordSet) {
      if (", ".equals(record)) {
        continue;
      }
      Set<String> psdSet = new LinkedHashSet<String>();
      StringTokenizer psds = new StringTokenizer(record, ",");
      while (psds.hasMoreTokens()) {
        psdSet.add(psds.nextToken());
      }
      BigDecimal recordAmount = new BigDecimal(vars.getNumericParameter(
          "inpPaymentAmount" + record, ""));
      HashMap<String, BigDecimal> recordsAmounts = calculateAmounts(recordAmount, psdSet);
      for (String psdId : psdSet) {
        selectedPaymentScheduleDetailsAmounts.put(psdId, recordsAmounts.get(psdId));
      }
    }
    return selectedPaymentScheduleDetailsAmounts;
  }

  private static HashMap<String, BigDecimal> calculateAmounts(BigDecimal recordAmount,
      Set<String> psdSet) {
    BigDecimal remainingAmount = recordAmount;
    HashMap<String, BigDecimal> recordsAmounts = new HashMap<String, BigDecimal>();
    // PSD needs to be properly ordered to ensure negative amounts are processed first
    List<FIN_PaymentScheduleDetail> psds = getOrderedPaymentScheduleDetails(psdSet);
    BigDecimal totalamount = BigDecimal.ZERO;
    for (FIN_PaymentScheduleDetail paymentScheduleDetail : psds) {
      totalamount = totalamount.add(paymentScheduleDetail.getAmount());
    }

    for (FIN_PaymentScheduleDetail paymentScheduleDetail : psds) {
      BigDecimal outstandingAmount = paymentScheduleDetail.getAmount();
      // Manage negative amounts
      if ((remainingAmount.compareTo(BigDecimal.ZERO) > 0 && remainingAmount
          .compareTo(outstandingAmount) >= 0)
          || ((remainingAmount.compareTo(BigDecimal.ZERO) == -1 && outstandingAmount
              .compareTo(BigDecimal.ZERO) == -1) && (remainingAmount.compareTo(outstandingAmount) <= 0))
          || (remainingAmount.compareTo(totalamount) == 0)) {
        recordsAmounts.put(paymentScheduleDetail.getId(), outstandingAmount);
        remainingAmount = remainingAmount.subtract(outstandingAmount);
      } else {
        recordsAmounts.put(paymentScheduleDetail.getId(), remainingAmount);
        if (psdSet.size() > 0) {
          remainingAmount = BigDecimal.ZERO;
        }
      }

    }
    return recordsAmounts;
  }

  private static List<FIN_PaymentScheduleDetail> getOrderedPaymentScheduleDetails(Set<String> psdSet) {
    OBCriteria<FIN_PaymentScheduleDetail> orderedPSDs = OBDal.getInstance().createCriteria(
        FIN_PaymentScheduleDetail.class);
    orderedPSDs.add(Restrictions.in(FIN_PaymentScheduleDetail.PROPERTY_ID, psdSet));
    orderedPSDs.addOrderBy(FIN_PaymentScheduleDetail.PROPERTY_AMOUNT, true);
    return orderedPSDs.list();
  }

  public static HashMap<String, String> existLineInAnyRemittance(FIN_Payment payment) {
    OBCriteria<Remittance> rem = OBDal.getInstance().createCriteria(Remittance.class);
    rem.createAlias(Remittance.PROPERTY_REMITTANCETYPE, "rt");
    rem.add(Restrictions.eq("rt." + RemittanceType.PROPERTY_PAYMENTMETHOD,
        payment.getPaymentMethod()));
    rem.add(Restrictions.eq(Remittance.PROPERTY_FINANCIALACCOUNT, payment.getAccount()));
    rem.add(Restrictions.eq(Remittance.PROPERTY_PROCESSED, false));
    rem.add(Restrictions.in("organization.id", OBContext.getOBContext()
        .getOrganizationStructureProvider().getParentTree(payment.getOrganization().getId(), true)));

    List<Remittance> openRemittances = rem.list();

    HashMap<String, String> result = new HashMap<String, String>();

    for (int i = 0; i < openRemittances.size(); i++) {
      for (RemittanceLine line : openRemittances.get(0).getREMRemittanceLineList()) {
        if (line.getPayment() == null) {
          continue;
        }
        if (line.getPayment().getId().equalsIgnoreCase(payment.getId())) {
          result.put("documentno", line.getRemittance().getDocumentNo());
          result.put("lineno", String.valueOf(line.getLineNo()));
          return result;
        }
      }
    }

    return result;
  }

  public static HashMap<String, String> existLineInSettledRemittance(FIN_Payment payment) {
    OBCriteria<RemittanceLineCancel> remlc = OBDal.getInstance().createCriteria(
        RemittanceLineCancel.class);
    remlc.add(Restrictions.eq(Remittance.PROPERTY_PAYMENT, payment));
    remlc.setMaxResults(1);
    RemittanceLineCancel settledLine = (RemittanceLineCancel) remlc.uniqueResult();
    HashMap<String, String> result = new HashMap<String, String>();
    if (settledLine != null) {
      result.put("documentno", settledLine.getRemittance().getDocumentNo());
      result.put("lineno", String.valueOf(settledLine.getLineNo()));
    }
    return result;
  }
}
