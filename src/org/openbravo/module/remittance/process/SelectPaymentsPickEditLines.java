/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2013-2017 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.remittance.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.module.remittance.Remittance;
import org.openbravo.module.remittance.RemittanceLine;

public class SelectPaymentsPickEditLines extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(SelectPaymentsPickEditLines.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    JSONObject jsonRequest = null;
    OBContext.setAdminMode();
    try {
      jsonRequest = new JSONObject(content);
      log.debug(jsonRequest);
      final String strRemittanceId = jsonRequest.getString("REM_Remittance_ID");
      Remittance remittance = OBDal.getInstance().get(Remittance.class, strRemittanceId);
      FIN_PaymentMethod paymentMethod = remittance.getRemittanceType().getPaymentMethod();

      List<RemittanceLine> rls = remittance.getREMRemittanceLineList();
      List<String> idList = new ArrayList<String>();
      for (RemittanceLine rl : rls) {
        if (rl.getPayment() != null)
          idList.add(rl.getId());
      }
      createPaymentProposalDetails(jsonRequest, paymentMethod, idList);
      jsonRequest = new JSONObject();

      JSONObject errorMessage = new JSONObject();
      errorMessage.put("severity", "success");
      errorMessage.put("text", OBMessageUtils.messageBD("Success"));
      // if (map.get("DifferentPaymentMethod").equals("true")) {
      // errorMessage.put("severity", "warning");
      // errorMessage.put("text",
      // OBMessageUtils.messageBD("APRM_Different_PaymentMethod_Selected"));
      // }
      jsonRequest.put("message", errorMessage);

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      log.error(e.getMessage(), e);

      try {
        jsonRequest = new JSONObject();

        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", OBMessageUtils.messageBD(e.getMessage()));

        jsonRequest.put("message", errorMessage);
      } catch (Exception e2) {
        log.error(e.getMessage(), e2);
        // do nothing, give up
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return jsonRequest;
  }

  private HashMap<String, String> createPaymentProposalDetails(JSONObject jsonRequest,
      FIN_PaymentMethod paymentMethod, List<String> idList) throws JSONException, OBException {

    HashMap<String, String> map = new HashMap<String, String>();
    map.put("DifferentPaymentMethod", "false");
    map.put("Count", "0");
    JSONObject params = jsonRequest.getJSONObject("_params");
    JSONObject gridInfo = params.getJSONObject("grid");
    JSONArray selectedLines = gridInfo.getJSONArray(ApplicationConstants.SELECTION_PROPERTY);
    final String strRemittanceId = jsonRequest.getString("REM_Remittance_ID");
    Remittance remittance = OBDal.getInstance().get(Remittance.class, strRemittanceId);
    // delete all the lines to avoid duplicated inserts
    removeNonSelectedLines(idList, remittance);
    // if no lines selected don't do anything.
    if (selectedLines.length() == 0) {
      // removeNonSelectedLines(idList, remittance);
      return map;
    }
    int cont = 0;
    long maxLine = 0l;
    for (int i = 0; i < selectedLines.length(); i++) {
      JSONObject selectedLine = selectedLines.getJSONObject((int) i);
      log.debug(selectedLine);
      // Get max line number
      FIN_Payment payment = OBDal.getInstance()
          .get(FIN_Payment.class, selectedLine.getString("id"));
      final OBCriteria<RemittanceLine> obc = OBDal.getInstance().createCriteria(
          RemittanceLine.class);
      obc.add(Restrictions.eq(RemittanceLine.PROPERTY_REMITTANCE, remittance));
      obc.addOrderBy(FIN_FinaccTransaction.PROPERTY_LINENO, false);
      obc.setMaxResults(1);
      if (obc.list() != null && obc.list().size() > 0) {
        maxLine = obc.list().get(0).getLineNo();
      }

      final RemittanceLine remLine = OBProvider.getInstance().get(RemittanceLine.class);
      remLine.setAmount(payment.getAmount());
      remLine.setPayment(payment);
      remLine.setRemittance(remittance);
      remLine.setOrganization(remittance.getOrganization());
      remLine.setLineNo(maxLine + 10);
      remLine.setOrigstatus(payment.getStatus());

      OBDal.getInstance().save(remLine);
      OBDal.getInstance().flush();
    }
    // removeNonSelectedLines(idList, remittance);

    OBDal.getInstance().save(remittance);
    // map.put("DifferentPaymentMethod", differentPaymentMethod);
    map.put("Count", Integer.toString(cont));
    return map;
  }

  private void removeNonSelectedLines(List<String> idList, Remittance remittance) {
    if (idList.size() > 0) {
      for (String id : idList) {
        RemittanceLine rl = OBDal.getInstance().get(RemittanceLine.class, id);
        remittance.getREMRemittanceLineList().remove(rl);
        OBDal.getInstance().remove(rl);
      }
      OBDal.getInstance().save(remittance);
      OBDal.getInstance().flush();
    }
  }
}
