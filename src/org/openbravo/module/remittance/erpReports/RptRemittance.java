/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.remittance.erpReports;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.application.report.ReportingUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.utility.FieldProviderFactory;
import org.openbravo.module.remittance.Remittance;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public class RptRemittance extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strremRemittanceId = vars.getSessionValue("RptRemittance.inpremRemittanceId_R");
      if (strremRemittanceId.equals(""))
        strremRemittanceId = vars.getSessionValue("RptRemittance.inpremRemittanceId");
      printPagePDF(response, vars, strremRemittanceId);
    } else {
      pageError(response);
    }
  }

  private void printPagePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strremRemittanceId) throws IOException, ServletException {
    log4j.debug("Output: pdf");

    String strLanguage = vars.getLanguage();
    String strBaseDesign = getBaseDesignPath(strLanguage);
    String strReportName = "@basedesign@/org/openbravo/module/remittance/erpReports/RptRemittance.jrxml";

    JasperReport jasperReportLines;
    try {

      jasperReportLines = ReportingUtils.getTranslatedJasperReport(this, strBaseDesign
          + "/org/openbravo/module/remittance/erpReports/RptRemittance_Lines.jrxml",
          vars.getLanguage());

    } catch (JRException e) {
      log4j.error("Error in printPagePDF: ", e);
      throw new ServletException(e.getMessage());
    }

    FieldProvider[] data = getData(strremRemittanceId);

    // include subreport
    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("SR_LINES", jasperReportLines);

    response.setContentType("text/html; charset=UTF-8");
    renderJR(vars, response, strReportName, "pdf", parameters, data, null);
  }

  /**
   * Data needed to fill the report
   * 
   * @param strRemittanceIds
   *          String containing the comma separated list of remittance id's to be printed
   * @return FieldProvider array with the data to fill the report
   */
  private FieldProvider[] getData(String strRemittanceIds) {
    String dateFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty("dateFormat.java");
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

    List<Remittance> remList = getOBObjectListFromString(Remittance.class, strRemittanceIds);
    FieldProvider[] data = new FieldProvider[remList.size()];
    try {
      OBContext.setAdminMode(true);
      int i = 0;
      for (Remittance rem : remList) {

        String taxId = "";
        if (rem.getOrganization().getOrganizationInformationList().size() > 0) {
          taxId = rem.getOrganization().getOrganizationInformationList().get(0).getTaxID();
        }

        String bankCode = (rem.getFinancialAccount().getBankCode() == null) ? "" : rem
            .getFinancialAccount().getBankCode() + " ";
        String branchCode = (rem.getFinancialAccount().getBranchCode() == null) ? "" : rem
            .getFinancialAccount().getBranchCode() + " ";
        String bankDigitControl = (rem.getFinancialAccount().getBankDigitcontrol() == null) ? ""
            : rem.getFinancialAccount().getBankDigitcontrol() + " ";
        String accountDigitControl = (rem.getFinancialAccount().getAccountDigitcontrol() == null) ? ""
            : rem.getFinancialAccount().getAccountDigitcontrol() + " ";
        String partialAccountNo = (rem.getFinancialAccount().getPartialAccountNo() == null) ? ""
            : rem.getFinancialAccount().getPartialAccountNo();
        String account = bankCode + branchCode + bankDigitControl + accountDigitControl
            + partialAccountNo;

        Map<String, String> hm = new HashMap<String, String>();
        hm.put("rem_remittance_id", rem.getId());
        hm.put("name", rem.getName());
        hm.put("documentno", rem.getDocumentNo());
        hm.put("discountdate", rem.getChargedate() != null ? sdf.format(rem.getChargedate()) : "");
        hm.put("datetrx", sdf.format(rem.getTransactionDate()));
        hm.put("entity", rem.getClient().getName());
        hm.put("cif", taxId);
        hm.put("name_bank", rem.getFinancialAccount().getName());
        hm.put("account", account);
        hm.put("isremitfordiscount",
            rem.getRemittanceType().isRemitForDiscount() && rem.getChargedate() != null ? "Y" : "N");
        hm.put("issinglepayment", rem.isSinglePayment() ? "Y" : "N");
        data[i] = new FieldProviderFactory(hm);

        i++;
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return data;
  }

  /**
   * Parses the string of comma separated id's to return a List with the BaseOBObjects of the given
   * class. If there is an invalid id a null value is added to the List. Added from OBDao.java (3.0)
   * for 2.50 compatibility
   * 
   * @param t
   *          class of the BaseOBObject the id's belong to
   * @param _IDs
   *          String containing the comma separated list of id's
   * @return a List object containing the parsed OBObjects
   */
  private static <T extends BaseOBObject> List<T> getOBObjectListFromString(Class<T> t, String _IDs) {
    String strBaseOBOBjectIDs = _IDs;
    final List<T> baseOBObjectList = new ArrayList<T>();
    if (strBaseOBOBjectIDs.startsWith("(")) {
      strBaseOBOBjectIDs = strBaseOBOBjectIDs.substring(1, strBaseOBOBjectIDs.length() - 1);
    }
    if (!strBaseOBOBjectIDs.equals("")) {
      strBaseOBOBjectIDs = StringUtils.remove(strBaseOBOBjectIDs, "'");
      StringTokenizer st = new StringTokenizer(strBaseOBOBjectIDs, ",", false);
      while (st.hasMoreTokens()) {
        String strBaseOBObjectID = st.nextToken().trim();
        baseOBObjectList.add((T) OBDal.getInstance().get(t, strBaseOBObjectID));
      }
    }
    return baseOBObjectList;
  }

  public String getServletInfo() {
    return "Servlet that presents the remittance";
  } // End of getServletInfo() method
}
