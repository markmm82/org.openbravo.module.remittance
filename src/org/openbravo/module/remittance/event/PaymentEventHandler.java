package org.openbravo.module.remittance.event;

import javax.enterprise.event.Observes;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.service.db.DalConnectionProvider;

import jxl.common.Logger;

public class PaymentEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      FIN_Payment.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    // TODO Auto-generated method stub
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final Entity finPaymentEntity = ModelProvider.getInstance().getEntity(FIN_Payment.ENTITY_NAME);
    final Property finPaymentStateProperty = finPaymentEntity
        .getProperty(FIN_Payment.PROPERTY_STATUS);
    final Property finPaymentProcessedProperty = finPaymentEntity
        .getProperty(FIN_Payment.PROPERTY_PROCESSED);
    final FIN_Payment payment = (FIN_Payment) event.getTargetInstance();
    String oldStatus = ((String) event.getPreviousState(finPaymentStateProperty));
    String newStatus = ((String) event.getCurrentState(finPaymentStateProperty));
    boolean newProcessed = (Boolean) (event.getCurrentState(finPaymentProcessedProperty));

    if ((!"RPAP".equalsIgnoreCase(oldStatus) && "RPAP".equalsIgnoreCase(newStatus))
        && (newProcessed == false)) {
      StringBuilder hql = new StringBuilder();
      hql.append("select 1 ");
      hql.append(" from REM_RemittanceLine ");
      hql.append(" where payment = '").append(payment.getId()).append("'");

      final Session session = OBDal.getInstance().getSession();
      @SuppressWarnings("rawtypes")
      final Query query = session.createQuery(hql.toString());

      if (query.list().size() > 0) {
        String language = OBContext.getOBContext().getLanguage().getLanguage();
        ConnectionProvider conn = new DalConnectionProvider(false);
        throw new OBException(Utility.messageBD(conn, "REM_PaymentInRemittance", language));
      }
    }
  }
}
