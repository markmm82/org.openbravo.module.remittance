/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012-2021 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

(function() {
  OB.REM = {};
  OB.REM.total = {};
  OB.REM.CalculateTotal = function(grid, record, state) {
    grid.disable();
    grid.fireOnPause(
      'selectionChanged' + record.id,
      function() {
        var receipt = record.receipt,
          recordAmount = record.amount,
          total = '',
          currencyIso,
          currencyIsoAttribute =
            'currency' + OB.Constants.FIELDSEPARATOR + OB.Constants.IDENTIFIER;
        if (!state) {
          recordAmount = -recordAmount;
        }
        if (!receipt) {
          recordAmount = -recordAmount;
        }
        if (!OB.REM.total[record[currencyIsoAttribute]]) {
          OB.REM.total[record[currencyIsoAttribute]] = new BigDecimal('0');
        }
        OB.REM.total[record[currencyIsoAttribute]] = OB.REM.total[
          record[currencyIsoAttribute]
        ]
          .add(new BigDecimal(recordAmount.toString()))
          .setScale(2);
        if (
          OB.REM.total[record[currencyIsoAttribute]].compareTo(
            new BigDecimal('0')
          ) === 0
        ) {
          delete OB.REM.total[record[currencyIsoAttribute]];
        }
        for (currencyIso in OB.REM.total) {
          if (Object.prototype.hasOwnProperty.call(OB.REM.total, currencyIso)) {
            total +=
              OB.REM.total[currencyIso].toString() + ' ' + currencyIso + ' ';
          }
        }
        if (!total) {
          total = '0';
        }
        grid.view.theForm.getItem('total').setValue(total);
        grid.enable();
      },
      200
    );
  };
  OB.REM.CalculateSelected = function(grid) {
    var total = '',
      selectedRecords = grid.getSelectedRecords(),
      recordAmount,
      i,
      currencyIso,
      currencyIsoAttribute =
        'currency' + OB.Constants.FIELDSEPARATOR + OB.Constants.IDENTIFIER;
    OB.REM.total = {};
    for (i = 0; i < selectedRecords.length; i++) {
      recordAmount = selectedRecords[i].amount;
      if (!selectedRecords[i].receipt) {
        recordAmount = -recordAmount;
      }
      if (!OB.REM.total[selectedRecords[i][currencyIsoAttribute]]) {
        OB.REM.total[selectedRecords[i][currencyIsoAttribute]] = new BigDecimal(
          '0'
        );
      }
      OB.REM.total[selectedRecords[i][currencyIsoAttribute]] = OB.REM.total[
        selectedRecords[i][currencyIsoAttribute]
      ]
        .add(new BigDecimal(recordAmount.toString()))
        .setScale(2);
      if (
        OB.REM.total[selectedRecords[i][currencyIsoAttribute]].compareTo(
          new BigDecimal('0')
        ) === 0
      ) {
        delete OB.REM.total[selectedRecords[i][currencyIsoAttribute]];
      }
    }
    for (currencyIso in OB.REM.total) {
      if (Object.prototype.hasOwnProperty.call(OB.REM.total, currencyIso)) {
        total += OB.REM.total[currencyIso].toString() + ' ' + currencyIso + ' ';
      }
    }
    if (!total) {
      total = '0';
    }
    grid.view.theForm.getItem('total').setValue(total);
  };
})();
