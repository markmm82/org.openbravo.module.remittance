<?xml version="1.0" encoding="UTF-8" ?>
<!--
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011-2016 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
-->
<SqlClass name="CreateAccountingConfigurationData" package="org.openbravo.module.remittance.modulescript">
  <SqlClassComment></SqlClassComment>
  <SqlMethod name="selectAcctSchema" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT C_AcctSchema_ID, ad_client_id, '' as ad_org_id, '' as ad_table_id, '' as name,
      '' as c_period_id, '' as value, '' as status, '' as isdefaultacct
      FROM C_AcctSchema
      ]]>
    </Sql>
  </SqlMethod>
  <SqlMethod name="selectTables" type="preparedStatement" return="boolean">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT count(*) as name
      FROM c_acctschema_table
      WHERE c_acctschema_id = ?
      AND ad_table_id = ?
      ]]>
    </Sql>
    <Parameter name="acctSchemaId"/>
    <Parameter name="tableId"/>
  </SqlMethod>
  <SqlMethod name="insertAcctSchemaTable" type="preparedStatement" connection="true" return="rowCount">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      INSERT INTO c_acctschema_table(
            c_acctschema_table_id, c_acctschema_id, ad_table_id, ad_client_id,
            ad_org_id, isactive, created, createdby, updated, updatedby,
            ad_createfact_template_id, acctdescription)
      VALUES (get_uuid(), ?, ?, ?,
            '0', 'Y', now(), '100', now(), '100',
            null, null)
      ]]>
    </Sql>
    <Parameter name="acctSchemaId"/>
    <Parameter name="tableId"/>
    <Parameter name="clientId"/>
  </SqlMethod>
  <SqlMethod name="selectPeriods" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT c_period.ad_client_id as ad_client_id, c_period.c_period_id as c_period_id,
      c_periodcontrol.ad_org_id, max(periodstatus) as status, '' as name, '' as value,
      '' as C_AcctSchema_ID
      FROM c_period, c_periodcontrol
      WHERE c_period.c_period_id = c_periodcontrol.c_period_id
      GROUP BY c_period.ad_client_id, c_period.c_period_id, c_periodcontrol.ad_org_id
      ]]>
    </Sql>
  </SqlMethod>
  <SqlMethod name="selectPeriodControl" type="preparedStatement" return="boolean">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      SELECT count(*) as name
      FROM c_periodcontrol
      WHERE c_period_id = ?
      AND docbasetype = ?
      AND ad_org_id = ?
      ]]>
    </Sql>
    <Parameter name="periodId"/>
    <Parameter name="docbasetype"/>
    <Parameter name="orgId"/>
  </SqlMethod>
   <SqlMethod name="insertPeriodControl" type="preparedStatement" connection="true" return="rowCount">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
      INSERT INTO c_periodcontrol(
                  c_periodcontrol_id, ad_client_id, ad_org_id, isactive, created,
                  createdby, updated, updatedby, c_period_id, docbasetype, periodstatus,
                  periodaction, processing)
      VALUES (get_uuid(), ?, ?, 'Y', now(),
              '100', now(), '100', ?, ?, ?,
              'N', 'N')
      ]]>
    </Sql>
    <Parameter name="clientId"/>
    <Parameter name="orgId"/>
    <Parameter name="periodId"/>
    <Parameter name="docBaseType"/>
    <Parameter name="status"/>
  </SqlMethod>
</SqlClass>
