<?xml version="1.0" encoding="UTF-8" ?>
<!--
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011-2018 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
-->
<SqlClass name="PopulateBankInstructionsData" package="org.openbravo.module.remittance.modulescript">
  <SqlClassComment></SqlClassComment>
  <SqlMethod name="selectRemittanceLines" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
        SELECT rl.ad_client_id, rl.ad_org_id, rl.rem_remittance_id,
            b.c_bpartner_id,
            sum(rl.amount) AS amount, COALESCE(TO_CHAR(bpb.accountno), TO_CHAR('')) as accountno, p.paymentdate AS duedate,
            CASE WHEN COALESCE(substr(p.description, length(p.description)),'') = chr(10)
              THEN substr(p.description, 1, length(p.description)-1)
            ELSE p.description END AS description
        FROM rem_remittanceline rl,
             c_bpartner b LEFT JOIN c_bp_bankaccount bpb ON b.c_bpartner_id = bpb.c_bpartner_id,
             fin_payment p
        WHERE rl.rem_remittance_id = ?
              AND rl.fin_payment_id = p.fin_payment_id
              AND p.c_bpartner_id   = b.c_bpartner_id
        GROUP BY p.fin_payment_id, p.paymentdate, p.description, bpb.accountno,
                 rl.ad_client_id, rl.ad_org_id, rl.rem_remittance_id, b.c_bpartner_id,
                 accountno, p.paymentdate
        ORDER BY p.paymentdate
      ]]>
    </Sql>
    <Parameter name="remittanceId"/>
  </SqlMethod>
  <SqlMethod name="selectRemittances" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
        SELECT rem.rem_remittance_id
        FROM rem_remittance rem
        WHERE processed='Y' AND
              not exists (SELECT 1
                          FROM rem_instruction inst
                          WHERE inst.rem_remittance_id = rem.rem_remittance_id)
      ]]>
    </Sql>
  </SqlMethod>
  <SqlMethod name="insertInstructions" type="preparedStatement" connection="true" return="rowCount">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[        
        INSERT INTO rem_instruction (
          rem_instruction_id, ad_client_id, ad_org_id, isactive, created, 
          createdby, updated, updatedby, rem_remittance_id,
          line, c_bpartner_id, 
          amount, accountno, duedate,
          description, msgtext)
        VALUES (get_uuid(), ?, ?, 'Y', now(),
            '100', now(), '100', ?,
            TO_NUMBER(?), ?,
            TO_NUMBER(?), ?, TO_DATE(?),
            ?, '')
      ]]>
    </Sql>
    <Parameter name="clientId"/>
    <Parameter name="orgId"/>
    <Parameter name="remittanceId"/>
    <Parameter name="lineNo"/>
    <Parameter name="bPartnerId"/>
    <Parameter name="amount"/>
    <Parameter name="accountNo"/>
    <Parameter name="dueDate"/>
    <Parameter name="description"/>
  </SqlMethod>
</SqlClass>
