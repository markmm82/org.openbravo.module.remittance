/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */


package org.openbravo.module.remittance.modulescript;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;

public class InvoiceWrongPaymentComplete extends ModuleScript {

  @Override
  public void execute() {
    try {
      ConnectionProvider cp = getConnectionProvider();
      boolean isInitialized= InvoiceWrongPaymentCompleteData.isExecuted(cp);

      if (!isInitialized){       
        for (InvoiceWrongPaymentCompleteData wronginvoice : InvoiceWrongPaymentCompleteData.selectWrongInvoices(cp)) {  
          for (InvoiceWrongPaymentCompleteData amounts : InvoiceWrongPaymentCompleteData.selectAmounts(cp, wronginvoice.finPaymentId)) {
            InvoiceWrongPaymentCompleteData.updateCustomerCredit(cp, amounts.isreceipt,amounts.amount, amounts.cBpartnerId);
            InvoiceWrongPaymentCompleteData.updateFinPaymentschedule(cp, amounts.amount,wronginvoice.cInvoiceId);
            InvoiceWrongPaymentCompleteData.updateInvoice(cp,amounts.amount,wronginvoice.cInvoiceId);
            InvoiceWrongPaymentCompleteData.updateIsInvoicePaid(cp, wronginvoice.finPaymentId);    
            InvoiceWrongPaymentCompleteData.updatePaidInFull(cp,wronginvoice.cInvoiceId); 
          }
          if ("Y".equals(wronginvoice.iscashvat)){
            InvoiceWrongPaymentCompleteData[] missedFPDs = InvoiceWrongPaymentCompleteData.selectMissedPaymentDetails(cp, wronginvoice.cInvoiceId);            
            for (int i = 0; i < missedFPDs.length; i++) {
              final String invoiceId = missedFPDs[i].cInvoiceId;
              final String isPaid = missedFPDs[i].ispaid;
              final String invoiceTaxId = missedFPDs[i].cInvoicetaxId;
              final String finPaymentDetailId = missedFPDs[i].finPaymentDetailId;
              final String stdPrecision = missedFPDs[i].stdprecision;
              final String percentage = missedFPDs[i].percentage;

              boolean isFinalAdjustment = false;
              if ("Y".equals(isPaid)) {
                  final String lastPaymentDetailId = InvoiceWrongPaymentCompleteData.selectLastPaymentDetail(cp, invoiceId)[0].finPaymentDetailId;
                  if (finPaymentDetailId.equals(lastPaymentDetailId)) {
                      isFinalAdjustment = true;
                  }
              }
              if (isFinalAdjustment) {
                  InvoiceWrongPaymentCompleteData.insertCashVATInfoLastPayment(cp, invoiceTaxId, finPaymentDetailId);
              } else {
                  InvoiceWrongPaymentCompleteData.insertCashVATInfoNonLastPayment(cp, invoiceTaxId, percentage, stdPrecision, finPaymentDetailId);
              }
          }
          }
        }   
        InvoiceWrongPaymentCompleteData.createPreference(cp);      
      }        
    } catch (Exception e) {
      handleError(e);
    }
  }
}
