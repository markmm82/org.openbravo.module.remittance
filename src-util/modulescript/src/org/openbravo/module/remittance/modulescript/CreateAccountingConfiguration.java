/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2016 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.module.remittance.modulescript;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;
import org.openbravo.modulescript.ModuleScriptExecutionLimits;
import org.openbravo.modulescript.OpenbravoVersion;

public class CreateAccountingConfiguration extends ModuleScript {
  private static final Logger log4j = Logger.getLogger(CreateAccountingConfiguration.class);

  @Override
  // Inserting:
  // 1) Accounting schema tables for new tables in the module
  // 2) Period control for newly added DocBaseTypes
  public void execute() {
    try {
      ConnectionProvider cp = getConnectionProvider();
      createAcctSchemaTables(cp);
      createPeriodControl(cp);
    } catch (Exception e) {
      handleError(e);
    }
  }

  void createAcctSchemaTables(ConnectionProvider cp) throws Exception {
    CreateAccountingConfigurationData[] data = CreateAccountingConfigurationData
        .selectAcctSchema(cp);
    int count = 0;
    final String TABLE_REMITTANCE = "FF8080812E6C2859012E6D0271E1009D";
    final String TABLE_REMITTANCE_CANCEL = "4E2A33FA8835466BAAA596A17D680E19";
    final String TABLE_REMITTANCE_RETURN = "379F8A7C50E3405E838CE94153025761";
    String tables[] = { TABLE_REMITTANCE, TABLE_REMITTANCE_CANCEL, TABLE_REMITTANCE_RETURN };
    for (int i = 0; i < data.length; i++) {
      for (int j = 0; j < tables.length; j++) {
        boolean exist = CreateAccountingConfigurationData.selectTables(cp,
            data[i].cAcctschemaId, tables[j]);
        if (!exist) {
          count++;
          CreateAccountingConfigurationData.insertAcctSchemaTable(cp.getConnection(), cp,
              data[i].cAcctschemaId, tables[j], data[i].adClientId);
        }
      }
    }
    if (count > 0) {
      log4j.info("Created " + count + " remittance accounting control records");
    }
  }

  void createPeriodControl(ConnectionProvider cp) throws Exception {
    CreateAccountingConfigurationData[] data = CreateAccountingConfigurationData.selectPeriods(cp);
    int count = 0;
    String docBaseTypes[] = { "REM_REMRETURN", "REM_REM", "REM_REMCANCEL" };
    for (int i = 0; i < data.length; i++) {
      for (int j = 0; j < docBaseTypes.length; j++) {
        boolean exist = CreateAccountingConfigurationData.selectPeriodControl(cp,
            data[i].cPeriodId, docBaseTypes[j], data[i].adOrgId);
        if (!exist) {
          count++;
          CreateAccountingConfigurationData.insertPeriodControl(cp.getConnection(), cp,
              data[i].adClientId, data[i].adOrgId, data[i].cPeriodId, docBaseTypes[j],
              data[i].status);
        }
      }
    }
    if (count > 0) {
      log4j.info("Created " + count + " remittance period control records");
    }
  }

  @Override
  protected ModuleScriptExecutionLimits getModuleScriptExecutionLimits() {
    return new ModuleScriptExecutionLimits("FF8080812E6B7A19012E6B7E62240008", null,
        new OpenbravoVersion(3, 2, 1000));
  }
}
